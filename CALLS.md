## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Cisco Prime. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Cisco Prime.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Cisco Prime. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpAaaTacacsPlusServer(serverIp, serverHostName, callback)</td>
    <td style="padding:15px">DELETE Delete Server
DELETE op/aaa/tacacsPlusServer</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/aaa/tacacsPlusServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpAaaTacacsPlusServer(callback)</td>
    <td style="padding:15px">GET List Servers
GET op/aaa/tacacsPlusServer</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/aaa/tacacsPlusServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpAaaTacacsPlusServer(body, callback)</td>
    <td style="padding:15px">POST Add Server
POST op/aaa/tacacsPlusServer</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/aaa/tacacsPlusServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpAaaTacacsPlusServer(body, callback)</td>
    <td style="padding:15px">PUT Update Server
PUT op/aaa/tacacsPlusServer</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/aaa/tacacsPlusServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataApOnboardingProfile(callback)</td>
    <td style="padding:15px">GET Get AP onboarding profiles
GET data/ApOnboardingProfile</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/ApOnboardingProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpApOnboardingProfile(body, callback)</td>
    <td style="padding:15px">DELETE Delete AP onboarding profiles
DELETE op/apOnboarding/profile</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/apOnboarding/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpApOnboardingProfile(overrideOption, callback)</td>
    <td style="padding:15px">POST Create AP onboarding profiles
POST op/apOnboarding/profile</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/apOnboarding/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpApOnboardingProfile(callback)</td>
    <td style="padding:15px">PUT Modify AP onboarding profile
PUT op/apOnboarding/profile</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/apOnboarding/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataApiHealthRecords(callback)</td>
    <td style="padding:15px">GET API Health Record
GET data/ApiHealthRecords</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/ApiHealthRecords?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataApiResponseTimeSummary(callback)</td>
    <td style="padding:15px">GET API Response Time Summary
GET data/ApiResponseTimeSummary</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/ApiResponseTimeSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpApiHealthCallsPerClient(service, callback)</td>
    <td style="padding:15px">GET API Calls Per Client
GET op/apiHealth/callsPerClient</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/apiHealth/callsPerClient?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpApiHealthRequestCountTrend(service, callback)</td>
    <td style="padding:15px">GET API Request Count Trend
GET op/apiHealth/requestCountTrend</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/apiHealth/requestCountTrend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpApiHealthResponseTimeTrend(service, callback)</td>
    <td style="padding:15px">GET API Response Time Trend
GET op/apiHealth/responseTimeTrend</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/apiHealth/responseTimeTrend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpApiHealthServiceNameList(callback)</td>
    <td style="padding:15px">GET API Service Name List
GET op/apiHealth/serviceNameList</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/apiHealth/serviceNameList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpApServiceRxNeighbors(apId, callback)</td>
    <td style="padding:15px">GET Get Rx Neighbors
GET op/apService/rxNeighbors</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/apService/rxNeighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpApServiceDeleteById(body, callback)</td>
    <td style="padding:15px">PUT Delete Access Points By IDs
PUT op/apService/deleteById</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/apService/deleteById?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpApServiceDeleteAp(body, callback)</td>
    <td style="padding:15px">PUT Delete Access Points By Names
PUT op/apService/deleteAp</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/apService/deleteAp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpApServiceAccessPoint(body, callback)</td>
    <td style="padding:15px">PUT Modify Unified Access Point
PUT op/apService/accessPoint</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/apService/accessPoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpRogueApClassification(callback)</td>
    <td style="padding:15px">PUT Set Classification Type And Rogue State
PUT op/rogueAp/classification</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/rogueAp/classification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpAlarmsAcknowledge(callback)</td>
    <td style="padding:15px">PUT Acknowledges Alarms
PUT op/alarms/acknowledge</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/alarms/acknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpAlarmsAnnotate(note, callback)</td>
    <td style="padding:15px">PUT Annotates Alarms
PUT op/alarms/annotate</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/alarms/annotate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpAlarmsClear(callback)</td>
    <td style="padding:15px">PUT Clears Alarms
PUT op/alarms/clear</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/alarms/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpAlarmsClearByEventType(setSeverityToInfo, callback)</td>
    <td style="padding:15px">PUT Clears Alarms with provided event type
PUT op/alarms/clearByEventType</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/alarms/clearByEventType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpAlarmsUnacknowledge(callback)</td>
    <td style="padding:15px">PUT Unacknowledges Alarms
PUT op/alarms/unacknowledge</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/alarms/unacknowledge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataApplications(callback)</td>
    <td style="padding:15px">GET Predefined and User-defined Applications
GET data/Applications</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/Applications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataCliTemplate(callback)</td>
    <td style="padding:15px">GET CLI Configuration Templates
GET data/CliTemplate</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/CliTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpCliTemplateConfigurationDeleteTemplate(callback)</td>
    <td style="padding:15px">DELETE Delete Configuration Template
DELETE op/cliTemplateConfiguration/deleteTemplate</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/cliTemplateConfiguration/deleteTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpCliTemplateConfigurationFolder(callback)</td>
    <td style="padding:15px">DELETE Delete Configuration Template Folder
DELETE op/cliTemplateConfiguration/folder</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/cliTemplateConfiguration/folder?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpCliTemplateConfigurationFolder(callback)</td>
    <td style="padding:15px">GET List Configuration Template Folders
GET op/cliTemplateConfiguration/folder</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/cliTemplateConfiguration/folder?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpCliTemplateConfigurationDeviceTypes(callback)</td>
    <td style="padding:15px">GET List Device Types
GET op/cliTemplateConfiguration/deviceTypes</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/cliTemplateConfiguration/deviceTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpCliTemplateConfigurationDeviceTypes(callback)</td>
    <td style="padding:15px">POST Create Configuration Template Folder
POST op/cliTemplateConfiguration/folder</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/cliTemplateConfiguration/deviceTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpCliTemplateConfigurationUpload(callback)</td>
    <td style="padding:15px">POST Upload Configuration Template
POST op/cliTemplateConfiguration/upload</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/cliTemplateConfiguration/upload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpCliTemplateConfigurationDeployTemplateThroughJob(startTime, callback)</td>
    <td style="padding:15px">PUT Deploy Configuration Template Through Job
PUT op/cliTemplateConfiguration/deployTemplateThrough</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/cliTemplateConfiguration/deployTemplateThroughJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpCliTemplateConfigurationTemplate(callback)</td>
    <td style="padding:15px">PUT Modify Configuration Template Content
PUT op/cliTemplateConfiguration/template</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/cliTemplateConfiguration/template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataClientCounts(callback)</td>
    <td style="padding:15px">GET Client Counts
GET data/ClientCounts</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/ClientCounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataClientSessions(callback)</td>
    <td style="padding:15px">GET Client Sessions
GET data/ClientSessions</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/ClientSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataClientTraffics(callback)</td>
    <td style="padding:15px">GET Client Traffic Information
GET data/ClientTraffics</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/ClientTraffics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataHistoricalClientCounts(callback)</td>
    <td style="padding:15px">GET Historical Client Counts
GET data/HistoricalClientCounts</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/HistoricalClientCounts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataHistoricalClientTraffics(callback)</td>
    <td style="padding:15px">GET Historical Client Traffic Information
GET data/HistoricalClientTraffics</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/HistoricalClientTraffics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpComplianceCheck(jobName, callback)</td>
    <td style="padding:15px">GET Checks status of Check Job
GET op/compliance/check</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/compliance/check?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpCompliancePolicy(body, callback)</td>
    <td style="padding:15px">POST Add policy
POST op/compliance/policy</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/compliance/policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpComplianceProfile(body, callback)</td>
    <td style="padding:15px">POST Add profile
POST op/compliance/profile</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/compliance/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpCompliancePolicyImport(body, callback)</td>
    <td style="padding:15px">POST Import policy
POST op/compliance/policy/import</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/compliance/policy/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpComplianceRun(body, callback)</td>
    <td style="padding:15px">POST Schedule a Check Job
POST op/compliance/run</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/compliance/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataBulkSanitizedConfigArchives(callback)</td>
    <td style="padding:15px">GET Bulk export sanitized configuration archives
GET data/BulkSanitizedConfigArchives</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/BulkSanitizedConfigArchives?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataBulkUnsanitizedConfigArchives(callback)</td>
    <td style="padding:15px">GET Bulk export unsanitized configuration archives
GET data/BulkUnsanitizedConfigArchives</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/BulkUnsanitizedConfigArchives?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataConfigArchives(callback)</td>
    <td style="padding:15px">GET Configuration archive list
GET data/ConfigArchives</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/ConfigArchives?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataConfigVersions(callback)</td>
    <td style="padding:15px">GET Configuration versions and files list
GET data/ConfigVersions</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/ConfigVersions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpConfigArchiveServiceConfigDiffDevice(device, timeStamp, diffType, callback)</td>
    <td style="padding:15px">GET Configuration Diff
GET op/configArchiveService/configDiff/{device}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/configArchiveService/configDiff/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpConfigArchiveServiceExtractSanitizedFile(fileId, callback)</td>
    <td style="padding:15px">GET Download the sanitized configuration file
GET op/configArchiveService/extractSanitizedFile</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/configArchiveService/extractSanitizedFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpConfigArchiveServiceExtractUnsanitizedFile(fileId, callback)</td>
    <td style="padding:15px">GET Download the unsanitized configuration file
GET op/configArchiveService/extractUnsanitizedFile</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/configArchiveService/extractUnsanitizedFile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpCredentialProfilesManagementCredentialProfiles(callback)</td>
    <td style="padding:15px">DELETE Delete credential profile
DELETE op/credentialProfilesManagement/credentialProfiles</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/credentialProfilesManagement/credentialProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpCredentialProfilesManagementCredentialProfiles(callback)</td>
    <td style="padding:15px">GET Get credential profiles
GET op/credentialProfilesManagement/credentialProfiles</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/credentialProfilesManagement/credentialProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpCredentialProfilesManagementDeviceList(profileName, callback)</td>
    <td style="padding:15px">GET Get list of configured devices
GET op/credentialProfilesManagement/deviceList</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/credentialProfilesManagement/deviceList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpCredentialProfilesManagementDeviceList(callback)</td>
    <td style="padding:15px">POST Create credential profile
POST op/credentialProfilesManagement/credentialProfiles</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/credentialProfilesManagement/deviceList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpCredentialProfilesManagementDeviceList(callback)</td>
    <td style="padding:15px">PUT Update credential profile
PUT op/credentialProfilesManagement/credentialProfiles</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/credentialProfilesManagement/deviceList?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataInventoryDetails(callback)</td>
    <td style="padding:15px">GET Inventory Details
GET data/InventoryDetails</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/InventoryDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataMerakiAccessPoints(callback)</td>
    <td style="padding:15px">GET Meraki Access Points
GET data/MerakiAccessPoints</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/MerakiAccessPoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataMerakiDevices(callback)</td>
    <td style="padding:15px">GET Meraki Devices
GET data/MerakiDevices</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/MerakiDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpDevicesExportDevices(ipAddress, groupId, groupPath, callback)</td>
    <td style="padding:15px">GET Export Devices
GET op/devices/exportDevices</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/devices/exportDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpDevicesRemovalJob(body, callback)</td>
    <td style="padding:15px">POST Delete devices through job
POST op/devices/removalJob</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/devices/removalJob?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpDevicesSyncDevices(callback)</td>
    <td style="padding:15px">POST Synchronize devices
POST op/devices/syncDevices</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/devices/syncDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpDevicesBulkImport(body, callback)</td>
    <td style="padding:15px">PUT Bulk Import
PUT op/devices/bulkImport</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/devices/bulkImport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpConfigureApScheduleMaintenanceMode(scheduledTime, callback)</td>
    <td style="padding:15px">PUT Schedule AP Maintenance Mode
PUT op/configure/ap/scheduleMaintenanceMode</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/configure/ap/scheduleMaintenanceMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpConfigureDeviceScheduleMaintenanceMode(scheduledTime, callback)</td>
    <td style="padding:15px">PUT Schedule Maintenance Mode
PUT op/configure/device/scheduleMaintenanceMode</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/configure/device/scheduleMaintenanceMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpConfigureApMaintenanceMode(body, callback)</td>
    <td style="padding:15px">PUT Set AP Maintenance Mode
PUT op/configure/ap/maintenanceMode</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/configure/ap/maintenanceMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpConfigureDeviceMaintenanceMode(body, callback)</td>
    <td style="padding:15px">PUT Set Device Maintenance Mode
PUT op/configure/device/maintenanceMode</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/configure/device/maintenanceMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpGroupsGroup(callback)</td>
    <td style="padding:15px">DELETE Delete device or port group
DELETE op/groups/group</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/groups/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpGroupsGroupRules(groupId, groupPath, callback)</td>
    <td style="padding:15px">GET Device Group Rules
GET op/groups/groupRules</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/groups/groupRules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpGroupsGroupDevices(groupId, groupPath, callback)</td>
    <td style="padding:15px">PUT Assign devices to a group
PUT op/groups/groupDevices</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/groups/groupDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpGroupsGroupInterfaces(groupId, callback)</td>
    <td style="padding:15px">PUT Create or update device or port group
PUT op/groups/group</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/groups/groupInterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpGroupsRemoveDevices(groupId, groupPath, callback)</td>
    <td style="padding:15px">PUT Remove devices from a group
PUT op/groups/removeDevices</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/groups/removeDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpGroupsRemoveInterfaces(groupId, groupPath, callback)</td>
    <td style="padding:15px">PUT Replace set of group rules
PUT op/groups/groupRules</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/groups/removeInterfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataGroupSpecification(callback)</td>
    <td style="padding:15px">GET Group Specification
GET data/GroupSpecification</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/GroupSpecification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpGroupsAlarmSummary(callback)</td>
    <td style="padding:15px">GET Alarm Summary
GET op/groups/alarmSummary</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/groups/alarmSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpGroupsDeviceGroups(noAlarms, noMembersCount, callback)</td>
    <td style="padding:15px">GET Device Groups
GET op/groups/deviceGroups</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/groups/deviceGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpGroupsSites(noAlarms, noMembersCount, callback)</td>
    <td style="padding:15px">GET Location Groups
GET op/groups/sites</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/groups/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpGroupsUserDefinedGroups(groupType, noAlarms, noMembersCount, callback)</td>
    <td style="padding:15px">GET User Defined Groups
GET op/groups/userDefinedGroups</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/groups/userDefinedGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataGuestUsers(callback)</td>
    <td style="padding:15px">GET Get guest users
GET data/GuestUsers</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/GuestUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpGuestUserGuestUsersOnController(controllerId, callback)</td>
    <td style="padding:15px">GET Get guest users from controller
GET op/guestUser/guestUsersOnController</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/guestUser/guestUsersOnController?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpGuestUserLoggedInGuestUsersOnController(controllerId, callback)</td>
    <td style="padding:15px">GET Get logged in guest users from controller
GET op/guestUser/loggedInGuestUsersOnController</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/guestUser/loggedInGuestUsersOnController?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpGuestUser(body, callback)</td>
    <td style="padding:15px">POST Create guest user
POST op/guestUser</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/guestUser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpGuestUserDelete(body, callback)</td>
    <td style="padding:15px">PUT Update guest user
PUT op/guestUser</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/guestUser/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataJobSummary(callback)</td>
    <td style="padding:15px">GET Job Summary
GET data/JobSummary</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/JobSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpJobServiceRunhistory(jobId, jobType, jobName, startTime, callback)</td>
    <td style="padding:15px">GET Query Job Run History
GET op/jobService/runhistory</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/jobService/runhistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpJobServiceCancel(body, callback)</td>
    <td style="padding:15px">PUT Cancel A Job
PUT op/jobService/cancel</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/jobService/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpJobServiceCancelrunning(body, callback)</td>
    <td style="padding:15px">PUT Cancel A Job running instance
PUT op/jobService/cancelrunning</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/jobService/cancelrunning?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpJobServiceResume(body, callback)</td>
    <td style="padding:15px">PUT Resume A Job
PUT op/jobService/resume</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/jobService/resume?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpJobServiceSuspend(body, callback)</td>
    <td style="padding:15px">PUT Suspend A Job
PUT op/jobService/suspend</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/jobService/suspend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataMacFilterTemplates(callback)</td>
    <td style="padding:15px">GET MAC Filter Templates
GET data/MacFilterTemplates</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/MacFilterTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpMacFilterMacFilterTemplate(callback)</td>
    <td style="padding:15px">DELETE Delete MAC Filter Template
DELETE op/macFilter/macFilterTemplate</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/macFilter/macFilterTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpMacFilterMacFilterTemplate(callback)</td>
    <td style="padding:15px">POST Create MAC Filter Template
POST op/macFilter/macFilterTemplate</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/macFilter/macFilterTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpMacFilterDeploy(callback)</td>
    <td style="padding:15px">PUT Update MAC Filter Template
PUT op/macFilter/macFilterTemplate</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/macFilter/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpInfoCoredumps(callback)</td>
    <td style="padding:15px">GET Core Dumps
GET op/info/coredumps</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/info/coredumps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpInfoDisk(callback)</td>
    <td style="padding:15px">GET Disk Usage and Capacity
GET op/info/disk</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/info/disk?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpInfoLicense(callback)</td>
    <td style="padding:15px">GET Licenses
GET op/info/license</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/info/license?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpInfoUptime(callback)</td>
    <td style="padding:15px">GET Uptime
GET op/info/uptime</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/info/uptime?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpInfoVersion(callback)</td>
    <td style="padding:15px">GET Version
GET op/info/version</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/info/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpPnpProfileProfileNameInstance(profileName, callback)</td>
    <td style="padding:15px">DELETE Delete All Profile Instances of a PnP Profile
DELETE op/pnp/profile/{profileName}/instance/</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pnp/profile/{pathv1}/instance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPnpProfilePnpProfileFolderProfileName(pnpProfileFolder, profileName, callback)</td>
    <td style="padding:15px">GET Read Pnp Profile
GET op/pnp/profile/{pnpProfileFolder}/{profileName}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pnp/profile/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpPnpProfilePnpProfileFolderProfileName(pnpProfileFolder, profileName, callback)</td>
    <td style="padding:15px">POST Create Pnp Profile
POST op/pnp/profile/{pnpProfileFolder}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pnp/profile/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpPnpProfilePnpProfileFolderProfileName(pnpProfileFolder, profileName, callback)</td>
    <td style="padding:15px">DELETE Delete Pnp Profile
DELETE op/pnp/profile/{pnpProfileFolder}/{profileName}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pnp/profile/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName(profileName, instanceName, callback)</td>
    <td style="padding:15px">DELETE Delete Profile Instance of a PnP Profile
DELETE op/pnp/profile/{profileName}/instance/{insta</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pnp/profile/{pathv1}/instance/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName(profileName, instanceName, callback)</td>
    <td style="padding:15px">GET Get Profile Instance of PnP Profile
GET op/pnp/profile/{profileName}/instance/{instanceName}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pnp/profile/{pathv1}/instance/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpRateServiceRateLimits(callback)</td>
    <td style="padding:15px">GET Get Rate Limits
GET op/rateService/rateLimits</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/rateService/rateLimits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpRateServiceRateLimits(callback)</td>
    <td style="padding:15px">PUT Update Rate Limits
PUT op/rateService/rateLimits</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/rateService/rateLimits?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpReportServiceTemplates(reportCategory, reportType, scheduled, virtualDomain, callback)</td>
    <td style="padding:15px">GET Get Available Report Templates
GET op/reportService/templates</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/reportService/templates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpReportServiceGetReport(reportTitle, pageIndex, callback)</td>
    <td style="padding:15px">GET Get a Report
GET op/reportService/getReport</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/reportService/getReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpReportServiceZipReport(callback)</td>
    <td style="padding:15px">GET Run a ZIP Report
GET op/reportService/zipReport</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/reportService/zipReport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpUpdateRetrieve(callback)</td>
    <td style="padding:15px">GET Information about updates
GET op/update/retrieve</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/update/retrieve?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetAllBorderRoutersStartTimeEndTimeSiteName(startTime, endTime, siteName, hierarchyPath, callback)</td>
    <td style="padding:15px">GET All Border Routers
GET op/pfrMonitoringExt/getAllBorderRouters/{startTime}/{endTime}/{siteName}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getAllBorderRouters/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime(startTime, endTime, srcSite, dstSite, srcHierarchyPath, destHierarchyPath, callback)</td>
    <td style="padding:15px">GET All IMEs
GET op/pfrMonitoringExt/getAllIMEs/{srcSite}/{dstSite}/{startTime}/{endTime}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getAllIMEs/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime(startTime, endTime, srcSite, dstSite, srcHierarchyPath, destHierarchyPath, callback)</td>
    <td style="padding:15px">GET All RCs
GET op/pfrMonitoringExt/getAllRCs/{srcSite}/{dstSite}/{startTime}/{endTime}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getAllRCs/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime(startTime, endTime, srcSite, dstSite, destHierarchyPath, srcHierarchyPath, callback)</td>
    <td style="padding:15px">GET All TCAs
GET op/pfrMonitoringExt/getAllTCAs/{srcSite}/{dstSite}/{startTime}/{endTime}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getAllTCAs/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp(startTime, endTime, siteName, brIp, hierarchyPath, callback)</td>
    <td style="padding:15px">GET All WANInterfaces
GET op/pfrMonitoringExt/getAllWANInterfaces/{startTime}/{endTime}/{siteName}/</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getAllWANInterfaces/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationApplicationArtAnalysis(range, startTime, endTime, appId, dataSourceId, siteId, clientIp, networkAwareType, callback)</td>
    <td style="padding:15px">GET Application Art Analysis
GET op/statisticsService/application/applicationArtAnalysis</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/application/applicationArtAnalysis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationPerformance(startTime, endTime, range, appId, siteId, domainId, pdsId, topN, nwAwareValue, ssidValue, callback)</td>
    <td style="padding:15px">GET Application Performance
GET op/statisticsService/application/performance</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/application/performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationTopNTraffics(range, startTime, endTime, trafficDirection, siteId, dataSourceId, deviceIp, interfaceId, interfaceIndex, sortOrder, appId, topN, clientIp, networkAwareType, dataType, nodeType, callback)</td>
    <td style="padding:15px">GET Application Top N Traffics
GET op/statisticsService/application/topNTraffics</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/application/topNTraffics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites(startTime, endTime, range, trafficDirection, appId, siteId, topN, dataType, callback)</td>
    <td style="padding:15px">GET Application Top N Volumes With Sites Statistics
GET op/statisticsService/application/topNApplic</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/application/topNApplicationsWithSites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis(startTime, endTime, range, type, appId, pdsId, hostId, nwAwareValue, ssidValue, callback)</td>
    <td style="padding:15px">GET Application Traffic Analysis
GET op/statisticsService/application/trafficAnalysis</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/application/trafficAnalysis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationAppVolume(range, startTime, endTime, trafficDirection, siteId, clientIp, deviceIp, interfaceIndex, topN, sortOrder, networkAwareType, dataType, callback)</td>
    <td style="padding:15px">GET Application Volume
GET op/statisticsService/application/appVolume</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/application/appVolume?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationNumberOfUsers(startTime, endTime, range, appId, siteId, pdsId, hostId, nwAwareValue, ssidValue, userId, macAddr, callback)</td>
    <td style="padding:15px">GET Applications Number of Users
GET op/statisticsService/application/numberOfUsers</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/application/numberOfUsers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDeviceCpuUtilSummary(range, startTime, endTime, type, source, rangeString, callback)</td>
    <td style="padding:15px">GET CPU Utilization Summary
GET op/statisticsService/device/cpuUtilSummary</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/device/cpuUtilSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend(ipAddress, range, startTime, endTime, callback)</td>
    <td style="padding:15px">GET CPU Utilization Trend
GET op/statisticsService/device/cpuUtilTrend</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/device/cpuUtilTrend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceClientsDistributionsDistribution(distribution, device, autoAp, ssid, site, siteType, clientType, clientState, direction, timeInterval, startTime, endTime, sort, callback)</td>
    <td style="padding:15px">GET Client Distribution Data
GET op/statisticsService/clients/distributions/{distribution}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/clients/distributions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceClientsDistributions(callback)</td>
    <td style="padding:15px">GET Client Distribution Supported
GET op/statisticsService/clients/distributions</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/clients/distributions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceClusters(datacenter, cluster, children, range, firstResult, maxResults, sort, callback)</td>
    <td style="padding:15px">GET Cluster Details
GET op/statisticsService/clusters</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/clusters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceClustersMetricsMetric(metric, datacenter, cluster, timeInterval, startTime, endTime, callback)</td>
    <td style="padding:15px">GET Cluster Metric Data
GET op/statisticsService/clusters/metrics/{metric}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/clusters/metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceClustersMetrics(datacenter, cluster, timeInterval, startTime, endTime, callback)</td>
    <td style="padding:15px">GET Cluster Metrics Supported
GET op/statisticsService/clusters/metrics</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/clusters/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageStartTimeEndTime(startTime, endTime, srcSiteName, dstSiteName, managedIpBR, tunnelName, srcHierarchyPath, destHierarchyPath, callback)</td>
    <td style="padding:15px">GET DSCP App Usage
GET op/pfrMonitoringExt/getDscpAppUsage/{startTime}/{endTime}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getDscpAppUsage/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId(startTime, endTime, srcSiteId, dstSiteId, vrfIdList, callback)</td>
    <td style="padding:15px">GET DSCP App Usage For Live Topo
GET op/pfrMonitoringExt/getDscpAppUsageForLiveTopo/{startTime}/{en</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getDscpAppUsageForLiveTopo/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints(startTime, endTime, siteName, brIp, dscp, noOfPoints, interfaceName, hierarchyPath, callback)</td>
    <td style="padding:15px">GET DSCP Time Vs Usage Chart
GET op/pfrMonitoringExt/getDSCPTimeVsUsageChart/{startTime}/{endTime}/</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getDSCPTimeVsUsageChart/{pathv1}/{pathv2}/{pathv3}/{pathv4}/{pathv5}/{pathv6}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDatacenters(datacenter, children, firstResult, maxResults, sort, callback)</td>
    <td style="padding:15px">GET Datacenter Details
GET op/statisticsService/datacenters</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/datacenters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric(metric, datacenter, timeInterval, startTime, endTime, callback)</td>
    <td style="padding:15px">GET Datacenter Metric Data
GET op/statisticsService/datacenters/metrics/{metric}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/datacenters/metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDatacentersMetrics(datacenter, timeInterval, startTime, endTime, callback)</td>
    <td style="padding:15px">GET Datacenter Metrics Supported
GET op/statisticsService/datacenters/metrics</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/datacenters/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDeviceAvailability(ipAddress, range, startTime, endTime, callback)</td>
    <td style="padding:15px">GET Device Availability
GET op/statisticsService/device/availability</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/device/availability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDeviceAvailabilityMessage(ipAddress, callback)</td>
    <td style="padding:15px">GET Device Availability Message
GET op/statisticsService/device/availabilityMessage</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/device/availabilityMessage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDeviceAvailabilitySummary(range, startTime, endTime, source, rangeString, callback)</td>
    <td style="padding:15px">GET Device Availability Summary
GET op/statisticsService/device/availabilitySummary</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/device/availabilitySummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDeviceDownMessage(ipAddress, callback)</td>
    <td style="padding:15px">GET Device Down Message
GET op/statisticsService/device/downMessage</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/device/downMessage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDeviceHealthInfo(range, startTime, endTime, source, callback)</td>
    <td style="padding:15px">GET Device Health Info
GET op/statisticsService/device/healthInfo</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/device/healthInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDevicesMetricsMetric(metric, device, lineCard, timeInterval, startTime, endTime, callback)</td>
    <td style="padding:15px">GET Device Metric Data
GET op/statisticsService/devices/metrics/{metric}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/devices/metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDevicesMetrics(device, timeInterval, startTime, endTime, callback)</td>
    <td style="padding:15px">GET Device Metrics Supported
GET op/statisticsService/devices/metrics</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/devices/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDevicePortSummary(ipAddress, type, callback)</td>
    <td style="padding:15px">GET Device Port Summary
GET op/statisticsService/device/portSummary</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/device/portSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDeviceReachabilityStatus(topN, source, type, callback)</td>
    <td style="padding:15px">GET Device Reachability Status
GET op/statisticsService/device/reachabilityStatus</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/device/reachabilityStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDevices(device, firstResult, maxResults, callback)</td>
    <td style="padding:15px">GET Device Resource
GET op/statisticsService/devices</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceHosts(datacenter, cluster, host, children, range, firstResult, maxResults, sort, callback)</td>
    <td style="padding:15px">GET Host Details
GET op/statisticsService/hosts</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/hosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceHostsMetricsMetric(metric, datacenter, cluster, host, timeInterval, startTime, endTime, callback)</td>
    <td style="padding:15px">GET Host Metric Data
GET op/statisticsService/hosts/metrics/{metric}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/hosts/metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceHostsMetrics(datacenter, cluster, host, timeInterval, startTime, endTime, callback)</td>
    <td style="padding:15px">GET Host Metrics Supported
GET op/statisticsService/hosts/metrics</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/hosts/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceAvailability(startTime, endTime, range, ipAddress, ifName, callback)</td>
    <td style="padding:15px">GET Interface Availability
GET op/statisticsService/interface/availability</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/availability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceAvailMessage(ipAddress, ifName, callback)</td>
    <td style="padding:15px">GET Interface Availability Message
GET op/statisticsService/interface/availMessage</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/availMessage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceAvailabilitySummary(startTime, endTime, range, rangeString, source, callback)</td>
    <td style="padding:15px">GET Interface Availability Summary
GET op/statisticsService/interface/availabilitySummary</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/availabilitySummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceDetails(ipAddress, ifName, callback)</td>
    <td style="padding:15px">GET Interface Details
GET op/statisticsService/interface/details</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceDiscards(startTime, endTime, range, ipAddress, ifName, callback)</td>
    <td style="padding:15px">GET Interface Discards
GET op/statisticsService/interface/discards</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/discards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceDownMessage(ipAddress, callback)</td>
    <td style="padding:15px">GET Interface Down Message
GET op/statisticsService/interface/downMessage</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/downMessage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceErrors(startTime, endTime, range, ipAddress, ifName, callback)</td>
    <td style="padding:15px">GET Interface Errors
GET op/statisticsService/interface/errors</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/errors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfacesMetricsMetric(metric, device, ifName, timeInterval, startTime, endTime, isFullHierarchy, direction, qosRateField, trafficDirection, sortOrder, networkAwareType, appId, siteId, dataType, dscp, metricDataType, callback)</td>
    <td style="padding:15px">GET Interface Metric Data
GET op/statisticsService/interfaces/metrics/{metric}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interfaces/metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfacesMetrics(device, ifName, timeInterval, startTime, endTime, callback)</td>
    <td style="padding:15px">GET Interface Metrics Supported
GET op/statisticsService/interfaces/metrics</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interfaces/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaces(device, ifName, firstResult, maxResults, sort, callback)</td>
    <td style="padding:15px">GET Interface Resource
GET op/statisticsService/interfaces</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceStatusSummary(startTime, endTime, range, rangeString, source, callback)</td>
    <td style="padding:15px">GET Interface Status Summary
GET op/statisticsService/interface/statusSummary</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/statusSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceUtilSummary(startTime, endTime, range, rangeString, source, utilType, callback)</td>
    <td style="padding:15px">GET Interface Utilization Summary
GET op/statisticsService/interface/utilSummary</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/utilSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceUtil(startTime, endTime, range, ipAddress, ifName, type, callback)</td>
    <td style="padding:15px">GET Interface Utilizations
GET op/statisticsService/interface/util</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/util?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpDeviceRestDeviceLispMapSummaryDeviceIpStartTimeEndTime(deviceIp, startTime, endTime, callback)</td>
    <td style="padding:15px">GET Lisp Map Summary Trend
GET op/device-rest/DeviceLispMapSummary/{deviceIp}/{startTime}/{endTime}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/device-rest/DeviceLispMapSummary/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend(ipAddress, range, startTime, endTime, callback)</td>
    <td style="padding:15px">GET Memory Utilization Trend
GET op/statisticsService/device/memoryUtilTrend</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/device/memoryUtilTrend?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetOutStandingIMEsForSiteStartTimeEndTimeSrcSite(startTime, endTime, srcSite, siteHierarchyPath, callback)</td>
    <td style="padding:15px">GET OutStanding IMEs For Site
GET op/pfrMonitoringExt/getOutStandingIMEsForSite/{startTime}/{endTim</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getOutStandingIMEsForSite/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords(startTime, endTime, groupId, maxrecords, callback)</td>
    <td style="padding:15px">GET Reachable UCI Devcies
GET op/device-rest/UciDeviceReachability/{groupId}/{startTime}/{endTime}/</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/device-rest/UciDeviceReachability/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceResourceTypes(callback)</td>
    <td style="padding:15px">GET Resource Types Supported
GET op/statisticsService/resourceTypes</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/resourceTypes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetSiteAppHealthStartTimeEndTime(startTime, endTime, callback)</td>
    <td style="padding:15px">GET Site App Health
GET op/pfrMonitoringExt/getSiteAppHealth/{startTime}/{endTime}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getSiteAppHealth/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceSystemHealth(startTime, endTime, range, callback)</td>
    <td style="padding:15px">GET System Health
GET op/statisticsService/system/health</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/system/health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceSystemInfo(callback)</td>
    <td style="padding:15px">GET System Information
GET op/statisticsService/system/info</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/system/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationsAppHealthCrossing(startTime, endTime, range, siteId, callback)</td>
    <td style="padding:15px">GET Threshold Violated Applications
GET op/statisticsService/applications/appHealthCrossing</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/applications/appHealthCrossing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationTopNHosts(startTime, endTime, range, appId, siteId, domainId, pdsId, topN, trafficDir, ipAddress, interfaceName, interfaceId, sortOrder, nwAwareValue, ssidValue, callback)</td>
    <td style="padding:15px">GET Top N Application Hosts
GET op/statisticsService/application/topNHosts</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/application/topNHosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationsPerformance(attribute, startTime, endTime, range, trafficDir, siteId, pdsId, dscp, appId, hostId, deviceIp, interfaceId, interfaceIndex, userName, macAddr, nAwareValue, topN, sortOrder, type, ssid, domainId, dataType, nodeType, controllerId, apId, viewType, siteMapId, isFabric, callback)</td>
    <td style="padding:15px">GET Top N Applications
GET op/statisticsService/applications/performance</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/applications/performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationsOvertime(startTime, endTime, range, trafficType, pdsId, interfaceName, deviceIp, interfaceIndex, sortOrder, rownum, hostId, siteId, domainId, type, nwAwareValue, ssidValue, appId, dataType, isFabric, callback)</td>
    <td style="padding:15px">GET Top N Applications Over Time
GET op/statisticsService/applications/overtime</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/applications/overtime?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS(startTime, endTime, range, topN, source, ifIndex, callback)</td>
    <td style="padding:15px">GET Top N Class Map QOS
GET op/statisticsService/interface/topNClassMapQOS</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/topNClassMapQOS?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDeviceTopNCPU(range, startTime, endTime, topN, source, callback)</td>
    <td style="padding:15px">GET Top N Device CPU Utilization
GET op/statisticsService/device/topNCPU</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/device/topNCPU?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn(groupId, startTime, endTime, topn, source, dashlet, callback)</td>
    <td style="padding:15px">GET Top N Device Cache
GET op/device-rest/TopNDeviceCache/ALL/{groupId}/{startTime}/{endTime}/{topn}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/device-rest/TopNDeviceCache/ALL/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDeviceTopNMemory(range, startTime, endTime, topN, source, callback)</td>
    <td style="padding:15px">GET Top N Device Memory Utilization
GET op/statisticsService/device/topNMemory</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/device/topNMemory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceDeviceTopNTemp(range, startTime, endTime, topN, source, type, callback)</td>
    <td style="padding:15px">GET Top N Device Temperature
GET op/statisticsService/device/topNTemp</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/device/topNTemp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors(startTime, endTime, range, topN, source, type, callback)</td>
    <td style="padding:15px">GET Top N Interface Errors
GET op/statisticsService/interface/topNErrors</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/topNErrors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil(startTime, endTime, range, topN, source, utilType, callback)</td>
    <td style="padding:15px">GET Top N Interface Utilization
GET op/statisticsService/interface/topNUtil</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/topNUtil?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues(startTime, endTime, range, topN, source, callback)</td>
    <td style="padding:15px">GET Top N WAN Interface Issues
GET op/statisticsService/interface/topNWanIssues</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/topNWanIssues?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil(startTime, endTime, range, topN, source, callback)</td>
    <td style="padding:15px">GET Top N WAN Interface Utilization
GET op/statisticsService/interface/topNWanUtil</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/interface/topNWanUtil?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum(startTime, endTime, deviceIp, rownum, interfaceName, callback)</td>
    <td style="padding:15px">GET TopNApplication Usage Over Time
GET op/pfrMonitoringExt/getTopNApplicationUsageOverTimeOld/{sta</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getTopNApplicationUsageOverTimeOld/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum(startTime, endTime, deviceIp, rownum, interfaceName, callback)</td>
    <td style="padding:15px">GET TopNApplication Usage Over Time PfR
GET op/pfrMonitoringExt/getTopNApplicationUsageOverTime/{st</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getTopNApplicationUsageOverTime/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationsAppCount(startTime, endTime, range, siteId, callback)</td>
    <td style="padding:15px">GET Total Application Count
GET op/statisticsService/applications/appCount</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/applications/appCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords(startTime, endTime, maxrecords, groupId, callback)</td>
    <td style="padding:15px">GET Unreachable UCI Devcies
GET op/device-rest/UnReachableUciDevice/{groupId}/{startTime}/{endTime}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/device-rest/UnReachableUciDevice/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceVms(datacenter, cluster, host, vm, children, range, firstResult, maxResults, sort, callback)</td>
    <td style="padding:15px">GET VM Details
GET op/statisticsService/vms</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/vms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceVmsMetricsMetric(metric, datacenter, cluster, host, vm, timeInterval, startTime, endTime, callback)</td>
    <td style="padding:15px">GET VM Metric Data
GET op/statisticsService/vms/metrics/{metric}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/vms/metrics/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceVmsMetrics(datacenter, cluster, host, vm, timeInterval, startTime, endTime, callback)</td>
    <td style="padding:15px">GET VM Metrics Supported
GET op/statisticsService/vms/metrics</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/vms/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection(startTime, endTime, brIpAddr, direction, interfaceName, callback)</td>
    <td style="padding:15px">GET WAN Utilization Over TimeRate For Link
GET op/pfrMonitoringExt/getWanUtilizationOverTimeRateFor</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getWanUtilizationOverTimeRateForLink/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection(startTime, endTime, brIpAddr, direction, interfaceName, callback)</td>
    <td style="padding:15px">GET WanUtilization Over Time For Link
GET op/pfrMonitoringExt/getWanUtilizationOverTimeForLink/{sta</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getWanUtilizationOverTimeForLink/{pathv1}/{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts(startTime, endTime, range, appId, siteId, domainId, pdsId, attribute, sortOrder, topN, nwAwareValue, ssidValue, userId, macAddr, callback)</td>
    <td style="padding:15px">GET Worst N Application Hosts
GET op/statisticsService/application/worstNHosts</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/application/worstNHosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpStatisticsServiceApplicationWorstNSites(startTime, endTime, range, appId, siteId, domainId, pdsId, attribute, topN, nwAwareValue, ssidValue, callback)</td>
    <td style="padding:15px">GET Worst N Application Locations
GET op/statisticsService/application/worstNSites</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/statisticsService/application/worstNSites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpPfrMonitoringExtGetDSCPvsAllTCAViaPostStartTimeEndTime(startTime, endTime, callback)</td>
    <td style="padding:15px">POST DSCP vs All TCA
POST op/pfrMonitoringExt/getDSCPvsAllTCAViaPost/{startTime}/{endTime}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getDSCPvsAllTCAViaPost/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpPfrMonitoringExtGetDSCPvsUrTCAViaPostStartTimeEndTime(startTime, endTime, callback)</td>
    <td style="padding:15px">POST DSCP vs UrTCA
POST op/pfrMonitoringExt/getDSCPvsUrTCAViaPost/{startTime}/{endTime}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getDSCPvsUrTCAViaPost/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpPfrMonitoringExtGetMaxDelayVsDSCPChartReportsViaPostStartTimeEndTime(startTime, endTime, callback)</td>
    <td style="padding:15px">POST Max Delay Vs DSCP Chart Reports
POST op/pfrMonitoringExt/getMaxDelayVsDSCPChartReportsViaPost/</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getMaxDelayVsDSCPChartReportsViaPost/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpPfrMonitoringExtGetMaxJitterVsDSCPChartReportsViaPostStartTimeEndTime(startTime, endTime, callback)</td>
    <td style="padding:15px">POST Max Jitter Vs DSCP Chart Reports
POST op/pfrMonitoringExt/getMaxJitterVsDSCPChartReportsViaPos</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getMaxJitterVsDSCPChartReportsViaPost/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpPfrMonitoringExtGetMaxPacketLossVsDSCPChartReportsViaPostStartTimeEndTime(startTime, endTime, callback)</td>
    <td style="padding:15px">POST Max Packet Loss Vs DSCP Chart Reports
POST op/pfrMonitoringExt/getMaxPacketLossVsDSCPChartRepo</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getMaxPacketLossVsDSCPChartReportsViaPost/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpPfrMonitoringExtGetPfrEventListViaPostStartTimeEndTime(endTime, startTime, callback)</td>
    <td style="padding:15px">POST Pfr Event List
POST op/pfrMonitoringExt/getPfrEventListViaPost/{startTime}/{endTime}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getPfrEventListViaPost/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpPfrMonitoringExtGetServiceProviderUsageVsDSCPChartStartTimeEndTimeTopN(startTime, endTime, topN, callback)</td>
    <td style="padding:15px">POST Service Provider Usage Vs DSCP Chart
POST op/pfrMonitoringExt/getServiceProviderUsageVsDSCPCha</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/pfrMonitoringExt/getServiceProviderUsageVsDSCPChart/{pathv1}/{pathv2}/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpSettingsServersFtpServersServerName(serverName, callback)</td>
    <td style="padding:15px">DELETE WLAN FTP Server
DELETE op/settings/servers/ftpServers/{serverName}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/servers/ftpServers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpSettingsNotificationAlarmCategories(callback)</td>
    <td style="padding:15px">GET Alarms notification settings
GET op/settings/notification/alarmCategories</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/notification/alarmCategories?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpSettingsInventoryConfigArchiveBasic(callback)</td>
    <td style="padding:15px">GET Basic parameters for the configuration archive
GET op/settings/inventory/configArchiveBasic</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/inventory/configArchiveBasic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpSettingsAlarmsAlarmDeletionPeriods(callback)</td>
    <td style="padding:15px">GET Configuration for Automatic Deletion Periods of Alarms
GET op/settings/alarms/alarmDeletionPeri</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/alarms/alarmDeletionPeriods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpSettingsLoggingGeneralLoggingOptions(callback)</td>
    <td style="padding:15px">GET Current general logging options
GET op/settings/logging/generalLoggingOptions</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/logging/generalLoggingOptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpSettingsDataPruneDataConfigTableName(tableName, callback)</td>
    <td style="padding:15px">GET Data Pruning Configuration
GET op/settings/data/pruneDataConfig/{tableName}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/data/pruneDataConfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpSettingsDataPruneDataConfigsByGroupCategory(category, callback)</td>
    <td style="padding:15px">GET Data Pruning Configuration for Category
GET op/settings/data/pruneDataConfigsByGroup/{category}</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/data/pruneDataConfigsByGroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpSettingsDataDataRetentionPeriods(callback)</td>
    <td style="padding:15px">GET Data Retention Periods
GET op/settings/data/dataRetentionPeriods</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/data/dataRetentionPeriods?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpSettingsServersFtpServers(callback)</td>
    <td style="padding:15px">GET List of WLAN FTP Servers
GET op/settings/servers/ftpServers</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/servers/ftpServers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpSettingsLoggingLogFileNames(callback)</td>
    <td style="padding:15px">GET Log file names
GET op/settings/logging/logFileNames</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/logging/logFileNames?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpSettingsNotificationMailServer(callback)</td>
    <td style="padding:15px">GET Mail Server settings
GET op/settings/notification/mailServer</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/notification/mailServer?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpSettingsLoggingSnmpSettings(callback)</td>
    <td style="padding:15px">GET SNMP settings
GET op/settings/logging/snmpSettings</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/logging/snmpSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpSettingsAlarmsSeverityAutoClearConfig(callback)</td>
    <td style="padding:15px">GET Severity and Auto Clear Config
GET op/settings/alarms/severityAutoClearConfig</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/alarms/severityAutoClearConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpSettingsLoggingSysLogSettings(callback)</td>
    <td style="padding:15px">GET System log settings
GET op/settings/logging/sysLogSettings</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/logging/sysLogSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpSettingsLoggingSysLogSettings(callback)</td>
    <td style="padding:15px">POST Add WLAN FTP Server
POST op/settings/servers/ftpServers</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/logging/sysLogSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpSettingsLoggingSysLogSettings(callback)</td>
    <td style="padding:15px">PUT Update system log settings
PUT op/settings/logging/sysLogSettings</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/settings/logging/sysLogSettings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataUserDefinedFieldDefinition(callback)</td>
    <td style="padding:15px">GET User Defined Field Definition
GET data/UserDefinedFieldDefinition</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/UserDefinedFieldDefinition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpUdfServiceDeleteUserTagDefinition(body, callback)</td>
    <td style="padding:15px">PUT Delete User Defined Field Definition
PUT op/udfService/deleteUserTagDefinition</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/udfService/deleteUserTagDefinition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpUdfServiceImportUserTagDefinition(body, callback)</td>
    <td style="padding:15px">PUT Import User Defined Field Definition
PUT op/udfService/importUserTagDefinition</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/udfService/importUserTagDefinition?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpUserManagementUsers(callback)</td>
    <td style="padding:15px">DELETE Delete user
DELETE op/userManagement/users</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/userManagement/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpUserManagementUserGroups(callback)</td>
    <td style="padding:15px">GET Get users
GET op/userManagement/users</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/userManagement/userGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpUserManagementUserGroups(callback)</td>
    <td style="padding:15px">POST Create user
POST op/userManagement/users</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/userManagement/userGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpUserManagementUserGroups(callback)</td>
    <td style="padding:15px">PUT Update user
PUT op/userManagement/users</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/userManagement/userGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpNfvGetBridges(deviceIp, callback)</td>
    <td style="padding:15px">GET Get Bridges
GET op/nfv/getBridges</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/getBridges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpNfvGetDeployments(deviceIp, callback)</td>
    <td style="padding:15px">GET Get Deployments
GET op/nfv/getDeployments</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/getDeployments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpNfvDeviceDetails(deviceIp, callback)</td>
    <td style="padding:15px">GET Get Device Details
GET op/nfv/deviceDetails</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/deviceDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpNfvGetOvsBridges(deviceIp, callback)</td>
    <td style="padding:15px">GET Get OVS Bridges
GET op/nfv/getOvsBridges</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/getOvsBridges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpNfvGetServices(deviceIp, callback)</td>
    <td style="padding:15px">GET Get Services
GET op/nfv/getServices</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/getServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpNfvAddBridge(body, callback)</td>
    <td style="padding:15px">POST Add Bridge
POST op/nfv/addBridge</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/addBridge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpVirtualAddDevice(body, callback)</td>
    <td style="padding:15px">POST Add Device
POST op/virtual/addDevice</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/virtual/addDevice?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpNfvCreateOvsBridge(body, callback)</td>
    <td style="padding:15px">POST Create OVS Bridge
POST op/nfv/createOvsBridge</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/createOvsBridge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpNfvDeleteBridge(body, callback)</td>
    <td style="padding:15px">POST Delete Bridge
POST op/nfv/deleteBridge</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/deleteBridge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpNfvDeleteOvsBridge(body, callback)</td>
    <td style="padding:15px">POST Delete OVS Bridge
POST op/nfv/deleteOvsBridge</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/deleteOvsBridge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpNfvDeploy(body, callback)</td>
    <td style="padding:15px">POST Deploy Service
POST op/nfv/deploy</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/deploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpNfvRegister(body, callback)</td>
    <td style="padding:15px">POST Register Service
POST op/nfv/register</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/register?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpNfvUndeploy(body, callback)</td>
    <td style="padding:15px">POST Un-Deploy Service
POST op/nfv/undeploy</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/undeploy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpNfvUnregister(body, callback)</td>
    <td style="padding:15px">POST Un-Register Service
POST op/nfv/unregister</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/unregister?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpNfvUpdateBridge(body, callback)</td>
    <td style="padding:15px">PUT Update Bridge
PUT op/nfv/updateBridge</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/updateBridge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpNfvUpdateOvsBridge(body, callback)</td>
    <td style="padding:15px">PUT Update OVS Bridge
PUT op/nfv/updateOvsBridge</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/nfv/updateOvsBridge?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataVDAssociatedAccessPoints(callback)</td>
    <td style="padding:15px">GET Access Points associated with current Virtual Domain
GET data/VDAssociatedAccessPoints</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/VDAssociatedAccessPoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataVDAssociatedDynamicGroups(callback)</td>
    <td style="padding:15px">GET Dynamic Groups associated with current Virtual Domain
GET data/VDAssociatedDynamicGroups</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/VDAssociatedDynamicGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataVDAssociatedGroups(callback)</td>
    <td style="padding:15px">GET Groups associated with current Virtual Domain
GET data/VDAssociatedGroups</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/VDAssociatedGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataVDAssociatedDevices(callback)</td>
    <td style="padding:15px">GET Network Devices associated with current Virtual Domain
GET data/VDAssociatedDevices</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/VDAssociatedDevices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataVDAssociatedSiteMaps(callback)</td>
    <td style="padding:15px">GET Site Maps associated with current Virtual Domain
GET data/VDAssociatedSiteMaps</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/VDAssociatedSiteMaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataVDAssociatedVirtualElements(callback)</td>
    <td style="padding:15px">GET Virtual Elements associated with current Virtual Domain
GET data/VDAssociatedVirtualElements</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/VDAssociatedVirtualElements?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpVd(domainFQN, callback)</td>
    <td style="padding:15px">DELETE Delete Virtual Domain
DELETE op/vd</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpVd(parentDomainName, callback)</td>
    <td style="padding:15px">GET List Virtual Domains
GET op/vd</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpVd(body, callback)</td>
    <td style="padding:15px">POST Create Virtual Domain
POST op/vd</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpVd(domainFQN, callback)</td>
    <td style="padding:15px">PUT Update Virtual Domain
PUT op/vd</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/vd?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpImageId(callback)</td>
    <td style="padding:15px">DELETE Delete Image by id
DELETE op/image/id</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/image/id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpImageListId(idNum, callback)</td>
    <td style="padding:15px">GET List Image by id
GET op/image/list/id</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/image/list/id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpImageList(callback)</td>
    <td style="padding:15px">GET List Images
GET op/image/list</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/image/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpImageListFamily(familyName, callback)</td>
    <td style="padding:15px">GET List Images by family
GET op/image/list/family</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/image/list/family?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpImageListPhysical(callback)</td>
    <td style="padding:15px">GET List Physical Images
GET op/image/list/physical</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/image/list/physical?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4OpImageListVirtual(callback)</td>
    <td style="padding:15px">GET List Virtual Images
GET op/image/list/virtual</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/image/list/virtual?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpImageAdd(body, callback)</td>
    <td style="padding:15px">POST Add Image
POST op/image/add</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/image/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpImageDeployConfigure(body, callback)</td>
    <td style="padding:15px">POST Deploy Configuration
POST op/image/deployConfigure</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/image/deployConfigure?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataWlanProfiles(callback)</td>
    <td style="padding:15px">GET Wlan Profiles
GET data/WlanProfiles</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/WlanProfiles?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataWlanTemplates(callback)</td>
    <td style="padding:15px">GET Wlan Templates
GET data/WlanTemplates</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/WlanTemplates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpWlanProvisioningApGroup(controllerId, controllerName, apGroupName, callback)</td>
    <td style="padding:15px">DELETE Delete AP Group
DELETE op/wlanProvisioning/apGroup</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/wlanProvisioning/apGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpWlanProvisioningInterface(controllerId, controllerName, interfaceName, callback)</td>
    <td style="padding:15px">DELETE Delete Interface
DELETE op/wlanProvisioning/interface</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/wlanProvisioning/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpWlanProvisioningInterfaceGroup(controllerId, controllerName, interfaceGroupName, callback)</td>
    <td style="padding:15px">DELETE Delete Interface Group
DELETE op/wlanProvisioning/interfaceGroup</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/wlanProvisioning/interfaceGroup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpWlanProvisioningWlanProfile(controllerId, controllerName, wlanProfileName, callback)</td>
    <td style="padding:15px">DELETE Delete WLAN Profile
DELETE op/wlanProvisioning/wlanProfile</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/wlanProvisioning/wlanProfile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteWebacsApiV4OpWlanProvisioningWlanTemplate(callback)</td>
    <td style="padding:15px">DELETE Delete Wlan Template
DELETE op/wlanProvisioning/wlanTemplate</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/wlanProvisioning/wlanTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postWebacsApiV4OpWlanProvisioningWlanTemplate(callback)</td>
    <td style="padding:15px">POST Create Wlan Template
POST op/wlanProvisioning/wlanTemplate</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/wlanProvisioning/wlanTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putWebacsApiV4OpWlanProvisioningDeployTemplate(callback)</td>
    <td style="padding:15px">PUT Modify Wlan Template
PUT op/wlanProvisioning/wlanTemplate</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/op/wlanProvisioning/deployTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataAutoApRadioDetails(callback)</td>
    <td style="padding:15px">GET Autonomous AP Radio Details
GET data/AutoApRadioDetails</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/AutoApRadioDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataClientStats(callback)</td>
    <td style="padding:15px">GET Client Statistics
GET data/ClientStats</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/ClientStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataHistoricalClientStats(callback)</td>
    <td style="padding:15px">GET Historical Client Statistics
GET data/HistoricalClientStats</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/HistoricalClientStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataHistoricalRFCounters(callback)</td>
    <td style="padding:15px">GET Historical Radio Interface 802.11 Counters in Last 24 Hours
GET data/HistoricalRFCounters</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/HistoricalRFCounters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataHistoricalRFLoadStats(callback)</td>
    <td style="padding:15px">GET Historical Radio Interface Load Statistics in Last 24 Hours
GET data/HistoricalRFLoadStats</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/HistoricalRFLoadStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataHistoricalRFStats(callback)</td>
    <td style="padding:15px">GET Historical Radio Interface Statistics in Last 24 Hours
GET data/HistoricalRFStats</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/HistoricalRFStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataHistoricalWLCCPUUtilizations(callback)</td>
    <td style="padding:15px">GET Historical Wireless Lan Controller CPU Utilizations
GET data/HistoricalWLCCPUUtilizations</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/HistoricalWLCCPUUtilizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataHistoricalWLCMemUtilizations(callback)</td>
    <td style="padding:15px">GET Historical Wireless Lan Controller Memory Utilizations
GET data/HistoricalWLCMemUtilizations</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/HistoricalWLCMemUtilizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataRadioDetails(callback)</td>
    <td style="padding:15px">GET Lightweight AP Radio Details
GET data/RadioDetails</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/RadioDetails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataRFCounters(callback)</td>
    <td style="padding:15px">GET Radio Interface 802.11 Counters
GET data/RFCounters</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/RFCounters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataRFLoadStats(callback)</td>
    <td style="padding:15px">GET Radio Interface Load Statistics
GET data/RFLoadStats</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/RFLoadStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataRFStats(callback)</td>
    <td style="padding:15px">GET Radio Interface Statistics
GET data/RFStats</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/RFStats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataRadios(callback)</td>
    <td style="padding:15px">GET Radio Summary
GET data/Radios</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/Radios?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataThirdpartyAccessPoints(callback)</td>
    <td style="padding:15px">GET Third Party Access Point Summary
GET data/ThirdpartyAccessPoints</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/ThirdpartyAccessPoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataAccessPoints(callback)</td>
    <td style="padding:15px">GET Wireless Access Point Summary
GET data/AccessPoints</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/AccessPoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataWLCCPUUtilizations(callback)</td>
    <td style="padding:15px">GET Wireless Lan Controller CPU Utlizations
GET data/WLCCPUUtilizations</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/WLCCPUUtilizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataWLCMemoryUtilizations(callback)</td>
    <td style="padding:15px">GET Wireless Lan Controller Memory Utilizations
GET data/WLCMemoryUtilizations</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/WLCMemoryUtilizations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataWlanControllers(callback)</td>
    <td style="padding:15px">GET Wireless Lan Controller Summary
GET data/WlanControllers</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/WlanControllers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getWebacsApiV4DataDevices(callback)</td>
    <td style="padding:15px">GET data Devices</td>
    <td style="padding:15px">{base_path}/{version}/webacs/api/v4/data/Devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
