# Cisco Prime

Vendor: Cisco
Homepage: https://www.cisco.com/

Product: Cisco Prime
Product Page: https://www.cisco.com/c/en/us/products/cloud-systems-management/prime-infrastructure/index.html

## Introduction
We classify Cisco Prime into the Service Assurance domain as Cisco Prime monitors and gathers statistical information from the network. 

## Why Integrate
The Cisco Prime adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Cisco Prime. With this adapter you have the ability to perform operations such as:

- Configure and Manage devices
- Get Alarms
- Acknowledge Alarms

## Additional Product Documentation
The [API documents for Cisco Prime](https://www.cisco.com/c/en/us/support/cloud-systems-management/prime-infrastructure/products-programming-reference-guides-list.html)