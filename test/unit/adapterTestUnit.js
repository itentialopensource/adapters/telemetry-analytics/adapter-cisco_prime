/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cisco_prime',
      type: 'CiscoPrime',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const CiscoPrime = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Cisco_prime Adapter Test', () => {
  describe('CiscoPrime Class Tests', () => {
    const a = new CiscoPrime(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('cisco_prime'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('cisco_prime'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('CiscoPrime', pronghornDotJson.export);
          assert.equal('Cisco_prime', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-cisco_prime', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('cisco_prime'));
          assert.equal('CiscoPrime', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-cisco_prime', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-cisco_prime', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#deleteWebacsApiV4OpAaaTacacsPlusServer - errors', () => {
      it('should have a deleteWebacsApiV4OpAaaTacacsPlusServer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpAaaTacacsPlusServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpAaaTacacsPlusServer - errors', () => {
      it('should have a getWebacsApiV4OpAaaTacacsPlusServer function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpAaaTacacsPlusServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpAaaTacacsPlusServer - errors', () => {
      it('should have a postWebacsApiV4OpAaaTacacsPlusServer function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpAaaTacacsPlusServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpAaaTacacsPlusServer - errors', () => {
      it('should have a putWebacsApiV4OpAaaTacacsPlusServer function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpAaaTacacsPlusServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataApOnboardingProfile - errors', () => {
      it('should have a getWebacsApiV4DataApOnboardingProfile function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataApOnboardingProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpApOnboardingProfile - errors', () => {
      it('should have a deleteWebacsApiV4OpApOnboardingProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpApOnboardingProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpApOnboardingProfile - errors', () => {
      it('should have a postWebacsApiV4OpApOnboardingProfile function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpApOnboardingProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpApOnboardingProfile - errors', () => {
      it('should have a putWebacsApiV4OpApOnboardingProfile function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpApOnboardingProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataApiHealthRecords - errors', () => {
      it('should have a getWebacsApiV4DataApiHealthRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataApiHealthRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataApiResponseTimeSummary - errors', () => {
      it('should have a getWebacsApiV4DataApiResponseTimeSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataApiResponseTimeSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpApiHealthCallsPerClient - errors', () => {
      it('should have a getWebacsApiV4OpApiHealthCallsPerClient function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpApiHealthCallsPerClient === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpApiHealthRequestCountTrend - errors', () => {
      it('should have a getWebacsApiV4OpApiHealthRequestCountTrend function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpApiHealthRequestCountTrend === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpApiHealthResponseTimeTrend - errors', () => {
      it('should have a getWebacsApiV4OpApiHealthResponseTimeTrend function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpApiHealthResponseTimeTrend === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpApiHealthServiceNameList - errors', () => {
      it('should have a getWebacsApiV4OpApiHealthServiceNameList function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpApiHealthServiceNameList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpApServiceRxNeighbors - errors', () => {
      it('should have a getWebacsApiV4OpApServiceRxNeighbors function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpApServiceRxNeighbors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apId', (done) => {
        try {
          a.getWebacsApiV4OpApServiceRxNeighbors(null, (data, error) => {
            try {
              const displayE = 'apId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpApServiceRxNeighbors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpApServiceDeleteById - errors', () => {
      it('should have a putWebacsApiV4OpApServiceDeleteById function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpApServiceDeleteById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpApServiceDeleteAp - errors', () => {
      it('should have a putWebacsApiV4OpApServiceDeleteAp function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpApServiceDeleteAp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpApServiceAccessPoint - errors', () => {
      it('should have a putWebacsApiV4OpApServiceAccessPoint function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpApServiceAccessPoint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpRogueApClassification - errors', () => {
      it('should have a putWebacsApiV4OpRogueApClassification function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpRogueApClassification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpAlarmsAcknowledge - errors', () => {
      it('should have a putWebacsApiV4OpAlarmsAcknowledge function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpAlarmsAcknowledge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpAlarmsAnnotate - errors', () => {
      it('should have a putWebacsApiV4OpAlarmsAnnotate function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpAlarmsAnnotate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpAlarmsClear - errors', () => {
      it('should have a putWebacsApiV4OpAlarmsClear function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpAlarmsClear === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpAlarmsClearByEventType - errors', () => {
      it('should have a putWebacsApiV4OpAlarmsClearByEventType function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpAlarmsClearByEventType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpAlarmsUnacknowledge - errors', () => {
      it('should have a putWebacsApiV4OpAlarmsUnacknowledge function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpAlarmsUnacknowledge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataApplications - errors', () => {
      it('should have a getWebacsApiV4DataApplications function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataApplications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataCliTemplate - errors', () => {
      it('should have a getWebacsApiV4DataCliTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataCliTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpCliTemplateConfigurationDeleteTemplate - errors', () => {
      it('should have a deleteWebacsApiV4OpCliTemplateConfigurationDeleteTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpCliTemplateConfigurationDeleteTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpCliTemplateConfigurationFolder - errors', () => {
      it('should have a deleteWebacsApiV4OpCliTemplateConfigurationFolder function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpCliTemplateConfigurationFolder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpCliTemplateConfigurationFolder - errors', () => {
      it('should have a getWebacsApiV4OpCliTemplateConfigurationFolder function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpCliTemplateConfigurationFolder === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpCliTemplateConfigurationDeviceTypes - errors', () => {
      it('should have a getWebacsApiV4OpCliTemplateConfigurationDeviceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpCliTemplateConfigurationDeviceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpCliTemplateConfigurationDeviceTypes - errors', () => {
      it('should have a postWebacsApiV4OpCliTemplateConfigurationDeviceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpCliTemplateConfigurationDeviceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpCliTemplateConfigurationUpload - errors', () => {
      it('should have a postWebacsApiV4OpCliTemplateConfigurationUpload function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpCliTemplateConfigurationUpload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpCliTemplateConfigurationDeployTemplateThroughJob - errors', () => {
      it('should have a putWebacsApiV4OpCliTemplateConfigurationDeployTemplateThroughJob function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpCliTemplateConfigurationDeployTemplateThroughJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpCliTemplateConfigurationTemplate - errors', () => {
      it('should have a putWebacsApiV4OpCliTemplateConfigurationTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpCliTemplateConfigurationTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataClientCounts - errors', () => {
      it('should have a getWebacsApiV4DataClientCounts function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataClientCounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataClientSessions - errors', () => {
      it('should have a getWebacsApiV4DataClientSessions function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataClientSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataClientTraffics - errors', () => {
      it('should have a getWebacsApiV4DataClientTraffics function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataClientTraffics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalClientCounts - errors', () => {
      it('should have a getWebacsApiV4DataHistoricalClientCounts function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataHistoricalClientCounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalClientTraffics - errors', () => {
      it('should have a getWebacsApiV4DataHistoricalClientTraffics function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataHistoricalClientTraffics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpComplianceCheck - errors', () => {
      it('should have a getWebacsApiV4OpComplianceCheck function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpComplianceCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobName', (done) => {
        try {
          a.getWebacsApiV4OpComplianceCheck(null, (data, error) => {
            try {
              const displayE = 'jobName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpComplianceCheck', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpCompliancePolicy - errors', () => {
      it('should have a postWebacsApiV4OpCompliancePolicy function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpCompliancePolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpComplianceProfile - errors', () => {
      it('should have a postWebacsApiV4OpComplianceProfile function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpComplianceProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpCompliancePolicyImport - errors', () => {
      it('should have a postWebacsApiV4OpCompliancePolicyImport function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpCompliancePolicyImport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpComplianceRun - errors', () => {
      it('should have a postWebacsApiV4OpComplianceRun function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpComplianceRun === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataBulkSanitizedConfigArchives - errors', () => {
      it('should have a getWebacsApiV4DataBulkSanitizedConfigArchives function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataBulkSanitizedConfigArchives === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataBulkUnsanitizedConfigArchives - errors', () => {
      it('should have a getWebacsApiV4DataBulkUnsanitizedConfigArchives function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataBulkUnsanitizedConfigArchives === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataConfigArchives - errors', () => {
      it('should have a getWebacsApiV4DataConfigArchives function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataConfigArchives === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataConfigVersions - errors', () => {
      it('should have a getWebacsApiV4DataConfigVersions function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataConfigVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpConfigArchiveServiceConfigDiffDevice - errors', () => {
      it('should have a getWebacsApiV4OpConfigArchiveServiceConfigDiffDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpConfigArchiveServiceConfigDiffDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getWebacsApiV4OpConfigArchiveServiceConfigDiffDevice(null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpConfigArchiveServiceConfigDiffDevice', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpConfigArchiveServiceExtractSanitizedFile - errors', () => {
      it('should have a getWebacsApiV4OpConfigArchiveServiceExtractSanitizedFile function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpConfigArchiveServiceExtractSanitizedFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getWebacsApiV4OpConfigArchiveServiceExtractSanitizedFile(null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpConfigArchiveServiceExtractSanitizedFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpConfigArchiveServiceExtractUnsanitizedFile - errors', () => {
      it('should have a getWebacsApiV4OpConfigArchiveServiceExtractUnsanitizedFile function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpConfigArchiveServiceExtractUnsanitizedFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fileId', (done) => {
        try {
          a.getWebacsApiV4OpConfigArchiveServiceExtractUnsanitizedFile(null, (data, error) => {
            try {
              const displayE = 'fileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpConfigArchiveServiceExtractUnsanitizedFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpCredentialProfilesManagementCredentialProfiles - errors', () => {
      it('should have a deleteWebacsApiV4OpCredentialProfilesManagementCredentialProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpCredentialProfilesManagementCredentialProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpCredentialProfilesManagementCredentialProfiles - errors', () => {
      it('should have a getWebacsApiV4OpCredentialProfilesManagementCredentialProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpCredentialProfilesManagementCredentialProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpCredentialProfilesManagementDeviceList - errors', () => {
      it('should have a getWebacsApiV4OpCredentialProfilesManagementDeviceList function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpCredentialProfilesManagementDeviceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileName', (done) => {
        try {
          a.getWebacsApiV4OpCredentialProfilesManagementDeviceList(null, (data, error) => {
            try {
              const displayE = 'profileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpCredentialProfilesManagementDeviceList', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpCredentialProfilesManagementDeviceList - errors', () => {
      it('should have a postWebacsApiV4OpCredentialProfilesManagementDeviceList function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpCredentialProfilesManagementDeviceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpCredentialProfilesManagementDeviceList - errors', () => {
      it('should have a putWebacsApiV4OpCredentialProfilesManagementDeviceList function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpCredentialProfilesManagementDeviceList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataInventoryDetails - errors', () => {
      it('should have a getWebacsApiV4DataInventoryDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataInventoryDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataMerakiAccessPoints - errors', () => {
      it('should have a getWebacsApiV4DataMerakiAccessPoints function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataMerakiAccessPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataMerakiDevices - errors', () => {
      it('should have a getWebacsApiV4DataMerakiDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataMerakiDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpDevicesExportDevices - errors', () => {
      it('should have a getWebacsApiV4OpDevicesExportDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpDevicesExportDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpDevicesRemovalJob - errors', () => {
      it('should have a postWebacsApiV4OpDevicesRemovalJob function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpDevicesRemovalJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpDevicesSyncDevices - errors', () => {
      it('should have a postWebacsApiV4OpDevicesSyncDevices function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpDevicesSyncDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpDevicesBulkImport - errors', () => {
      it('should have a putWebacsApiV4OpDevicesBulkImport function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpDevicesBulkImport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpConfigureApScheduleMaintenanceMode - errors', () => {
      it('should have a putWebacsApiV4OpConfigureApScheduleMaintenanceMode function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpConfigureApScheduleMaintenanceMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpConfigureDeviceScheduleMaintenanceMode - errors', () => {
      it('should have a putWebacsApiV4OpConfigureDeviceScheduleMaintenanceMode function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpConfigureDeviceScheduleMaintenanceMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpConfigureApMaintenanceMode - errors', () => {
      it('should have a putWebacsApiV4OpConfigureApMaintenanceMode function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpConfigureApMaintenanceMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpConfigureDeviceMaintenanceMode - errors', () => {
      it('should have a putWebacsApiV4OpConfigureDeviceMaintenanceMode function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpConfigureDeviceMaintenanceMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpGroupsGroup - errors', () => {
      it('should have a deleteWebacsApiV4OpGroupsGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpGroupsGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGroupsGroupRules - errors', () => {
      it('should have a getWebacsApiV4OpGroupsGroupRules function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpGroupsGroupRules === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.getWebacsApiV4OpGroupsGroupRules(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpGroupsGroupRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupPath', (done) => {
        try {
          a.getWebacsApiV4OpGroupsGroupRules('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpGroupsGroupRules', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpGroupsGroupDevices - errors', () => {
      it('should have a putWebacsApiV4OpGroupsGroupDevices function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpGroupsGroupDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.putWebacsApiV4OpGroupsGroupDevices(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-putWebacsApiV4OpGroupsGroupDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupPath', (done) => {
        try {
          a.putWebacsApiV4OpGroupsGroupDevices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-putWebacsApiV4OpGroupsGroupDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpGroupsGroupInterfaces - errors', () => {
      it('should have a putWebacsApiV4OpGroupsGroupInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpGroupsGroupInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpGroupsRemoveDevices - errors', () => {
      it('should have a putWebacsApiV4OpGroupsRemoveDevices function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpGroupsRemoveDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.putWebacsApiV4OpGroupsRemoveDevices(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-putWebacsApiV4OpGroupsRemoveDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupPath', (done) => {
        try {
          a.putWebacsApiV4OpGroupsRemoveDevices('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-putWebacsApiV4OpGroupsRemoveDevices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpGroupsRemoveInterfaces - errors', () => {
      it('should have a putWebacsApiV4OpGroupsRemoveInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpGroupsRemoveInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.putWebacsApiV4OpGroupsRemoveInterfaces(null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-putWebacsApiV4OpGroupsRemoveInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupPath', (done) => {
        try {
          a.putWebacsApiV4OpGroupsRemoveInterfaces('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupPath is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-putWebacsApiV4OpGroupsRemoveInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataGroupSpecification - errors', () => {
      it('should have a getWebacsApiV4DataGroupSpecification function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataGroupSpecification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGroupsAlarmSummary - errors', () => {
      it('should have a getWebacsApiV4OpGroupsAlarmSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpGroupsAlarmSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGroupsDeviceGroups - errors', () => {
      it('should have a getWebacsApiV4OpGroupsDeviceGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpGroupsDeviceGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGroupsSites - errors', () => {
      it('should have a getWebacsApiV4OpGroupsSites function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpGroupsSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGroupsUserDefinedGroups - errors', () => {
      it('should have a getWebacsApiV4OpGroupsUserDefinedGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpGroupsUserDefinedGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataGuestUsers - errors', () => {
      it('should have a getWebacsApiV4DataGuestUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataGuestUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGuestUserGuestUsersOnController - errors', () => {
      it('should have a getWebacsApiV4OpGuestUserGuestUsersOnController function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpGuestUserGuestUsersOnController === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerId', (done) => {
        try {
          a.getWebacsApiV4OpGuestUserGuestUsersOnController(null, (data, error) => {
            try {
              const displayE = 'controllerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpGuestUserGuestUsersOnController', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGuestUserLoggedInGuestUsersOnController - errors', () => {
      it('should have a getWebacsApiV4OpGuestUserLoggedInGuestUsersOnController function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpGuestUserLoggedInGuestUsersOnController === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerId', (done) => {
        try {
          a.getWebacsApiV4OpGuestUserLoggedInGuestUsersOnController(null, (data, error) => {
            try {
              const displayE = 'controllerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpGuestUserLoggedInGuestUsersOnController', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpGuestUser - errors', () => {
      it('should have a postWebacsApiV4OpGuestUser function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpGuestUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpGuestUserDelete - errors', () => {
      it('should have a putWebacsApiV4OpGuestUserDelete function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpGuestUserDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataJobSummary - errors', () => {
      it('should have a getWebacsApiV4DataJobSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataJobSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpJobServiceRunhistory - errors', () => {
      it('should have a getWebacsApiV4OpJobServiceRunhistory function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpJobServiceRunhistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpJobServiceCancel - errors', () => {
      it('should have a putWebacsApiV4OpJobServiceCancel function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpJobServiceCancel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpJobServiceCancelrunning - errors', () => {
      it('should have a putWebacsApiV4OpJobServiceCancelrunning function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpJobServiceCancelrunning === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpJobServiceResume - errors', () => {
      it('should have a putWebacsApiV4OpJobServiceResume function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpJobServiceResume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpJobServiceSuspend - errors', () => {
      it('should have a putWebacsApiV4OpJobServiceSuspend function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpJobServiceSuspend === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataMacFilterTemplates - errors', () => {
      it('should have a getWebacsApiV4DataMacFilterTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataMacFilterTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpMacFilterMacFilterTemplate - errors', () => {
      it('should have a deleteWebacsApiV4OpMacFilterMacFilterTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpMacFilterMacFilterTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpMacFilterMacFilterTemplate - errors', () => {
      it('should have a postWebacsApiV4OpMacFilterMacFilterTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpMacFilterMacFilterTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpMacFilterDeploy - errors', () => {
      it('should have a putWebacsApiV4OpMacFilterDeploy function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpMacFilterDeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpInfoCoredumps - errors', () => {
      it('should have a getWebacsApiV4OpInfoCoredumps function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpInfoCoredumps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpInfoDisk - errors', () => {
      it('should have a getWebacsApiV4OpInfoDisk function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpInfoDisk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpInfoLicense - errors', () => {
      it('should have a getWebacsApiV4OpInfoLicense function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpInfoLicense === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpInfoUptime - errors', () => {
      it('should have a getWebacsApiV4OpInfoUptime function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpInfoUptime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpInfoVersion - errors', () => {
      it('should have a getWebacsApiV4OpInfoVersion function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpInfoVersion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpPnpProfileProfileNameInstance - errors', () => {
      it('should have a deleteWebacsApiV4OpPnpProfileProfileNameInstance function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpPnpProfileProfileNameInstance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileName', (done) => {
        try {
          a.deleteWebacsApiV4OpPnpProfileProfileNameInstance(null, (data, error) => {
            try {
              const displayE = 'profileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpPnpProfileProfileNameInstance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPnpProfilePnpProfileFolderProfileName - errors', () => {
      it('should have a getWebacsApiV4OpPnpProfilePnpProfileFolderProfileName function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPnpProfilePnpProfileFolderProfileName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pnpProfileFolder', (done) => {
        try {
          a.getWebacsApiV4OpPnpProfilePnpProfileFolderProfileName(null, null, (data, error) => {
            try {
              const displayE = 'pnpProfileFolder is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPnpProfilePnpProfileFolderProfileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileName', (done) => {
        try {
          a.getWebacsApiV4OpPnpProfilePnpProfileFolderProfileName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'profileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPnpProfilePnpProfileFolderProfileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpPnpProfilePnpProfileFolderProfileName - errors', () => {
      it('should have a postWebacsApiV4OpPnpProfilePnpProfileFolderProfileName function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpPnpProfilePnpProfileFolderProfileName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pnpProfileFolder', (done) => {
        try {
          a.postWebacsApiV4OpPnpProfilePnpProfileFolderProfileName(null, null, (data, error) => {
            try {
              const displayE = 'pnpProfileFolder is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPnpProfilePnpProfileFolderProfileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileName', (done) => {
        try {
          a.postWebacsApiV4OpPnpProfilePnpProfileFolderProfileName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'profileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPnpProfilePnpProfileFolderProfileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpPnpProfilePnpProfileFolderProfileName - errors', () => {
      it('should have a deleteWebacsApiV4OpPnpProfilePnpProfileFolderProfileName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpPnpProfilePnpProfileFolderProfileName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pnpProfileFolder', (done) => {
        try {
          a.deleteWebacsApiV4OpPnpProfilePnpProfileFolderProfileName(null, null, (data, error) => {
            try {
              const displayE = 'pnpProfileFolder is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpPnpProfilePnpProfileFolderProfileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileName', (done) => {
        try {
          a.deleteWebacsApiV4OpPnpProfilePnpProfileFolderProfileName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'profileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpPnpProfilePnpProfileFolderProfileName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName - errors', () => {
      it('should have a deleteWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileName', (done) => {
        try {
          a.deleteWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName(null, null, (data, error) => {
            try {
              const displayE = 'profileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceName', (done) => {
        try {
          a.deleteWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName - errors', () => {
      it('should have a getWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing profileName', (done) => {
        try {
          a.getWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName(null, null, (data, error) => {
            try {
              const displayE = 'profileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing instanceName', (done) => {
        try {
          a.getWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'instanceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpRateServiceRateLimits - errors', () => {
      it('should have a getWebacsApiV4OpRateServiceRateLimits function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpRateServiceRateLimits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpRateServiceRateLimits - errors', () => {
      it('should have a putWebacsApiV4OpRateServiceRateLimits function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpRateServiceRateLimits === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpReportServiceTemplates - errors', () => {
      it('should have a getWebacsApiV4OpReportServiceTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpReportServiceTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpReportServiceGetReport - errors', () => {
      it('should have a getWebacsApiV4OpReportServiceGetReport function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpReportServiceGetReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportTitle', (done) => {
        try {
          a.getWebacsApiV4OpReportServiceGetReport(null, null, (data, error) => {
            try {
              const displayE = 'reportTitle is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpReportServiceGetReport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpReportServiceZipReport - errors', () => {
      it('should have a getWebacsApiV4OpReportServiceZipReport function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpReportServiceZipReport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpUpdateRetrieve - errors', () => {
      it('should have a getWebacsApiV4OpUpdateRetrieve function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpUpdateRetrieve === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetAllBorderRoutersStartTimeEndTimeSiteName - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetAllBorderRoutersStartTimeEndTimeSiteName function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetAllBorderRoutersStartTimeEndTimeSiteName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllBorderRoutersStartTimeEndTimeSiteName(null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllBorderRoutersStartTimeEndTimeSiteName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllBorderRoutersStartTimeEndTimeSiteName('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllBorderRoutersStartTimeEndTimeSiteName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteName', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllBorderRoutersStartTimeEndTimeSiteName('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'siteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllBorderRoutersStartTimeEndTimeSiteName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing srcSite', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'srcSite is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dstSite', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'dstSite is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing srcSite', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'srcSite is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dstSite', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'dstSite is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing srcSite', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'srcSite is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dstSite', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'dstSite is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteName', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'siteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing brIp', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'brIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationApplicationArtAnalysis - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationApplicationArtAnalysis function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationApplicationArtAnalysis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationPerformance - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationPerformance function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationPerformance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationPerformance(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationPerformance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationPerformance('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationPerformance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationPerformance('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationPerformance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationPerformance('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationPerformance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationTopNTraffics - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationTopNTraffics function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationTopNTraffics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationAppVolume - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationAppVolume function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationAppVolume === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationNumberOfUsers - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationNumberOfUsers function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationNumberOfUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationNumberOfUsers(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationNumberOfUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationNumberOfUsers('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationNumberOfUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationNumberOfUsers('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationNumberOfUsers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceCpuUtilSummary - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDeviceCpuUtilSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDeviceCpuUtilSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceCpuUtilSummary(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceCpuUtilSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceCpuUtilSummary('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceCpuUtilSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceCpuUtilSummary('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceCpuUtilSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend(null, null, null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceClientsDistributionsDistribution - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceClientsDistributionsDistribution function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceClientsDistributionsDistribution === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing distribution', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceClientsDistributionsDistribution(null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'distribution is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceClientsDistributionsDistribution', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceClientsDistributions - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceClientsDistributions function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceClientsDistributions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceClusters - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceClusters function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceClusters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceClustersMetricsMetric - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceClustersMetricsMetric function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceClustersMetricsMetric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metric', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceClustersMetricsMetric(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'metric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceClustersMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeInterval', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceClustersMetricsMetric('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'timeInterval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceClustersMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceClustersMetricsMetric('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceClustersMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceClustersMetricsMetric('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceClustersMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceClustersMetrics - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceClustersMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceClustersMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeInterval', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceClustersMetrics('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'timeInterval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceClustersMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceClustersMetrics('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceClustersMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceClustersMetrics('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceClustersMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageStartTimeEndTime - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageStartTimeEndTime function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageStartTimeEndTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageStartTimeEndTime(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageStartTimeEndTime('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing srcSiteId', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'srcSiteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dstSiteId', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'dstSiteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteName', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'siteName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing brIp', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'brIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dscp', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'dscp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noOfPoints', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'noOfPoints is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDatacenters - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDatacenters function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDatacenters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metric', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'metric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeInterval', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'timeInterval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDatacentersMetrics - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDatacentersMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDatacentersMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeInterval', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDatacentersMetrics('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'timeInterval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDatacentersMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDatacentersMetrics('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDatacentersMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDatacentersMetrics('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDatacentersMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceAvailability - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDeviceAvailability function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDeviceAvailability === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceAvailability(null, null, null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceAvailability('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceAvailability('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceAvailability('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceAvailabilityMessage - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDeviceAvailabilityMessage function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDeviceAvailabilityMessage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceAvailabilityMessage(null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceAvailabilityMessage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceAvailabilitySummary - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDeviceAvailabilitySummary function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDeviceAvailabilitySummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceAvailabilitySummary(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceAvailabilitySummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceAvailabilitySummary('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceAvailabilitySummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceAvailabilitySummary('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceAvailabilitySummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceDownMessage - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDeviceDownMessage function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDeviceDownMessage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceDownMessage(null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceDownMessage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceHealthInfo - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDeviceHealthInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDeviceHealthInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceHealthInfo(null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceHealthInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceHealthInfo('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceHealthInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceHealthInfo('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceHealthInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDevicesMetricsMetric - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDevicesMetricsMetric function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDevicesMetricsMetric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metric', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDevicesMetricsMetric(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'metric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDevicesMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDevicesMetricsMetric('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDevicesMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lineCard', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDevicesMetricsMetric('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'lineCard is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDevicesMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDevicesMetrics - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDevicesMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDevicesMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDevicesMetrics(null, null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDevicesMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDevicePortSummary - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDevicePortSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDevicePortSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDevicePortSummary(null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDevicePortSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceReachabilityStatus - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDeviceReachabilityStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDeviceReachabilityStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDevices - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceHosts - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceHosts function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceHosts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceHostsMetricsMetric - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceHostsMetricsMetric function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceHostsMetricsMetric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metric', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceHostsMetricsMetric(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'metric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceHostsMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeInterval', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceHostsMetricsMetric('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'timeInterval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceHostsMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceHostsMetricsMetric('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceHostsMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceHostsMetricsMetric('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceHostsMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceHostsMetrics - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceHostsMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceHostsMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeInterval', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceHostsMetrics('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'timeInterval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceHostsMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceHostsMetrics('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceHostsMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceHostsMetrics('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceHostsMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceAvailability - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceAvailability function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceAvailability === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceAvailability(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceAvailability('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceAvailability('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceAvailability('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ifName', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceAvailability('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ifName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceAvailability', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceAvailMessage - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceAvailMessage function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceAvailMessage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceAvailMessage(null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceAvailMessage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ifName', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceAvailMessage('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ifName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceAvailMessage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceAvailabilitySummary - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceAvailabilitySummary function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceAvailabilitySummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceAvailabilitySummary(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceAvailabilitySummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceAvailabilitySummary('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceAvailabilitySummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceAvailabilitySummary('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceAvailabilitySummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceDetails - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceDetails(null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ifName', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceDetails('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ifName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceDiscards - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceDiscards function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceDiscards === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceDiscards(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceDiscards', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceDiscards('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceDiscards', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceDiscards('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceDiscards', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceDiscards('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceDiscards', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ifName', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceDiscards('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ifName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceDiscards', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceDownMessage - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceDownMessage function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceDownMessage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceDownMessage(null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceDownMessage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceErrors - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceErrors function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceErrors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceErrors(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceErrors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceErrors('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceErrors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceErrors('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceErrors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceErrors('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceErrors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ifName', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceErrors('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ifName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceErrors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfacesMetricsMetric - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfacesMetricsMetric function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfacesMetricsMetric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metric', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfacesMetricsMetric(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'metric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfacesMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfacesMetricsMetric('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfacesMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ifName', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfacesMetricsMetric('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'ifName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfacesMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfacesMetrics - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfacesMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfacesMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfacesMetrics(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfacesMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ifName', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfacesMetrics('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'ifName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfacesMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaces - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaces function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaces === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing device', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaces(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'device is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaces', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceStatusSummary - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceStatusSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceStatusSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceStatusSummary(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceStatusSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceStatusSummary('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceStatusSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceStatusSummary('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceStatusSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceUtilSummary - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceUtilSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceUtilSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceUtilSummary(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceUtilSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceUtilSummary('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceUtilSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceUtilSummary('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceUtilSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceUtil - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceUtil function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceUtil === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceUtil(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceUtil', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceUtil('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceUtil', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceUtil('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceUtil', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceUtil('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceUtil', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ifName', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceUtil('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ifName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceUtil', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpDeviceRestDeviceLispMapSummaryDeviceIpStartTimeEndTime - errors', () => {
      it('should have a getWebacsApiV4OpDeviceRestDeviceLispMapSummaryDeviceIpStartTimeEndTime function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpDeviceRestDeviceLispMapSummaryDeviceIpStartTimeEndTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIp', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestDeviceLispMapSummaryDeviceIpStartTimeEndTime(null, null, null, (data, error) => {
            try {
              const displayE = 'deviceIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestDeviceLispMapSummaryDeviceIpStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestDeviceLispMapSummaryDeviceIpStartTimeEndTime('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestDeviceLispMapSummaryDeviceIpStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestDeviceLispMapSummaryDeviceIpStartTimeEndTime('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestDeviceLispMapSummaryDeviceIpStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipAddress', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend(null, null, null, null, (data, error) => {
            try {
              const displayE = 'ipAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetOutStandingIMEsForSiteStartTimeEndTimeSrcSite - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetOutStandingIMEsForSiteStartTimeEndTimeSrcSite function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetOutStandingIMEsForSiteStartTimeEndTimeSrcSite === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetOutStandingIMEsForSiteStartTimeEndTimeSrcSite(null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetOutStandingIMEsForSiteStartTimeEndTimeSrcSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetOutStandingIMEsForSiteStartTimeEndTimeSrcSite('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetOutStandingIMEsForSiteStartTimeEndTimeSrcSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing srcSite', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetOutStandingIMEsForSiteStartTimeEndTimeSrcSite('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'srcSite is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetOutStandingIMEsForSiteStartTimeEndTimeSrcSite', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords - errors', () => {
      it('should have a getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords(null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxrecords', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'maxrecords is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceResourceTypes - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceResourceTypes function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceResourceTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetSiteAppHealthStartTimeEndTime - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetSiteAppHealthStartTimeEndTime function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetSiteAppHealthStartTimeEndTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetSiteAppHealthStartTimeEndTime(null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetSiteAppHealthStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetSiteAppHealthStartTimeEndTime('fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetSiteAppHealthStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceSystemHealth - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceSystemHealth function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceSystemHealth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceSystemHealth(null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceSystemHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceSystemHealth('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceSystemHealth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceSystemInfo - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceSystemInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceSystemInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationsAppHealthCrossing - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationsAppHealthCrossing function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationsAppHealthCrossing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationTopNHosts - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationTopNHosts function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationTopNHosts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTopNHosts(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationTopNHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTopNHosts('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationTopNHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTopNHosts('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationTopNHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTopNHosts('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationTopNHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationsPerformance - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationsPerformance function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationsPerformance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationsPerformance('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationsPerformance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationsPerformance('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationsPerformance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationsPerformance('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationsPerformance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationsOvertime - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationsOvertime function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationsOvertime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationsOvertime(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationsOvertime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationsOvertime('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationsOvertime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing siteId', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationsOvertime('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'siteId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationsOvertime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing isFabric', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationsOvertime('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'isFabric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationsOvertime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ifIndex', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ifIndex is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceTopNCPU - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDeviceTopNCPU function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDeviceTopNCPU === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNCPU(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceTopNCPU', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNCPU('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceTopNCPU', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNCPU('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceTopNCPU', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNCPU('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceTopNCPU', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn - errors', () => {
      it('should have a getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topn', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'topn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing source', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'source is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dashlet', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'dashlet is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceTopNMemory - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDeviceTopNMemory function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDeviceTopNMemory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNMemory(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceTopNMemory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNMemory('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceTopNMemory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNMemory('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceTopNMemory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNMemory('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceTopNMemory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceTopNTemp - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceDeviceTopNTemp function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceDeviceTopNTemp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNTemp(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceTopNTemp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNTemp('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceTopNTemp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNTemp('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceTopNTemp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNTemp('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceTopNTemp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNTemp('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceDeviceTopNTemp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIp', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'deviceIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rownum', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'rownum is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deviceIp', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'deviceIp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing rownum', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'rownum is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationsAppCount - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationsAppCount function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationsAppCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords - errors', () => {
      it('should have a getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords(null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maxrecords', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'maxrecords is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupId', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceVms - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceVms function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceVms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceVmsMetricsMetric - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceVmsMetricsMetric function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceVmsMetricsMetric === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metric', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceVmsMetricsMetric(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'metric is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceVmsMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeInterval', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceVmsMetricsMetric('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'timeInterval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceVmsMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceVmsMetricsMetric('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceVmsMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceVmsMetricsMetric('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceVmsMetricsMetric', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceVmsMetrics - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceVmsMetrics function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceVmsMetrics === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timeInterval', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceVmsMetrics('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'timeInterval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceVmsMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceVmsMetrics('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceVmsMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceVmsMetrics('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceVmsMetrics', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing brIpAddr', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'brIpAddr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing direction', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'direction is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceName', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection - errors', () => {
      it('should have a getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing brIpAddr', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'brIpAddr is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing direction', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'direction is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceName', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts(null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts('fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationWorstNSites - errors', () => {
      it('should have a getWebacsApiV4OpStatisticsServiceApplicationWorstNSites function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpStatisticsServiceApplicationWorstNSites === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationWorstNSites(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationWorstNSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationWorstNSites('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationWorstNSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing range', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationWorstNSites('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'range is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationWorstNSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationWorstNSites('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpStatisticsServiceApplicationWorstNSites', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpPfrMonitoringExtGetDSCPvsAllTCAViaPostStartTimeEndTime - errors', () => {
      it('should have a postWebacsApiV4OpPfrMonitoringExtGetDSCPvsAllTCAViaPostStartTimeEndTime function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpPfrMonitoringExtGetDSCPvsAllTCAViaPostStartTimeEndTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetDSCPvsAllTCAViaPostStartTimeEndTime(null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetDSCPvsAllTCAViaPostStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetDSCPvsAllTCAViaPostStartTimeEndTime('fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetDSCPvsAllTCAViaPostStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpPfrMonitoringExtGetDSCPvsUrTCAViaPostStartTimeEndTime - errors', () => {
      it('should have a postWebacsApiV4OpPfrMonitoringExtGetDSCPvsUrTCAViaPostStartTimeEndTime function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpPfrMonitoringExtGetDSCPvsUrTCAViaPostStartTimeEndTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetDSCPvsUrTCAViaPostStartTimeEndTime(null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetDSCPvsUrTCAViaPostStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetDSCPvsUrTCAViaPostStartTimeEndTime('fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetDSCPvsUrTCAViaPostStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpPfrMonitoringExtGetMaxDelayVsDSCPChartReportsViaPostStartTimeEndTime - errors', () => {
      it('should have a postWebacsApiV4OpPfrMonitoringExtGetMaxDelayVsDSCPChartReportsViaPostStartTimeEndTime function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpPfrMonitoringExtGetMaxDelayVsDSCPChartReportsViaPostStartTimeEndTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetMaxDelayVsDSCPChartReportsViaPostStartTimeEndTime(null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetMaxDelayVsDSCPChartReportsViaPostStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetMaxDelayVsDSCPChartReportsViaPostStartTimeEndTime('fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetMaxDelayVsDSCPChartReportsViaPostStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpPfrMonitoringExtGetMaxJitterVsDSCPChartReportsViaPostStartTimeEndTime - errors', () => {
      it('should have a postWebacsApiV4OpPfrMonitoringExtGetMaxJitterVsDSCPChartReportsViaPostStartTimeEndTime function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpPfrMonitoringExtGetMaxJitterVsDSCPChartReportsViaPostStartTimeEndTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetMaxJitterVsDSCPChartReportsViaPostStartTimeEndTime(null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetMaxJitterVsDSCPChartReportsViaPostStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetMaxJitterVsDSCPChartReportsViaPostStartTimeEndTime('fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetMaxJitterVsDSCPChartReportsViaPostStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpPfrMonitoringExtGetMaxPacketLossVsDSCPChartReportsViaPostStartTimeEndTime - errors', () => {
      it('should have a postWebacsApiV4OpPfrMonitoringExtGetMaxPacketLossVsDSCPChartReportsViaPostStartTimeEndTime function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpPfrMonitoringExtGetMaxPacketLossVsDSCPChartReportsViaPostStartTimeEndTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetMaxPacketLossVsDSCPChartReportsViaPostStartTimeEndTime(null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetMaxPacketLossVsDSCPChartReportsViaPostStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetMaxPacketLossVsDSCPChartReportsViaPostStartTimeEndTime('fakeparam', null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetMaxPacketLossVsDSCPChartReportsViaPostStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpPfrMonitoringExtGetPfrEventListViaPostStartTimeEndTime - errors', () => {
      it('should have a postWebacsApiV4OpPfrMonitoringExtGetPfrEventListViaPostStartTimeEndTime function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpPfrMonitoringExtGetPfrEventListViaPostStartTimeEndTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetPfrEventListViaPostStartTimeEndTime(null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetPfrEventListViaPostStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetPfrEventListViaPostStartTimeEndTime('fakeparam', null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetPfrEventListViaPostStartTimeEndTime', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpPfrMonitoringExtGetServiceProviderUsageVsDSCPChartStartTimeEndTimeTopN - errors', () => {
      it('should have a postWebacsApiV4OpPfrMonitoringExtGetServiceProviderUsageVsDSCPChartStartTimeEndTimeTopN function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpPfrMonitoringExtGetServiceProviderUsageVsDSCPChartStartTimeEndTimeTopN === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetServiceProviderUsageVsDSCPChartStartTimeEndTimeTopN(null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetServiceProviderUsageVsDSCPChartStartTimeEndTimeTopN', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetServiceProviderUsageVsDSCPChartStartTimeEndTimeTopN('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetServiceProviderUsageVsDSCPChartStartTimeEndTimeTopN', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing topN', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetServiceProviderUsageVsDSCPChartStartTimeEndTimeTopN('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'topN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-postWebacsApiV4OpPfrMonitoringExtGetServiceProviderUsageVsDSCPChartStartTimeEndTimeTopN', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpSettingsServersFtpServersServerName - errors', () => {
      it('should have a deleteWebacsApiV4OpSettingsServersFtpServersServerName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpSettingsServersFtpServersServerName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing serverName', (done) => {
        try {
          a.deleteWebacsApiV4OpSettingsServersFtpServersServerName(null, (data, error) => {
            try {
              const displayE = 'serverName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpSettingsServersFtpServersServerName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsNotificationAlarmCategories - errors', () => {
      it('should have a getWebacsApiV4OpSettingsNotificationAlarmCategories function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpSettingsNotificationAlarmCategories === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsInventoryConfigArchiveBasic - errors', () => {
      it('should have a getWebacsApiV4OpSettingsInventoryConfigArchiveBasic function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpSettingsInventoryConfigArchiveBasic === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsAlarmsAlarmDeletionPeriods - errors', () => {
      it('should have a getWebacsApiV4OpSettingsAlarmsAlarmDeletionPeriods function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpSettingsAlarmsAlarmDeletionPeriods === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsLoggingGeneralLoggingOptions - errors', () => {
      it('should have a getWebacsApiV4OpSettingsLoggingGeneralLoggingOptions function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpSettingsLoggingGeneralLoggingOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsDataPruneDataConfigTableName - errors', () => {
      it('should have a getWebacsApiV4OpSettingsDataPruneDataConfigTableName function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpSettingsDataPruneDataConfigTableName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tableName', (done) => {
        try {
          a.getWebacsApiV4OpSettingsDataPruneDataConfigTableName(null, (data, error) => {
            try {
              const displayE = 'tableName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpSettingsDataPruneDataConfigTableName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsDataPruneDataConfigsByGroupCategory - errors', () => {
      it('should have a getWebacsApiV4OpSettingsDataPruneDataConfigsByGroupCategory function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpSettingsDataPruneDataConfigsByGroupCategory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing category', (done) => {
        try {
          a.getWebacsApiV4OpSettingsDataPruneDataConfigsByGroupCategory(null, (data, error) => {
            try {
              const displayE = 'category is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-getWebacsApiV4OpSettingsDataPruneDataConfigsByGroupCategory', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsDataDataRetentionPeriods - errors', () => {
      it('should have a getWebacsApiV4OpSettingsDataDataRetentionPeriods function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpSettingsDataDataRetentionPeriods === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsServersFtpServers - errors', () => {
      it('should have a getWebacsApiV4OpSettingsServersFtpServers function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpSettingsServersFtpServers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsLoggingLogFileNames - errors', () => {
      it('should have a getWebacsApiV4OpSettingsLoggingLogFileNames function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpSettingsLoggingLogFileNames === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsNotificationMailServer - errors', () => {
      it('should have a getWebacsApiV4OpSettingsNotificationMailServer function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpSettingsNotificationMailServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsLoggingSnmpSettings - errors', () => {
      it('should have a getWebacsApiV4OpSettingsLoggingSnmpSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpSettingsLoggingSnmpSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsAlarmsSeverityAutoClearConfig - errors', () => {
      it('should have a getWebacsApiV4OpSettingsAlarmsSeverityAutoClearConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpSettingsAlarmsSeverityAutoClearConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsLoggingSysLogSettings - errors', () => {
      it('should have a getWebacsApiV4OpSettingsLoggingSysLogSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpSettingsLoggingSysLogSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpSettingsLoggingSysLogSettings - errors', () => {
      it('should have a postWebacsApiV4OpSettingsLoggingSysLogSettings function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpSettingsLoggingSysLogSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpSettingsLoggingSysLogSettings - errors', () => {
      it('should have a putWebacsApiV4OpSettingsLoggingSysLogSettings function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpSettingsLoggingSysLogSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataUserDefinedFieldDefinition - errors', () => {
      it('should have a getWebacsApiV4DataUserDefinedFieldDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataUserDefinedFieldDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpUdfServiceDeleteUserTagDefinition - errors', () => {
      it('should have a putWebacsApiV4OpUdfServiceDeleteUserTagDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpUdfServiceDeleteUserTagDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpUdfServiceImportUserTagDefinition - errors', () => {
      it('should have a putWebacsApiV4OpUdfServiceImportUserTagDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpUdfServiceImportUserTagDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpUserManagementUsers - errors', () => {
      it('should have a deleteWebacsApiV4OpUserManagementUsers function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpUserManagementUsers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpUserManagementUserGroups - errors', () => {
      it('should have a getWebacsApiV4OpUserManagementUserGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpUserManagementUserGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpUserManagementUserGroups - errors', () => {
      it('should have a postWebacsApiV4OpUserManagementUserGroups function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpUserManagementUserGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpUserManagementUserGroups - errors', () => {
      it('should have a putWebacsApiV4OpUserManagementUserGroups function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpUserManagementUserGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpNfvGetBridges - errors', () => {
      it('should have a getWebacsApiV4OpNfvGetBridges function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpNfvGetBridges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpNfvGetDeployments - errors', () => {
      it('should have a getWebacsApiV4OpNfvGetDeployments function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpNfvGetDeployments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpNfvDeviceDetails - errors', () => {
      it('should have a getWebacsApiV4OpNfvDeviceDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpNfvDeviceDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpNfvGetOvsBridges - errors', () => {
      it('should have a getWebacsApiV4OpNfvGetOvsBridges function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpNfvGetOvsBridges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpNfvGetServices - errors', () => {
      it('should have a getWebacsApiV4OpNfvGetServices function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpNfvGetServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpNfvAddBridge - errors', () => {
      it('should have a postWebacsApiV4OpNfvAddBridge function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpNfvAddBridge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpVirtualAddDevice - errors', () => {
      it('should have a postWebacsApiV4OpVirtualAddDevice function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpVirtualAddDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpNfvCreateOvsBridge - errors', () => {
      it('should have a postWebacsApiV4OpNfvCreateOvsBridge function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpNfvCreateOvsBridge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpNfvDeleteBridge - errors', () => {
      it('should have a postWebacsApiV4OpNfvDeleteBridge function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpNfvDeleteBridge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpNfvDeleteOvsBridge - errors', () => {
      it('should have a postWebacsApiV4OpNfvDeleteOvsBridge function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpNfvDeleteOvsBridge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpNfvDeploy - errors', () => {
      it('should have a postWebacsApiV4OpNfvDeploy function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpNfvDeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpNfvRegister - errors', () => {
      it('should have a postWebacsApiV4OpNfvRegister function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpNfvRegister === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpNfvUndeploy - errors', () => {
      it('should have a postWebacsApiV4OpNfvUndeploy function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpNfvUndeploy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpNfvUnregister - errors', () => {
      it('should have a postWebacsApiV4OpNfvUnregister function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpNfvUnregister === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpNfvUpdateBridge - errors', () => {
      it('should have a putWebacsApiV4OpNfvUpdateBridge function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpNfvUpdateBridge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpNfvUpdateOvsBridge - errors', () => {
      it('should have a putWebacsApiV4OpNfvUpdateOvsBridge function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpNfvUpdateOvsBridge === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataVDAssociatedAccessPoints - errors', () => {
      it('should have a getWebacsApiV4DataVDAssociatedAccessPoints function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataVDAssociatedAccessPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataVDAssociatedDynamicGroups - errors', () => {
      it('should have a getWebacsApiV4DataVDAssociatedDynamicGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataVDAssociatedDynamicGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataVDAssociatedGroups - errors', () => {
      it('should have a getWebacsApiV4DataVDAssociatedGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataVDAssociatedGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataVDAssociatedDevices - errors', () => {
      it('should have a getWebacsApiV4DataVDAssociatedDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataVDAssociatedDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataVDAssociatedSiteMaps - errors', () => {
      it('should have a getWebacsApiV4DataVDAssociatedSiteMaps function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataVDAssociatedSiteMaps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataVDAssociatedVirtualElements - errors', () => {
      it('should have a getWebacsApiV4DataVDAssociatedVirtualElements function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataVDAssociatedVirtualElements === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpVd - errors', () => {
      it('should have a deleteWebacsApiV4OpVd function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpVd === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainFQN', (done) => {
        try {
          a.deleteWebacsApiV4OpVd(null, (data, error) => {
            try {
              const displayE = 'domainFQN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpVd', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpVd - errors', () => {
      it('should have a getWebacsApiV4OpVd function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpVd === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpVd - errors', () => {
      it('should have a postWebacsApiV4OpVd function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpVd === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpVd - errors', () => {
      it('should have a putWebacsApiV4OpVd function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpVd === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domainFQN', (done) => {
        try {
          a.putWebacsApiV4OpVd(null, (data, error) => {
            try {
              const displayE = 'domainFQN is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-putWebacsApiV4OpVd', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpImageId - errors', () => {
      it('should have a deleteWebacsApiV4OpImageId function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpImageId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpImageListId - errors', () => {
      it('should have a getWebacsApiV4OpImageListId function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpImageListId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpImageList - errors', () => {
      it('should have a getWebacsApiV4OpImageList function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpImageList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpImageListFamily - errors', () => {
      it('should have a getWebacsApiV4OpImageListFamily function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpImageListFamily === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpImageListPhysical - errors', () => {
      it('should have a getWebacsApiV4OpImageListPhysical function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpImageListPhysical === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpImageListVirtual - errors', () => {
      it('should have a getWebacsApiV4OpImageListVirtual function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4OpImageListVirtual === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpImageAdd - errors', () => {
      it('should have a postWebacsApiV4OpImageAdd function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpImageAdd === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpImageDeployConfigure - errors', () => {
      it('should have a postWebacsApiV4OpImageDeployConfigure function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpImageDeployConfigure === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataWlanProfiles - errors', () => {
      it('should have a getWebacsApiV4DataWlanProfiles function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataWlanProfiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataWlanTemplates - errors', () => {
      it('should have a getWebacsApiV4DataWlanTemplates function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataWlanTemplates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpWlanProvisioningApGroup - errors', () => {
      it('should have a deleteWebacsApiV4OpWlanProvisioningApGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpWlanProvisioningApGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerId', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningApGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'controllerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpWlanProvisioningApGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerName', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningApGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'controllerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpWlanProvisioningApGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing apGroupName', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningApGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'apGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpWlanProvisioningApGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpWlanProvisioningInterface - errors', () => {
      it('should have a deleteWebacsApiV4OpWlanProvisioningInterface function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpWlanProvisioningInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerId', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningInterface(null, null, null, (data, error) => {
            try {
              const displayE = 'controllerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpWlanProvisioningInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerName', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningInterface('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'controllerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpWlanProvisioningInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceName', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningInterface('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpWlanProvisioningInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpWlanProvisioningInterfaceGroup - errors', () => {
      it('should have a deleteWebacsApiV4OpWlanProvisioningInterfaceGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpWlanProvisioningInterfaceGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerId', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningInterfaceGroup(null, null, null, (data, error) => {
            try {
              const displayE = 'controllerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpWlanProvisioningInterfaceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerName', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningInterfaceGroup('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'controllerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpWlanProvisioningInterfaceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interfaceGroupName', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningInterfaceGroup('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'interfaceGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpWlanProvisioningInterfaceGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpWlanProvisioningWlanProfile - errors', () => {
      it('should have a deleteWebacsApiV4OpWlanProvisioningWlanProfile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpWlanProvisioningWlanProfile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerId', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningWlanProfile(null, null, null, (data, error) => {
            try {
              const displayE = 'controllerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpWlanProvisioningWlanProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing controllerName', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningWlanProfile('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'controllerName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpWlanProvisioningWlanProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing wlanProfileName', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningWlanProfile('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'wlanProfileName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-cisco_prime-adapter-deleteWebacsApiV4OpWlanProvisioningWlanProfile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpWlanProvisioningWlanTemplate - errors', () => {
      it('should have a deleteWebacsApiV4OpWlanProvisioningWlanTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.deleteWebacsApiV4OpWlanProvisioningWlanTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpWlanProvisioningWlanTemplate - errors', () => {
      it('should have a postWebacsApiV4OpWlanProvisioningWlanTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.postWebacsApiV4OpWlanProvisioningWlanTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpWlanProvisioningDeployTemplate - errors', () => {
      it('should have a putWebacsApiV4OpWlanProvisioningDeployTemplate function', (done) => {
        try {
          assert.equal(true, typeof a.putWebacsApiV4OpWlanProvisioningDeployTemplate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataAutoApRadioDetails - errors', () => {
      it('should have a getWebacsApiV4DataAutoApRadioDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataAutoApRadioDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataClientStats - errors', () => {
      it('should have a getWebacsApiV4DataClientStats function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataClientStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalClientStats - errors', () => {
      it('should have a getWebacsApiV4DataHistoricalClientStats function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataHistoricalClientStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalRFCounters - errors', () => {
      it('should have a getWebacsApiV4DataHistoricalRFCounters function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataHistoricalRFCounters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalRFLoadStats - errors', () => {
      it('should have a getWebacsApiV4DataHistoricalRFLoadStats function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataHistoricalRFLoadStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalRFStats - errors', () => {
      it('should have a getWebacsApiV4DataHistoricalRFStats function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataHistoricalRFStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalWLCCPUUtilizations - errors', () => {
      it('should have a getWebacsApiV4DataHistoricalWLCCPUUtilizations function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataHistoricalWLCCPUUtilizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalWLCMemUtilizations - errors', () => {
      it('should have a getWebacsApiV4DataHistoricalWLCMemUtilizations function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataHistoricalWLCMemUtilizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataRadioDetails - errors', () => {
      it('should have a getWebacsApiV4DataRadioDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataRadioDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataRFCounters - errors', () => {
      it('should have a getWebacsApiV4DataRFCounters function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataRFCounters === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataRFLoadStats - errors', () => {
      it('should have a getWebacsApiV4DataRFLoadStats function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataRFLoadStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataRFStats - errors', () => {
      it('should have a getWebacsApiV4DataRFStats function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataRFStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataRadios - errors', () => {
      it('should have a getWebacsApiV4DataRadios function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataRadios === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataThirdpartyAccessPoints - errors', () => {
      it('should have a getWebacsApiV4DataThirdpartyAccessPoints function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataThirdpartyAccessPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataAccessPoints - errors', () => {
      it('should have a getWebacsApiV4DataAccessPoints function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataAccessPoints === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataWLCCPUUtilizations - errors', () => {
      it('should have a getWebacsApiV4DataWLCCPUUtilizations function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataWLCCPUUtilizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataWLCMemoryUtilizations - errors', () => {
      it('should have a getWebacsApiV4DataWLCMemoryUtilizations function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataWLCMemoryUtilizations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataWlanControllers - errors', () => {
      it('should have a getWebacsApiV4DataWlanControllers function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataWlanControllers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataDevices - errors', () => {
      it('should have a getWebacsApiV4DataDevices function', (done) => {
        try {
          assert.equal(true, typeof a.getWebacsApiV4DataDevices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
