/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-cisco_prime',
      type: 'CiscoPrime',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const CiscoPrime = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Cisco_prime Adapter Test', () => {
  describe('CiscoPrime Class Tests', () => {
    const a = new CiscoPrime(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cisco_prime-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-cisco_prime-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const webacsPostWebacsApiV4OpAaaTacacsPlusServerBodyParam = {
      authenticationType: {},
      localInterfaceIp: {},
      numberOfTries: {},
      port: {},
      retransmitTimeout: {},
      secretKey: 'string',
      secretKeyType: {},
      serverHostName: 'string',
      serverIp: {}
    };
    describe('#postWebacsApiV4OpAaaTacacsPlusServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpAaaTacacsPlusServer(webacsPostWebacsApiV4OpAaaTacacsPlusServerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.operationSucceeded);
                assert.equal('string', data.response.resultMessage);
                assert.equal('object', typeof data.response.server);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpAaaTacacsPlusServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpApOnboardingProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpApOnboardingProfile(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.profiles));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpApOnboardingProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpCliTemplateConfigurationDeviceTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpCliTemplateConfigurationDeviceTypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.folderFQN);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpCliTemplateConfigurationDeviceTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpCliTemplateConfigurationUpload - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpCliTemplateConfigurationUpload((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.content);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.deviceType);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.path);
                assert.equal('string', data.response.tags);
                assert.equal(true, Array.isArray(data.response.variables));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpCliTemplateConfigurationUpload', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpCompliancePolicyBodyParam = {
      policyDescription: 'string',
      policyId: 'string',
      policyTitle: 'string',
      ruleList: [
        {}
      ]
    };
    describe('#postWebacsApiV4OpCompliancePolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpCompliancePolicy(webacsPostWebacsApiV4OpCompliancePolicyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.isSuccess);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpCompliancePolicy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpCompliancePolicyImportBodyParam = {
      policyXml: 'string'
    };
    describe('#postWebacsApiV4OpCompliancePolicyImport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpCompliancePolicyImport(webacsPostWebacsApiV4OpCompliancePolicyImportBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.isSuccess);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpCompliancePolicyImport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpComplianceProfileBodyParam = {
      group: {},
      name: 'string',
      policies: [
        {}
      ]
    };
    describe('#postWebacsApiV4OpComplianceProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpComplianceProfile(webacsPostWebacsApiV4OpComplianceProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.isSuccess);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpComplianceProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpComplianceRunBodyParam = {
      configFileFrom: 'string',
      jobDto: {},
      policyProfileName: 'string',
      targets: [
        {}
      ]
    };
    describe('#postWebacsApiV4OpComplianceRun - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpComplianceRun(webacsPostWebacsApiV4OpComplianceRunBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.isSuccess);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpComplianceRun', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webacsProfileName = 'fakedata';
    describe('#postWebacsApiV4OpCredentialProfilesManagementDeviceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpCredentialProfilesManagementDeviceList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.cliParameters);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.httpParameters);
                assert.equal('string', data.response.profileName);
                assert.equal('object', typeof data.response.snmpParameters);
              } else {
                runCommonAsserts(data, error);
              }
              webacsProfileName = data.response.profileName;
              saveMockData('Webacs', 'postWebacsApiV4OpCredentialProfilesManagementDeviceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webacsJobName = 'fakedata';
    const webacsPostWebacsApiV4OpDevicesRemovalJobBodyParam = {
      deleteAPs: true,
      ipAddressList: [
        {}
      ]
    };
    describe('#postWebacsApiV4OpDevicesRemovalJob - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpDevicesRemovalJob(webacsPostWebacsApiV4OpDevicesRemovalJobBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobType);
              } else {
                runCommonAsserts(data, error);
              }
              webacsJobName = data.response.jobName;
              saveMockData('Webacs', 'postWebacsApiV4OpDevicesRemovalJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpDevicesSyncDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpDevicesSyncDevices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.devices));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpDevicesSyncDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpGuestUserBodyParam = {
      applyGuestUserTo: {},
      configGroup: 'string',
      controllerId: [
        {}
      ],
      description: 'string',
      disclaimer: 'string',
      endTime: {},
      locationGroupId: {},
      password: 'string',
      profile: 'string',
      rebootController: false,
      saveConfigToFlash: false,
      userRole: 'string',
      username: 'string'
    };
    describe('#postWebacsApiV4OpGuestUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpGuestUser(webacsPostWebacsApiV4OpGuestUserBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.operationType);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpGuestUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpImageAddBodyParam = {
      applicationType: 'string',
      family: 'string',
      imageType: 'string',
      imageURL: 'string',
      minDisk: 'string',
      minMemory: 'string',
      minProcessors: 'string',
      version: 'string'
    };
    describe('#postWebacsApiV4OpImageAdd - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpImageAdd(webacsPostWebacsApiV4OpImageAddBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpImageAdd', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpImageDeployConfigureBodyParam = {
      deployConfigs: [
        {}
      ],
      imageConfigType: 'string',
      imageFileName: 'string',
      imageId: 'string',
      powerOn: false,
      server: 'string',
      vmDeviceType: 'string'
    };
    describe('#postWebacsApiV4OpImageDeployConfigure - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpImageDeployConfigure(webacsPostWebacsApiV4OpImageDeployConfigureBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.jobName);
                assert.equal('string', data.response.jobScheduleStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpImageDeployConfigure', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webacsInterfaceName = 'fakedata';
    describe('#postWebacsApiV4OpMacFilterMacFilterTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpMacFilterMacFilterTemplate((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.deviceType);
                assert.equal('string', data.response.interfaceName);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal('string', data.response.path);
                assert.equal('string', data.response.profileName);
                assert.equal('string', data.response.templateName);
              } else {
                runCommonAsserts(data, error);
              }
              webacsInterfaceName = data.response.interfaceName;
              saveMockData('Webacs', 'postWebacsApiV4OpMacFilterMacFilterTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpNfvAddBridgeBodyParam = {
      bridges: [
        {}
      ],
      server: {},
      transactionId: 'string'
    };
    describe('#postWebacsApiV4OpNfvAddBridge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpNfvAddBridge(webacsPostWebacsApiV4OpNfvAddBridgeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.deploymentStage);
                assert.equal('string', data.response.eventTime);
                assert.equal('string', data.response.payload);
                assert.equal(6, data.response.statusCode);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.transactionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpNfvAddBridge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpNfvCreateOvsBridgeBodyParam = {
      networks: [
        {}
      ],
      server: {},
      transactionId: 'string'
    };
    describe('#postWebacsApiV4OpNfvCreateOvsBridge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpNfvCreateOvsBridge(webacsPostWebacsApiV4OpNfvCreateOvsBridgeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.deploymentStage);
                assert.equal('string', data.response.eventTime);
                assert.equal('string', data.response.payload);
                assert.equal(6, data.response.statusCode);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.transactionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpNfvCreateOvsBridge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpNfvDeleteBridgeBodyParam = {
      bridges: [
        {}
      ],
      server: {},
      transactionId: 'string'
    };
    describe('#postWebacsApiV4OpNfvDeleteBridge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpNfvDeleteBridge(webacsPostWebacsApiV4OpNfvDeleteBridgeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.deploymentStage);
                assert.equal('string', data.response.eventTime);
                assert.equal('string', data.response.payload);
                assert.equal(4, data.response.statusCode);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.transactionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpNfvDeleteBridge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpNfvDeleteOvsBridgeBodyParam = {
      networks: [
        {}
      ],
      server: {},
      transactionId: 'string'
    };
    describe('#postWebacsApiV4OpNfvDeleteOvsBridge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpNfvDeleteOvsBridge(webacsPostWebacsApiV4OpNfvDeleteOvsBridgeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.deploymentStage);
                assert.equal('string', data.response.eventTime);
                assert.equal('string', data.response.payload);
                assert.equal(5, data.response.statusCode);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.transactionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpNfvDeleteOvsBridge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpNfvDeployBodyParam = {
      configuration: [
        {}
      ],
      server: {},
      serviceType: 'string',
      servicedefinition: {},
      transactionId: 'string'
    };
    describe('#postWebacsApiV4OpNfvDeploy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpNfvDeploy(webacsPostWebacsApiV4OpNfvDeployBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.deploymentStage);
                assert.equal('string', data.response.eventTime);
                assert.equal('string', data.response.payload);
                assert.equal(9, data.response.statusCode);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.transactionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpNfvDeploy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpNfvRegisterBodyParam = {
      server: {},
      servicedefinition: {},
      transactionId: 'string'
    };
    describe('#postWebacsApiV4OpNfvRegister - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpNfvRegister(webacsPostWebacsApiV4OpNfvRegisterBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.deploymentStage);
                assert.equal('string', data.response.eventTime);
                assert.equal('string', data.response.payload);
                assert.equal(10, data.response.statusCode);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.transactionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpNfvRegister', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpNfvUndeployBodyParam = {
      configuration: [
        {}
      ],
      server: {},
      serviceType: 'string',
      servicedefinition: {},
      transactionId: 'string'
    };
    describe('#postWebacsApiV4OpNfvUndeploy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpNfvUndeploy(webacsPostWebacsApiV4OpNfvUndeployBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.deploymentStage);
                assert.equal('string', data.response.eventTime);
                assert.equal('string', data.response.payload);
                assert.equal(3, data.response.statusCode);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.transactionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpNfvUndeploy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpNfvUnregisterBodyParam = {
      server: {},
      servicedefinition: {},
      transactionId: 'string'
    };
    describe('#postWebacsApiV4OpNfvUnregister - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpNfvUnregister(webacsPostWebacsApiV4OpNfvUnregisterBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.deploymentStage);
                assert.equal('string', data.response.eventTime);
                assert.equal('string', data.response.payload);
                assert.equal(6, data.response.statusCode);
                assert.equal('string', data.response.statusMessage);
                assert.equal('string', data.response.transactionId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpNfvUnregister', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsStartTime = 555;
    const webacsEndTime = 555;
    describe('#postWebacsApiV4OpPfrMonitoringExtGetDSCPvsAllTCAViaPostStartTimeEndTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetDSCPvsAllTCAViaPostStartTimeEndTime(webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.dSCPs);
                assert.equal('string', data.response.fromLGs);
                assert.equal('string', data.response.lGRole);
                assert.equal('string', data.response.lGs);
                assert.equal(true, Array.isArray(data.response.pfREventMetricListDTO));
                assert.equal('string', data.response.pfREvents);
                assert.equal('string', data.response.serviceProviders);
                assert.equal('string', data.response.toLGs);
                assert.equal('string', data.response.vrfs);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpPfrMonitoringExtGetDSCPvsAllTCAViaPostStartTimeEndTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpPfrMonitoringExtGetDSCPvsUrTCAViaPostStartTimeEndTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetDSCPvsUrTCAViaPostStartTimeEndTime(webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.dSCPs);
                assert.equal('string', data.response.fromLGs);
                assert.equal('string', data.response.lGRole);
                assert.equal('string', data.response.lGs);
                assert.equal(true, Array.isArray(data.response.pfREventMetricListDTO));
                assert.equal('string', data.response.pfREvents);
                assert.equal('string', data.response.serviceProviders);
                assert.equal('string', data.response.toLGs);
                assert.equal('string', data.response.vrfs);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpPfrMonitoringExtGetDSCPvsUrTCAViaPostStartTimeEndTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpPfrMonitoringExtGetMaxDelayVsDSCPChartReportsViaPostStartTimeEndTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetMaxDelayVsDSCPChartReportsViaPostStartTimeEndTime(webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.dSCPs);
                assert.equal('string', data.response.fromLGs);
                assert.equal('string', data.response.lGRole);
                assert.equal('string', data.response.lGs);
                assert.equal(true, Array.isArray(data.response.pfREventMetricListDTO));
                assert.equal('string', data.response.pfREvents);
                assert.equal('string', data.response.serviceProviders);
                assert.equal('string', data.response.toLGs);
                assert.equal('string', data.response.vrfs);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpPfrMonitoringExtGetMaxDelayVsDSCPChartReportsViaPostStartTimeEndTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpPfrMonitoringExtGetMaxJitterVsDSCPChartReportsViaPostStartTimeEndTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetMaxJitterVsDSCPChartReportsViaPostStartTimeEndTime(webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.dSCPs);
                assert.equal('string', data.response.fromLGs);
                assert.equal('string', data.response.lGRole);
                assert.equal('string', data.response.lGs);
                assert.equal(true, Array.isArray(data.response.pfREventMetricListDTO));
                assert.equal('string', data.response.pfREvents);
                assert.equal('string', data.response.serviceProviders);
                assert.equal('string', data.response.toLGs);
                assert.equal('string', data.response.vrfs);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpPfrMonitoringExtGetMaxJitterVsDSCPChartReportsViaPostStartTimeEndTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpPfrMonitoringExtGetMaxPacketLossVsDSCPChartReportsViaPostStartTimeEndTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetMaxPacketLossVsDSCPChartReportsViaPostStartTimeEndTime(webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.dSCPs);
                assert.equal('string', data.response.fromLGs);
                assert.equal('string', data.response.lGRole);
                assert.equal('string', data.response.lGs);
                assert.equal(true, Array.isArray(data.response.pfREventMetricListDTO));
                assert.equal('string', data.response.pfREvents);
                assert.equal('string', data.response.serviceProviders);
                assert.equal('string', data.response.toLGs);
                assert.equal('string', data.response.vrfs);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpPfrMonitoringExtGetMaxPacketLossVsDSCPChartReportsViaPostStartTimeEndTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpPfrMonitoringExtGetPfrEventListViaPostStartTimeEndTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetPfrEventListViaPostStartTimeEndTime(webacsEndTime, webacsStartTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.dSCPs);
                assert.equal('string', data.response.fromLGs);
                assert.equal('string', data.response.lGRole);
                assert.equal('string', data.response.lGs);
                assert.equal(true, Array.isArray(data.response.pfREventMetricListDTO));
                assert.equal('string', data.response.pfREvents);
                assert.equal('string', data.response.serviceProviders);
                assert.equal('string', data.response.toLGs);
                assert.equal('string', data.response.vrfs);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpPfrMonitoringExtGetPfrEventListViaPostStartTimeEndTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsTopN = 555;
    describe('#postWebacsApiV4OpPfrMonitoringExtGetServiceProviderUsageVsDSCPChartStartTimeEndTimeTopN - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpPfrMonitoringExtGetServiceProviderUsageVsDSCPChartStartTimeEndTimeTopN(webacsStartTime, webacsEndTime, webacsTopN, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.dSCPs);
                assert.equal('string', data.response.fromLGs);
                assert.equal('string', data.response.lGRole);
                assert.equal('string', data.response.lGs);
                assert.equal(true, Array.isArray(data.response.pfREventMetricListDTO));
                assert.equal('string', data.response.pfREvents);
                assert.equal('string', data.response.serviceProviders);
                assert.equal('string', data.response.toLGs);
                assert.equal('string', data.response.vrfs);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpPfrMonitoringExtGetServiceProviderUsageVsDSCPChartStartTimeEndTimeTopN', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPnpProfileFolder = 'fakedata';
    describe('#postWebacsApiV4OpPnpProfilePnpProfileFolderProfileName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpPnpProfilePnpProfileFolderProfileName(webacsPnpProfileFolder, webacsProfileName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.apicEmProfileOptions);
                assert.equal('string', data.response.bootStrapTemplateName);
                assert.equal('string', data.response.configurationTemplateName);
                assert.equal('string', data.response.credentialProfileName);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.deviceType);
                assert.equal('string', data.response.imageLocation);
                assert.equal('string', data.response.imageName);
                assert.equal('object', typeof data.response.nexusProfileDetails);
                assert.equal('string', data.response.postPnPTemplateName);
                assert.equal('string', data.response.profileName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpPnpProfilePnpProfileFolderProfileName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webacsIpAddress = 'fakedata';
    describe('#postWebacsApiV4OpSettingsLoggingSysLogSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpSettingsLoggingSysLogSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.serverType);
              } else {
                runCommonAsserts(data, error);
              }
              webacsIpAddress = data.response.ipAddress;
              saveMockData('Webacs', 'postWebacsApiV4OpSettingsLoggingSysLogSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpUserManagementUserGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpUserManagementUserGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.userGroups);
                assert.equal('string', data.response.virtualDomains);
                assert.equal(true, data.response.wirelessUser);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpUserManagementUserGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpVdBodyParam = {
      accessPoints: [
        {}
      ],
      description: 'string',
      devices: [
        {}
      ],
      domainFQN: 'string',
      dynamicGroups: [
        'string'
      ],
      email: 'string',
      groups: [
        'string'
      ],
      siteMaps: [
        {}
      ],
      timezone: 'string',
      virtualElements: [
        {}
      ]
    };
    describe('#postWebacsApiV4OpVd - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpVd(webacsPostWebacsApiV4OpVdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.warning);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpVd', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpVirtualAddDeviceBodyParam = {
      ip: 'string',
      password: 'string',
      port: 3,
      protocol: 'string',
      username: 'string'
    };
    describe('#postWebacsApiV4OpVirtualAddDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpVirtualAddDevice(webacsPostWebacsApiV4OpVirtualAddDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpVirtualAddDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postWebacsApiV4OpWlanProvisioningWlanTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postWebacsApiV4OpWlanProvisioningWlanTemplate((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.adminStatus);
                assert.equal(true, data.response.broadcastSsidEnabled);
                assert.equal('string', data.response.interfaceName);
                assert.equal(8, data.response.interfaceType);
                assert.equal('string', data.response.lanType);
                assert.equal('object', typeof data.response.layer2Security);
                assert.equal('string', data.response.profileName);
                assert.equal('string', data.response.ssid);
                assert.equal('string', data.response.templateName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'postWebacsApiV4OpWlanProvisioningWlanTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webacsType = 'fakedata';
    let webacsControllerName = 'fakedata';
    describe('#getWebacsApiV4DataAccessPoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataAccessPoints((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.adminStatus);
                assert.equal('string', data.response.bootVersion);
                assert.equal(7, data.response.clientCount);
                assert.equal(2, data.response.clientCount24GHz);
                assert.equal(5, data.response.clientCount5GHz);
                assert.equal('string', data.response.controllerIpAddress);
                assert.equal('string', data.response.controllerName);
                assert.equal('string', data.response.countryCode);
                assert.equal('object', typeof data.response.ethernetMac);
                assert.equal(true, data.response.hreapEnabled);
                assert.equal('object', typeof data.response.ipAddress);
                assert.equal('string', data.response.location);
                assert.equal(9, data.response.lwappUpTime);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.serialNumber);
                assert.equal('string', data.response.softwareVersion);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.type);
                assert.equal(3, data.response.upTime);
              } else {
                runCommonAsserts(data, error);
              }
              webacsType = data.response.type;
              webacsControllerName = data.response.controllerName;
              saveMockData('Webacs', 'getWebacsApiV4DataAccessPoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataApOnboardingProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataApOnboardingProfile((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.apName);
                assert.equal('string', data.response.apTemplateOne);
                assert.equal('string', data.response.apTemplateThree);
                assert.equal('string', data.response.apTemplateTwo);
                assert.equal('string', data.response.associatedControllerName);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal('object', typeof data.response.nextStage);
                assert.equal('string', data.response.profileGroup);
                assert.equal('object', typeof data.response.profileMode);
                assert.equal('object', typeof data.response.profileStatus);
                assert.equal('string', data.response.reason);
                assert.equal('string', data.response.serialNumber);
                assert.equal(9, data.response.updateTime);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataApOnboardingProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataApiHealthRecords - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataApiHealthRecords((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.clientIp);
                assert.equal('string', data.response.method);
                assert.equal('string', data.response.path);
                assert.equal('string', data.response.query);
                assert.equal('string', data.response.requestAcceptHeader);
                assert.equal(2, data.response.requestReceivedTime);
                assert.equal(1, data.response.responseSentTime);
                assert.equal(8, data.response.responseStatus);
                assert.equal(2, data.response.responseTime);
                assert.equal('string', data.response.userAgent);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataApiHealthRecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataApiResponseTimeSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataApiResponseTimeSummary((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.average);
                assert.equal(4, data.response.latest);
                assert.equal(7, data.response.max);
                assert.equal(8, data.response.min);
                assert.equal('string', data.response.path);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataApiResponseTimeSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webacsCategory = 'fakedata';
    describe('#getWebacsApiV4DataApplications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataApplications((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.applicationId);
                assert.equal('string', data.response.applicationName);
                assert.equal(false, data.response.businessCritical);
                assert.equal('string', data.response.category);
                assert.equal('string', data.response.encrypted);
                assert.equal('string', data.response.engineId);
                assert.equal(6, data.response.modified);
                assert.equal(false, data.response.outOfBox);
                assert.equal('string', data.response.p2p);
                assert.equal(1, data.response.selector);
                assert.equal(true, Array.isArray(data.response.serviceGroup));
                assert.equal('string', data.response.subcategory);
                assert.equal('string', data.response.tunnel);
              } else {
                runCommonAsserts(data, error);
              }
              webacsCategory = data.response.category;
              saveMockData('Webacs', 'getWebacsApiV4DataApplications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataAutoApRadioDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataAutoApRadioDetails((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.adminStatus);
                assert.equal(8, data.response.antennaAzimAngle);
                assert.equal(3, data.response.antennaElevAngle);
                assert.equal(5, data.response.antennaGain);
                assert.equal('string', data.response.antennaName);
                assert.equal('object', typeof data.response.apIpAddress);
                assert.equal('string', data.response.apName);
                assert.equal('object', typeof data.response.baseRadioMac);
                assert.equal('string', data.response.channelNumber);
                assert.equal(7, data.response.clientCount);
                assert.equal('object', typeof data.response.ethernetMac);
                assert.equal('string', data.response.ifDescr);
                assert.equal(false, data.response.nativePowerUseStandard);
                assert.equal('string', data.response.operBand);
                assert.equal('string', data.response.operStatus);
                assert.equal('string', data.response.phyType);
                assert.equal(10, data.response.powerLevel);
                assert.equal('object', typeof data.response.radioMac);
                assert.equal('string', data.response.radioRole);
                assert.equal('string', data.response.radioType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataAutoApRadioDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataBulkSanitizedConfigArchives - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataBulkSanitizedConfigArchives((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.comments);
                assert.equal(10, data.response.createdAt);
                assert.equal('string', data.response.createdBy);
                assert.equal(5, data.response.deviceId);
                assert.equal('string', data.response.deviceIpAddress);
                assert.equal('string', data.response.deviceName);
                assert.equal('string', data.response.diffType);
                assert.equal(true, Array.isArray(data.response.files));
                assert.equal(true, data.response.isFirst);
                assert.equal(true, data.response.isLast);
                assert.equal(false, data.response.outOfBand);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataBulkSanitizedConfigArchives', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataBulkUnsanitizedConfigArchives - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataBulkUnsanitizedConfigArchives((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.comments);
                assert.equal(1, data.response.createdAt);
                assert.equal('string', data.response.createdBy);
                assert.equal(10, data.response.deviceId);
                assert.equal('string', data.response.deviceIpAddress);
                assert.equal('string', data.response.deviceName);
                assert.equal('string', data.response.diffType);
                assert.equal(true, Array.isArray(data.response.files));
                assert.equal(true, data.response.isFirst);
                assert.equal(false, data.response.isLast);
                assert.equal(true, data.response.outOfBand);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataBulkUnsanitizedConfigArchives', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataCliTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataCliTemplate((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.author);
                assert.equal(5, data.response.configContainerId);
                assert.equal('string', data.response.content);
                assert.equal(7, data.response.createdOn);
                assert.equal(9, data.response.deployCount);
                assert.equal(3, data.response.deployJobCount);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.deviceType);
                assert.equal(6, data.response.lastDeployTime);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.path);
                assert.equal('string', data.response.tags);
                assert.equal(1, data.response.templateId);
                assert.equal(true, Array.isArray(data.response.userTags));
                assert.equal(true, Array.isArray(data.response.variable));
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataCliTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataClientCounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataClientCounts((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.authCount);
                assert.equal(6, data.response.collectionTime);
                assert.equal(7, data.response.count);
                assert.equal(4, data.response.dot11aAuthCount);
                assert.equal(10, data.response.dot11aCount);
                assert.equal(7, data.response.dot11acAuthCount);
                assert.equal(5, data.response.dot11acCount);
                assert.equal(9, data.response.dot11bAuthCount);
                assert.equal(8, data.response.dot11bCount);
                assert.equal(3, data.response.dot11gAuthCount);
                assert.equal(6, data.response.dot11gCount);
                assert.equal(9, data.response.dot11n24AuthCount);
                assert.equal(8, data.response.dot11n24Count);
                assert.equal(10, data.response.dot11n5AuthCount);
                assert.equal(3, data.response.dot11n5Count);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.subkey);
                assert.equal('string', data.response.type);
                assert.equal(2, data.response.wgbAuthCount);
                assert.equal(3, data.response.wgbCount);
                assert.equal(5, data.response.wired100MAuthCount);
                assert.equal(6, data.response.wired100MCount);
                assert.equal(7, data.response.wired10GAuthCount);
                assert.equal(2, data.response.wired10GCount);
                assert.equal(1, data.response.wired10MAuthCount);
                assert.equal(1, data.response.wired10MCount);
                assert.equal(8, data.response.wired1GAuthCount);
                assert.equal(9, data.response.wired1GCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataClientCounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataClientSessions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataClientSessions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.adDomainName);
                assert.equal('object', typeof data.response.anchorIpAddress);
                assert.equal('object', typeof data.response.apMacAddress);
                assert.equal('string', data.response.authenticationAlgorithm);
                assert.equal('string', data.response.authorizationPolicy);
                assert.equal(7, data.response.bytesReceived);
                assert.equal(7, data.response.bytesSent);
                assert.equal('string', data.response.clientInterface);
                assert.equal('string', data.response.connectionType);
                assert.equal('string', data.response.ctsSecurityGroup);
                assert.equal('object', typeof data.response.deviceMgmtAddress);
                assert.equal('string', data.response.deviceName);
                assert.equal('string', data.response.eapType);
                assert.equal('string', data.response.encryptionCypher);
                assert.equal('object', typeof data.response.ipAddress);
                assert.equal('string', data.response.ipType);
                assert.equal('string', data.response.location);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal(8, data.response.packetsReceived);
                assert.equal(7, data.response.packetsSent);
                assert.equal('string', data.response.policyTypeStatus);
                assert.equal('string', data.response.portSpeed);
                assert.equal('string', data.response.postureStatus);
                assert.equal('string', data.response.profileName);
                assert.equal('string', data.response.protocol);
                assert.equal('string', data.response.roamReason);
                assert.equal(6, data.response.rssi);
                assert.equal('string', data.response.securityPolicy);
                assert.equal(7, data.response.sessionEndTime);
                assert.equal(2, data.response.sessionStartTime);
                assert.equal(8, data.response.snr);
                assert.equal('string', data.response.ssid);
                assert.equal(10, data.response.throughput);
                assert.equal('string', data.response.userName);
                assert.equal(4, data.response.vlan);
                assert.equal('string', data.response.webSecurity);
                assert.equal('object', typeof data.response.wgbMacAddress);
                assert.equal('string', data.response.wgbStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataClientSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataClientStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataClientStats((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.bytesReceived);
                assert.equal(10, data.response.bytesSent);
                assert.equal(1, data.response.collectionTime);
                assert.equal(7, data.response.dataRate);
                assert.equal(1, data.response.dataRetries);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal(4, data.response.packetsReceived);
                assert.equal(7, data.response.packetsSent);
                assert.equal(6, data.response.raPacketsDropped);
                assert.equal(3, data.response.rssi);
                assert.equal(2, data.response.rtsRetries);
                assert.equal(10, data.response.rxBytesDropped);
                assert.equal(9, data.response.rxPacketsDropped);
                assert.equal(8, data.response.snr);
                assert.equal(5, data.response.txBytesDropped);
                assert.equal(1, data.response.txPacketsDropped);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataClientStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataClientTraffics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataClientTraffics((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.collectionTime);
                assert.equal(10, data.response.dot11aReceived);
                assert.equal(6, data.response.dot11aSent);
                assert.equal(5, data.response.dot11aThroughput);
                assert.equal(10, data.response.dot11acReceived);
                assert.equal(2, data.response.dot11acSent);
                assert.equal(4, data.response.dot11acThroughput);
                assert.equal(3, data.response.dot11bReceived);
                assert.equal(7, data.response.dot11bSent);
                assert.equal(6, data.response.dot11bThroughput);
                assert.equal(1, data.response.dot11gReceived);
                assert.equal(1, data.response.dot11gSent);
                assert.equal(2, data.response.dot11gThroughput);
                assert.equal(5, data.response.dot11n24Received);
                assert.equal(7, data.response.dot11n24Sent);
                assert.equal(4, data.response.dot11n24Throughput);
                assert.equal(2, data.response.dot11n5Received);
                assert.equal(5, data.response.dot11n5Sent);
                assert.equal(7, data.response.dot11n5Throughput);
                assert.equal('string', data.response.key);
                assert.equal(9, data.response.received);
                assert.equal(5, data.response.sent);
                assert.equal('string', data.response.subkey);
                assert.equal(5, data.response.throughput);
                assert.equal('string', data.response.type);
                assert.equal(4, data.response.wired100MReceived);
                assert.equal(9, data.response.wired100MSent);
                assert.equal(2, data.response.wired100MThroughput);
                assert.equal(10, data.response.wired10GReceived);
                assert.equal(4, data.response.wired10GSent);
                assert.equal(10, data.response.wired10GThroughput);
                assert.equal(1, data.response.wired10MReceived);
                assert.equal(6, data.response.wired10MSent);
                assert.equal(2, data.response.wired10MThroughput);
                assert.equal(1, data.response.wired1GReceived);
                assert.equal(7, data.response.wired1GSent);
                assert.equal(8, data.response.wired1GThroughput);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataClientTraffics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataConfigArchives - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataConfigArchives((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.deviceIpAddress);
                assert.equal('string', data.response.deviceName);
                assert.equal('string', data.response.lastMessage);
                assert.equal(true, data.response.lastSuccessful);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataConfigArchives', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataConfigVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataConfigVersions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.comments);
                assert.equal(5, data.response.createdAt);
                assert.equal('string', data.response.createdBy);
                assert.equal('string', data.response.deviceIpAddress);
                assert.equal('string', data.response.deviceName);
                assert.equal('string', data.response.diffType);
                assert.equal(true, Array.isArray(data.response.fileInfos));
                assert.equal(false, data.response.isFirst);
                assert.equal(true, data.response.isLast);
                assert.equal(false, data.response.outOfBand);
                assert.equal(true, Array.isArray(data.response.tags));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataConfigVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webacsGroupPath = 'fakedata';
    describe('#getWebacsApiV4DataGroupSpecification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataGroupSpecification((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.groupName);
                assert.equal('string', data.response.groupPath);
                assert.equal(5, data.response.parentId);
              } else {
                runCommonAsserts(data, error);
              }
              webacsGroupPath = data.response.groupPath;
              saveMockData('Webacs', 'getWebacsApiV4DataGroupSpecification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webacsControllerId = 'fakedata';
    describe('#getWebacsApiV4DataGuestUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataGuestUsers((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.applyGuestUserTo);
                assert.equal('string', data.response.building);
                assert.equal('string', data.response.campus);
                assert.equal('string', data.response.configGroup);
                assert.equal(true, Array.isArray(data.response.controllerId));
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.disclaimer);
                assert.equal(7, data.response.endTime);
                assert.equal('string', data.response.floor);
                assert.equal(2, data.response.lastModifiedTime);
                assert.equal('string', data.response.outdoorArea);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.profile);
                assert.equal(false, data.response.rebootController);
                assert.equal(false, data.response.saveConfigToFlash);
                assert.equal('object', typeof data.response.status);
                assert.equal('string', data.response.userRole);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              webacsControllerId = data.response.controllerId;
              saveMockData('Webacs', 'getWebacsApiV4DataGuestUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalClientCounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataHistoricalClientCounts((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.authCount);
                assert.equal(6, data.response.collectionTime);
                assert.equal(8, data.response.count);
                assert.equal(7, data.response.dot11aAuthCount);
                assert.equal(10, data.response.dot11aCount);
                assert.equal(3, data.response.dot11acAuthCount);
                assert.equal(10, data.response.dot11acCount);
                assert.equal(7, data.response.dot11bAuthCount);
                assert.equal(4, data.response.dot11bCount);
                assert.equal(1, data.response.dot11gAuthCount);
                assert.equal(10, data.response.dot11gCount);
                assert.equal(5, data.response.dot11n24AuthCount);
                assert.equal(7, data.response.dot11n24Count);
                assert.equal(4, data.response.dot11n5AuthCount);
                assert.equal(5, data.response.dot11n5Count);
                assert.equal('string', data.response.key);
                assert.equal('string', data.response.subkey);
                assert.equal('string', data.response.type);
                assert.equal(7, data.response.wgbAuthCount);
                assert.equal(1, data.response.wgbCount);
                assert.equal(9, data.response.wired100MAuthCount);
                assert.equal(1, data.response.wired100MCount);
                assert.equal(4, data.response.wired10GAuthCount);
                assert.equal(2, data.response.wired10GCount);
                assert.equal(2, data.response.wired10MAuthCount);
                assert.equal(3, data.response.wired10MCount);
                assert.equal(7, data.response.wired1GAuthCount);
                assert.equal(6, data.response.wired1GCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataHistoricalClientCounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalClientStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataHistoricalClientStats((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.bytesReceived);
                assert.equal(6, data.response.bytesSent);
                assert.equal(8, data.response.collectionTime);
                assert.equal(9, data.response.dataRate);
                assert.equal(7, data.response.dataRetries);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal(1, data.response.packetsReceived);
                assert.equal(7, data.response.packetsSent);
                assert.equal(3, data.response.raPacketsDropped);
                assert.equal(9, data.response.rssi);
                assert.equal(8, data.response.rtsRetries);
                assert.equal(10, data.response.rxBytesDropped);
                assert.equal(2, data.response.rxPacketsDropped);
                assert.equal(1, data.response.snr);
                assert.equal(9, data.response.txBytesDropped);
                assert.equal(7, data.response.txPacketsDropped);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataHistoricalClientStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalClientTraffics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataHistoricalClientTraffics((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.collectionTime);
                assert.equal(3, data.response.dot11aReceived);
                assert.equal(8, data.response.dot11aSent);
                assert.equal(1, data.response.dot11aThroughput);
                assert.equal(1, data.response.dot11acReceived);
                assert.equal(2, data.response.dot11acSent);
                assert.equal(8, data.response.dot11acThroughput);
                assert.equal(5, data.response.dot11bReceived);
                assert.equal(3, data.response.dot11bSent);
                assert.equal(4, data.response.dot11bThroughput);
                assert.equal(9, data.response.dot11gReceived);
                assert.equal(3, data.response.dot11gSent);
                assert.equal(4, data.response.dot11gThroughput);
                assert.equal(6, data.response.dot11n24Received);
                assert.equal(2, data.response.dot11n24Sent);
                assert.equal(6, data.response.dot11n24Throughput);
                assert.equal(7, data.response.dot11n5Received);
                assert.equal(8, data.response.dot11n5Sent);
                assert.equal(1, data.response.dot11n5Throughput);
                assert.equal('string', data.response.key);
                assert.equal(10, data.response.received);
                assert.equal(1, data.response.sent);
                assert.equal('string', data.response.subkey);
                assert.equal(5, data.response.throughput);
                assert.equal('string', data.response.type);
                assert.equal(6, data.response.wired100MReceived);
                assert.equal(1, data.response.wired100MSent);
                assert.equal(7, data.response.wired100MThroughput);
                assert.equal(5, data.response.wired10GReceived);
                assert.equal(8, data.response.wired10GSent);
                assert.equal(4, data.response.wired10GThroughput);
                assert.equal(6, data.response.wired10MReceived);
                assert.equal(10, data.response.wired10MSent);
                assert.equal(10, data.response.wired10MThroughput);
                assert.equal(4, data.response.wired1GReceived);
                assert.equal(3, data.response.wired1GSent);
                assert.equal(5, data.response.wired1GThroughput);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataHistoricalClientTraffics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalRFCounters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataHistoricalRFCounters((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.ackFailureCount);
                assert.equal(1, data.response.collectionTime);
                assert.equal(4, data.response.failedCount);
                assert.equal(2, data.response.fcsErrorCount);
                assert.equal(4, data.response.frameDuplicateCount);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal(9, data.response.multipleRetryCount);
                assert.equal(10, data.response.retryCount);
                assert.equal(4, data.response.rtsFailureCount);
                assert.equal(9, data.response.rtsSuccessCount);
                assert.equal(10, data.response.rxFragmentCount);
                assert.equal(8, data.response.rxMulticastFrameCount);
                assert.equal(3, data.response.slotId);
                assert.equal(8, data.response.txFragmentCount);
                assert.equal(2, data.response.txFrameCount);
                assert.equal(7, data.response.txMulticastFrameCount);
                assert.equal(7, data.response.wepUndecryptableCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataHistoricalRFCounters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalRFLoadStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataHistoricalRFLoadStats((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.channelUtilization);
                assert.equal(3, data.response.clientCount);
                assert.equal(1, data.response.collectionTime);
                assert.equal('object', typeof data.response.ethernetMac);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal(5, data.response.poorCoverageClients);
                assert.equal(6, data.response.rxUtilization);
                assert.equal(10, data.response.slotId);
                assert.equal(4, data.response.txUtilization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataHistoricalRFLoadStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalRFStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataHistoricalRFStats((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.channelNumber);
                assert.equal(8, data.response.clientCount);
                assert.equal(2, data.response.collectionTime);
                assert.equal('string', data.response.coverageProfile);
                assert.equal('object', typeof data.response.ethernetMac);
                assert.equal('string', data.response.interferenceProfile);
                assert.equal('string', data.response.loadProfile);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal('string', data.response.noiseProfile);
                assert.equal('string', data.response.operStatus);
                assert.equal(2, data.response.powerLevel);
                assert.equal(6, data.response.slotId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataHistoricalRFStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalWLCCPUUtilizations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataHistoricalWLCCPUUtilizations((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.averageCPUUtilization);
                assert.equal(6, data.response.collectionTime);
                assert.equal(true, Array.isArray(data.response.cpuUtilization));
                assert.equal('object', typeof data.response.ipAddress);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataHistoricalWLCCPUUtilizations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataHistoricalWLCMemUtilizations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataHistoricalWLCMemUtilizations((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.collectionTime);
                assert.equal('object', typeof data.response.ipAddress);
                assert.equal(2, data.response.memoryFree);
                assert.equal(true, Array.isArray(data.response.memoryPoolUtilization));
                assert.equal(8, data.response.memoryTotal);
                assert.equal(7, data.response.memoryUsed);
                assert.equal(4, data.response.memoryUtilization);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataHistoricalWLCMemUtilizations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataInventoryDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataInventoryDetails((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.cdpNeighbor));
                assert.equal(true, Array.isArray(data.response.chassis));
                assert.equal(true, Array.isArray(data.response.etherChannel));
                assert.equal(1, data.response.ethernetInterface);
                assert.equal(true, Array.isArray(data.response.fan));
                assert.equal(4, data.response.ipInterface);
                assert.equal(true, Array.isArray(data.response.module));
                assert.equal(true, Array.isArray(data.response.physicalPort));
                assert.equal(true, Array.isArray(data.response.powerSupply));
                assert.equal(true, Array.isArray(data.response.sensor));
                assert.equal('object', typeof data.response.summary);
                assert.equal(true, Array.isArray(data.response.udf));
                assert.equal(5, data.response.vlanInterface);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataInventoryDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataJobSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataJobSummary((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.completionTime);
                assert.equal('string', data.response.description);
                assert.equal(9, data.response.duration);
                assert.equal('string', data.response.jobName);
                assert.equal('object', typeof data.response.jobStatus);
                assert.equal('string', data.response.jobType);
                assert.equal(4, data.response.lastStartTime);
                assert.equal(3, data.response.nextRunTime);
                assert.equal('object', typeof data.response.resultStatus);
                assert.equal(2, data.response.runId);
                assert.equal('object', typeof data.response.runStatus);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataJobSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataMacFilterTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataMacFilterTemplates((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.author);
                assert.equal(2, data.response.configContainerId);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.deviceType);
                assert.equal('string', data.response.interfaceName);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal('string', data.response.path);
                assert.equal('string', data.response.profileName);
                assert.equal(true, Array.isArray(data.response.tags));
                assert.equal('string', data.response.templateName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataMacFilterTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataMerakiAccessPoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataMerakiAccessPoints((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.clientCount);
                assert.equal(10, data.response.contactedAt);
                assert.equal('object', typeof data.response.dashboardAddress);
                assert.equal('string', data.response.deviceName);
                assert.equal('string', data.response.deviceType);
                assert.equal('object', typeof data.response.ipAddress);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal('string', data.response.meshStatus);
                assert.equal('string', data.response.networkName);
                assert.equal('string', data.response.productFamily);
                assert.equal('string', data.response.reachability);
                assert.equal('string', data.response.serialNumber);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataMerakiAccessPoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataMerakiDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataMerakiDevices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.clientCount);
                assert.equal(9, data.response.contactedAt);
                assert.equal('object', typeof data.response.dashboardAddress);
                assert.equal('string', data.response.deviceName);
                assert.equal('string', data.response.deviceType);
                assert.equal('object', typeof data.response.ipAddress);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal('string', data.response.networkName);
                assert.equal('string', data.response.productFamily);
                assert.equal('string', data.response.reachability);
                assert.equal('string', data.response.serialNumber);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataMerakiDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataRFCounters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataRFCounters((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.ackFailureCount);
                assert.equal(5, data.response.collectionTime);
                assert.equal(9, data.response.failedCount);
                assert.equal(8, data.response.fcsErrorCount);
                assert.equal(4, data.response.frameDuplicateCount);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal(8, data.response.multipleRetryCount);
                assert.equal(9, data.response.retryCount);
                assert.equal(10, data.response.rtsFailureCount);
                assert.equal(3, data.response.rtsSuccessCount);
                assert.equal(9, data.response.rxFragmentCount);
                assert.equal(6, data.response.rxMulticastFrameCount);
                assert.equal(5, data.response.slotId);
                assert.equal(8, data.response.txFragmentCount);
                assert.equal(10, data.response.txFrameCount);
                assert.equal(3, data.response.txMulticastFrameCount);
                assert.equal(3, data.response.wepUndecryptableCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataRFCounters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataRFLoadStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataRFLoadStats((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.channelUtilization);
                assert.equal(9, data.response.clientCount);
                assert.equal(8, data.response.collectionTime);
                assert.equal('object', typeof data.response.ethernetMac);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal(1, data.response.poorCoverageClients);
                assert.equal(4, data.response.rxUtilization);
                assert.equal(4, data.response.slotId);
                assert.equal(6, data.response.txUtilization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataRFLoadStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataRFStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataRFStats((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.channelNumber);
                assert.equal(8, data.response.clientCount);
                assert.equal(2, data.response.collectionTime);
                assert.equal('string', data.response.coverageProfile);
                assert.equal('object', typeof data.response.ethernetMac);
                assert.equal('string', data.response.interferenceProfile);
                assert.equal('string', data.response.loadProfile);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal('string', data.response.noiseProfile);
                assert.equal('string', data.response.operStatus);
                assert.equal(3, data.response.powerLevel);
                assert.equal(1, data.response.slotId);
                assert.equal(4, data.response.txPowerOutput);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataRFStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataRadioDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataRadioDetails((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.adminStatus);
                assert.equal('string', data.response.alarmStatus);
                assert.equal(2, data.response.antennaAzimAngle);
                assert.equal('string', data.response.antennaDiversity);
                assert.equal(2, data.response.antennaElevAngle);
                assert.equal(9, data.response.antennaGain);
                assert.equal('string', data.response.antennaMode);
                assert.equal('string', data.response.antennaName);
                assert.equal('string', data.response.antennaType);
                assert.equal('object', typeof data.response.apIpAddress);
                assert.equal('string', data.response.apName);
                assert.equal('object', typeof data.response.baseRadioMac);
                assert.equal('string', data.response.channelControl);
                assert.equal('string', data.response.channelNumber);
                assert.equal('string', data.response.channelWidth);
                assert.equal(false, data.response.cleanAirCapable);
                assert.equal('object', typeof data.response.cleanAirSensorStatus);
                assert.equal(false, data.response.cleanAirStatus);
                assert.equal(7, data.response.clientCount);
                assert.equal('object', typeof data.response.controllerIpAddress);
                assert.equal(false, data.response.dot11nCapable);
                assert.equal('object', typeof data.response.ethernetMac);
                assert.equal('string', data.response.operStatus);
                assert.equal(5, data.response.port);
                assert.equal(6, data.response.powerLevel);
                assert.equal('string', data.response.radioBand);
                assert.equal('string', data.response.radioRole);
                assert.equal('string', data.response.radioType);
                assert.equal(5, data.response.slotId);
                assert.equal('string', data.response.txPowerControl);
                assert.equal(7, data.response.txPowerOutput);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataRadioDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataRadios - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataRadios((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.adminStatus);
                assert.equal('object', typeof data.response.apIpAddress);
                assert.equal('string', data.response.apName);
                assert.equal(7, data.response.apType);
                assert.equal('object', typeof data.response.baseRadioMac);
                assert.equal('string', data.response.channelNumber);
                assert.equal(6, data.response.clientCount);
                assert.equal('object', typeof data.response.controllerIpAddress);
                assert.equal('object', typeof data.response.ethernetMac);
                assert.equal('string', data.response.operStatus);
                assert.equal(9, data.response.powerLevel);
                assert.equal('string', data.response.radioType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataRadios', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataThirdpartyAccessPoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataThirdpartyAccessPoints((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.adminStatus);
                assert.equal(8, data.response.clientCount);
                assert.equal(3, data.response.clientCount24GHz);
                assert.equal(5, data.response.clientCount5GHz);
                assert.equal('string', data.response.controllerIpAddress);
                assert.equal('string', data.response.controllerName);
                assert.equal('string', data.response.ethernetMac);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.location);
                assert.equal('string', data.response.macAddress);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.operationalStatus);
                assert.equal('string', data.response.serialNumber);
                assert.equal('string', data.response.softwareVersion);
                assert.equal('string', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataThirdpartyAccessPoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataUserDefinedFieldDefinition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataUserDefinedFieldDefinition((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataUserDefinedFieldDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataVDAssociatedAccessPoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataVDAssociatedAccessPoints((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.controllerIp);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.macAddress);
                assert.equal(6, data.response.mneId);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataVDAssociatedAccessPoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataVDAssociatedDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataVDAssociatedDevices((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataVDAssociatedDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataVDAssociatedDynamicGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataVDAssociatedDynamicGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.hierarchyName);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataVDAssociatedDynamicGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataVDAssociatedGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataVDAssociatedGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.hierarchyName);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataVDAssociatedGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataVDAssociatedSiteMaps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataVDAssociatedSiteMaps((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.hierarchyName);
                assert.equal('string', data.response.name);
                assert.equal(3, data.response.parentId);
                assert.equal(2, data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataVDAssociatedSiteMaps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataVDAssociatedVirtualElements - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataVDAssociatedVirtualElements((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(7, data.response.clusterId);
                assert.equal('string', data.response.clusterName);
                assert.equal(2, data.response.datacenterId);
                assert.equal('string', data.response.datacenterName);
                assert.equal('string', data.response.discoverySource);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.parentId);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataVDAssociatedVirtualElements', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataWLCCPUUtilizations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataWLCCPUUtilizations((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.averageCPUUtilization);
                assert.equal(2, data.response.collectionTime);
                assert.equal(true, Array.isArray(data.response.cpuUtilization));
                assert.equal('object', typeof data.response.ipAddress);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataWLCCPUUtilizations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataWLCMemoryUtilizations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataWLCMemoryUtilizations((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.collectionTime);
                assert.equal('object', typeof data.response.ipAddress);
                assert.equal(9, data.response.memoryFree);
                assert.equal(true, Array.isArray(data.response.memoryPoolUtilization));
                assert.equal(8, data.response.memoryTotal);
                assert.equal(1, data.response.memoryUsed);
                assert.equal(8, data.response.memoryUtilization);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataWLCMemoryUtilizations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataWlanControllers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataWlanControllers((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.alarmStatus);
                assert.equal(7, data.response.apCount);
                assert.equal('string', data.response.auditStatus);
                assert.equal(false, data.response.autoRefresh);
                assert.equal(8, data.response.clientCount);
                assert.equal('string', data.response.contact);
                assert.equal('string', data.response.ipAddress);
                assert.equal(9, data.response.lastBackup);
                assert.equal('string', data.response.location);
                assert.equal('string', data.response.mobilityGroupName);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.reachabilityStatus);
                assert.equal('string', data.response.rfGroupName);
                assert.equal('string', data.response.softwareVersion);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataWlanControllers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataWlanProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataWlanProfiles((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.aaaAllowOverride);
                assert.equal('string', data.response.aaaLdapPrimaryServer);
                assert.equal('string', data.response.aaaLdapSecondaryServer);
                assert.equal('string', data.response.aaaLdapTertiaryServer);
                assert.equal(true, data.response.aaaLocalEapAuthenticationEnabled);
                assert.equal('string', data.response.aaaLocalEapAuthenticationProfileName);
                assert.equal('string', data.response.aaaRadiusAccountingPrimaryServer);
                assert.equal('string', data.response.aaaRadiusAccountingSecondaryServer);
                assert.equal(true, data.response.aaaRadiusAccountingServersEnabled);
                assert.equal('string', data.response.aaaRadiusAccountingTertiaryServer);
                assert.equal('string', data.response.aaaRadiusAuthenticationPrimaryServer);
                assert.equal('string', data.response.aaaRadiusAuthenticationSecondaryServer);
                assert.equal(false, data.response.aaaRadiusAuthenticationServersEnabled);
                assert.equal('string', data.response.aaaRadiusAuthenticationTertiaryServer);
                assert.equal(false, data.response.aaaRadiusInterimUpdateEnabled);
                assert.equal(5, data.response.aaaRadiusInterimUpdateInterval);
                assert.equal(false, data.response.adminStatus);
                assert.equal(true, data.response.advancedAironetIeEnabled);
                assert.equal(true, data.response.advancedClientExclusionEnabled);
                assert.equal(6, data.response.advancedClientExclusionTimeout);
                assert.equal(false, data.response.advancedCoverageHoleDetectionEnabled);
                assert.equal(false, data.response.advancedDhcpAddressAssignmentRequired);
                assert.equal(true, data.response.advancedDhcpProfilingEnabled);
                assert.equal('object', typeof data.response.advancedDhcpServerIpAddress);
                assert.equal(false, data.response.advancedDiagnosticChannelEnabled);
                assert.equal(5, data.response.advancedDot11anDtimPeriod);
                assert.equal(8, data.response.advancedDot11bgnDtimPeriod);
                assert.equal(false, data.response.advancedFlexConnectCentralDhcpProcessingEnabled);
                assert.equal(false, data.response.advancedFlexConnectLearnClientIpEnabled);
                assert.equal(false, data.response.advancedFlexConnectLocalAuthEnabled);
                assert.equal(true, data.response.advancedFlexConnectLocalSwitchingEnabled);
                assert.equal(false, data.response.advancedFlexConnectNatPatEnabled);
                assert.equal(true, data.response.advancedFlexConnectOverrideDnsEnabled);
                assert.equal(true, data.response.advancedFlexConnectReapCentralAssociation);
                assert.equal(false, data.response.advancedFlexConnectVlanCentralSwitchingEnabled);
                assert.equal(true, data.response.advancedHttpProfilingEnabled);
                assert.equal(true, data.response.advancedIpv6Enabled);
                assert.equal(false, data.response.advancedKtsCacEnabled);
                assert.equal(true, data.response.advancedLoadBalancingBandSelectEnabled);
                assert.equal(false, data.response.advancedLoadBalancingEnabled);
                assert.equal(4, data.response.advancedMaximumClients);
                assert.equal('string', data.response.advancedMdnsProfileName);
                assert.equal(true, data.response.advancedMdnsSnoopingEnabled);
                assert.equal(true, data.response.advancedMediaSessionSnoopingEnabled);
                assert.equal('string', data.response.advancedMfpClientProtection);
                assert.equal(true, data.response.advancedMfpSignatureGenerationEnabled);
                assert.equal(6, data.response.advancedMfpVersion);
                assert.equal('string', data.response.advancedOverrideInterfaceAclName);
                assert.equal('string', data.response.advancedOverrideInterfaceIpv6AclName);
                assert.equal(false, data.response.advancedPassiveClientEnabled);
                assert.equal('string', data.response.advancedPeerToPeerBlocking);
                assert.equal('string', data.response.advancedPmipMobilityType);
                assert.equal('string', data.response.advancedPmipProfile);
                assert.equal('string', data.response.advancedPmipRealm);
                assert.equal(false, data.response.advancedScanDeferPriority0);
                assert.equal(false, data.response.advancedScanDeferPriority1);
                assert.equal(true, data.response.advancedScanDeferPriority2);
                assert.equal(true, data.response.advancedScanDeferPriority3);
                assert.equal(true, data.response.advancedScanDeferPriority4);
                assert.equal(false, data.response.advancedScanDeferPriority5);
                assert.equal(false, data.response.advancedScanDeferPriority6);
                assert.equal(true, data.response.advancedScanDeferPriority7);
                assert.equal(6, data.response.advancedScanDeferTime);
                assert.equal(9, data.response.advancedSessionTimeout);
                assert.equal('string', data.response.advancedWifiDirectClientsPolicy);
                assert.equal(false, data.response.broadcastSsidEnabled);
                assert.equal(1, data.response.ckipKeyIndex);
                assert.equal(false, data.response.ckipKeyPermutationEnabled);
                assert.equal('string', data.response.ckipKeySize);
                assert.equal(true, data.response.ckipMmhModeEnabled);
                assert.equal(true, data.response.ckipSecurityEnabled);
                assert.equal(3, data.response.controllerId);
                assert.equal('object', typeof data.response.hotspot2Enable);
                assert.equal(true, Array.isArray(data.response.hotspot2Operators));
                assert.equal(true, Array.isArray(data.response.hotspot2Ports));
                assert.equal('object', typeof data.response.hotspot2Wan);
                assert.equal(true, Array.isArray(data.response.hotspotCellularNetworks));
                assert.equal(true, Array.isArray(data.response.hotspotDomains));
                assert.equal('object', typeof data.response.hotspotGeneral);
                assert.equal(true, Array.isArray(data.response.hotspotOuiConfigs));
                assert.equal(true, Array.isArray(data.response.hotspotRealms));
                assert.equal('object', typeof data.response.hotspotServiceAdvertisement);
                assert.equal('string', data.response.interfaceName);
                assert.equal(3, data.response.interfaceType);
                assert.equal('string', data.response.ipAddress);
                assert.equal(true, data.response.isWiredLan);
                assert.equal('string', data.response.lanType);
                assert.equal(false, data.response.layer2FastTransitionEnabled);
                assert.equal(true, data.response.layer2FastTransitionOverDsEnabled);
                assert.equal(9, data.response.layer2FastTransitionReassociationTimeout);
                assert.equal(true, data.response.layer2MacFilteringEnabled);
                assert.equal(true, data.response.layer3GlobalWebAuthEnabled);
                assert.equal('string', data.response.layer3PreauthenticationAcl);
                assert.equal('string', data.response.layer3PreauthenticationIpv6Acl);
                assert.equal(false, data.response.layer3VpnPassthroughEnabled);
                assert.equal('string', data.response.layer3WebAuthFlexAcl);
                assert.equal('string', data.response.layer3WebAuthType);
                assert.equal(false, data.response.layer3WebPolicyAuthenticationEnabled);
                assert.equal(true, data.response.layer3WebPolicyConditionalRedirectEnabled);
                assert.equal(true, data.response.layer3WebPolicyOnMacFailureEnabled);
                assert.equal(false, data.response.layer3WebPolicyPassthroughEnabled);
                assert.equal(false, data.response.multicastVlanEnabled);
                assert.equal('string', data.response.multicastVlanInterface);
                assert.equal('string', data.response.profileName);
                assert.equal('string', data.response.qos7920Cac);
                assert.equal('string', data.response.qosAvcProfileName);
                assert.equal(false, data.response.qosNbarVisibilityEnabled);
                assert.equal('string', data.response.qosNetflowMonitor);
                assert.equal(10, data.response.qosPerSSidBurstRealTimeUpstreamRate);
                assert.equal(9, data.response.qosPerSsidAverageDownstreamRate);
                assert.equal(2, data.response.qosPerSsidAverageRealTimeDownstreamRate);
                assert.equal(10, data.response.qosPerSsidAverageRealTimeUpstreamRate);
                assert.equal(3, data.response.qosPerSsidAverageUpstreamRate);
                assert.equal(1, data.response.qosPerSsidBurstDownstreamRate);
                assert.equal(5, data.response.qosPerSsidBurstRealTimeDownstreamRate);
                assert.equal(8, data.response.qosPerSsidBurstUpstreamRate);
                assert.equal(8, data.response.qosPerUserAverageDownstreamRate);
                assert.equal(2, data.response.qosPerUserAverageRealTimeDownstreamRate);
                assert.equal(10, data.response.qosPerUserAverageRealTimeUpstreamRate);
                assert.equal(6, data.response.qosPerUserAverageUpstreamRate);
                assert.equal(9, data.response.qosPerUserBurstDownstreamRate);
                assert.equal(1, data.response.qosPerUserBurstRealTimeDownstreamRate);
                assert.equal(5, data.response.qosPerUserBurstRealTimeUpstreamRate);
                assert.equal(7, data.response.qosPerUserBurstUpstreamRate);
                assert.equal('string', data.response.qosProfile);
                assert.equal('string', data.response.qosWmmPolicy);
                assert.equal('string', data.response.radioPolicy);
                assert.equal('string', data.response.ssid);
                assert.equal('object', typeof data.response.vpnPassThroughGatewayAddress);
                assert.equal('string', data.response.webAuthExternalUrl);
                assert.equal('string', data.response.webAuthLoginFailurePage);
                assert.equal('string', data.response.webAuthLoginPage);
                assert.equal('string', data.response.webAuthLogoutPage);
                assert.equal(false, data.response.webPassthruEmailInputEnabled);
                assert.equal(true, data.response.wepAllowSharedKeyAuthentication);
                assert.equal(2, data.response.wepKeyIndex);
                assert.equal('string', data.response.wepKeySize);
                assert.equal(false, data.response.wepSecurityEnabled);
                assert.equal(10, data.response.wlanId);
                assert.equal(true, data.response.wpa2Enabled);
                assert.equal(false, data.response.wpa2EncryptionProtocolAes);
                assert.equal(false, data.response.wpa2EncryptionProtocolTkip);
                assert.equal(false, data.response.wpaAuthenticationKeyManagement8021x);
                assert.equal(false, data.response.wpaAuthenticationKeyManagementCckm);
                assert.equal(false, data.response.wpaAuthenticationKeyManagementFt8021x);
                assert.equal(true, data.response.wpaAuthenticationKeyManagementFtPsk);
                assert.equal(true, data.response.wpaAuthenticationKeyManagementPmf8021x);
                assert.equal(false, data.response.wpaAuthenticationKeyManagementPmfPsk);
                assert.equal(true, data.response.wpaAuthenticationKeyManagementPsk);
                assert.equal(false, data.response.wpaEnabled);
                assert.equal(false, data.response.wpaEncryptionProtocolAes);
                assert.equal(true, data.response.wpaEncryptionProtocolTkip);
                assert.equal('string', data.response.wpaPresharedKey);
                assert.equal('string', data.response.wpaPresharedKeyFormat);
                assert.equal(true, data.response.wpaSecurityEnabled);
                assert.equal('string', data.response.x8021KeySize);
                assert.equal(false, data.response.x8021SecurityEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataWlanProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4DataWlanTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4DataWlanTemplates((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.aaaAllowOverride);
                assert.equal('string', data.response.aaaLdapPrimaryServer);
                assert.equal('string', data.response.aaaLdapSecondaryServer);
                assert.equal('string', data.response.aaaLdapTertiaryServer);
                assert.equal(true, data.response.aaaLocalEapAuthenticationEnabled);
                assert.equal('string', data.response.aaaLocalEapAuthenticationProfileName);
                assert.equal('string', data.response.aaaRadiusAccountingPrimaryServer);
                assert.equal('string', data.response.aaaRadiusAccountingSecondaryServer);
                assert.equal(true, data.response.aaaRadiusAccountingServersEnabled);
                assert.equal('string', data.response.aaaRadiusAccountingTertiaryServer);
                assert.equal(true, data.response.aaaRadiusAccountingWlcSpecificEnabled);
                assert.equal('string', data.response.aaaRadiusAuthenticationPrimaryServer);
                assert.equal('string', data.response.aaaRadiusAuthenticationSecondaryServer);
                assert.equal(false, data.response.aaaRadiusAuthenticationServersEnabled);
                assert.equal('string', data.response.aaaRadiusAuthenticationTertiaryServer);
                assert.equal(true, data.response.aaaRadiusAuthenticationWlcSpecificEnabled);
                assert.equal(true, data.response.aaaRadiusInterimUpdateEnabled);
                assert.equal(2, data.response.aaaRadiusInterimUpdateInterval);
                assert.equal(true, data.response.adminStatus);
                assert.equal(false, data.response.advancedAironetIeEnabled);
                assert.equal(false, data.response.advancedClientExclusionEnabled);
                assert.equal(5, data.response.advancedClientExclusionTimeout);
                assert.equal(true, data.response.advancedCoverageHoleDetectionEnabled);
                assert.equal(false, data.response.advancedDhcpAddressAssignmentRequired);
                assert.equal(false, data.response.advancedDhcpProfilingEnabled);
                assert.equal('object', typeof data.response.advancedDhcpServerIpAddress);
                assert.equal(true, data.response.advancedDiagnosticChannelEnabled);
                assert.equal(2, data.response.advancedDot11anDtimPeriod);
                assert.equal(8, data.response.advancedDot11bgnDtimPeriod);
                assert.equal(false, data.response.advancedFlexConnectCentralDhcpProcessingEnabled);
                assert.equal(false, data.response.advancedFlexConnectLearnClientIpEnabled);
                assert.equal(true, data.response.advancedFlexConnectLocalAuthEnabled);
                assert.equal(false, data.response.advancedFlexConnectLocalSwitchingEnabled);
                assert.equal(false, data.response.advancedFlexConnectNatPatEnabled);
                assert.equal(true, data.response.advancedFlexConnectOverrideDnsEnabled);
                assert.equal(true, data.response.advancedFlexConnectReapCentralAssociation);
                assert.equal(true, data.response.advancedFlexConnectVlanCentralSwitchingEnabled);
                assert.equal(false, data.response.advancedHttpProfilingEnabled);
                assert.equal(true, data.response.advancedIpv6Enabled);
                assert.equal(true, data.response.advancedKtsCacEnabled);
                assert.equal(true, data.response.advancedLoadBalancingBandSelectEnabled);
                assert.equal(false, data.response.advancedLoadBalancingEnabled);
                assert.equal(1, data.response.advancedMaximumClients);
                assert.equal('string', data.response.advancedMdnsProfileName);
                assert.equal(true, data.response.advancedMdnsSnoopingEnabled);
                assert.equal(false, data.response.advancedMediaSessionSnoopingEnabled);
                assert.equal('string', data.response.advancedMfpClientProtection);
                assert.equal(false, data.response.advancedMfpSignatureGenerationEnabled);
                assert.equal(5, data.response.advancedMfpVersion);
                assert.equal('string', data.response.advancedOverrideInterfaceAclName);
                assert.equal('string', data.response.advancedOverrideInterfaceIpv6AclName);
                assert.equal(true, data.response.advancedPassiveClientEnabled);
                assert.equal('string', data.response.advancedPeerToPeerBlocking);
                assert.equal('string', data.response.advancedPmipMobilityType);
                assert.equal('string', data.response.advancedPmipProfile);
                assert.equal('string', data.response.advancedPmipRealm);
                assert.equal(false, data.response.advancedScanDeferPriority0);
                assert.equal(false, data.response.advancedScanDeferPriority1);
                assert.equal(false, data.response.advancedScanDeferPriority2);
                assert.equal(true, data.response.advancedScanDeferPriority3);
                assert.equal(false, data.response.advancedScanDeferPriority4);
                assert.equal(true, data.response.advancedScanDeferPriority5);
                assert.equal(true, data.response.advancedScanDeferPriority6);
                assert.equal(false, data.response.advancedScanDeferPriority7);
                assert.equal(1, data.response.advancedScanDeferTime);
                assert.equal(9, data.response.advancedSessionTimeout);
                assert.equal(false, data.response.advancedSessionTimeoutEnabled);
                assert.equal('string', data.response.advancedWifiDirectClientsPolicy);
                assert.equal(false, data.response.broadcastSsidEnabled);
                assert.equal(7, data.response.ckipKeyIndex);
                assert.equal(false, data.response.ckipKeyPermutationEnabled);
                assert.equal('string', data.response.ckipKeySize);
                assert.equal(false, data.response.ckipMmhModeEnabled);
                assert.equal(true, data.response.ckipSecurityEnabled);
                assert.equal('object', typeof data.response.hotspot2Enable);
                assert.equal(true, Array.isArray(data.response.hotspot2Operators));
                assert.equal(true, Array.isArray(data.response.hotspot2Ports));
                assert.equal('object', typeof data.response.hotspot2Wan);
                assert.equal(true, Array.isArray(data.response.hotspotCellularNetworks));
                assert.equal(true, Array.isArray(data.response.hotspotDomains));
                assert.equal('object', typeof data.response.hotspotGeneral);
                assert.equal(true, Array.isArray(data.response.hotspotOuiTemplate));
                assert.equal(true, Array.isArray(data.response.hotspotRealms));
                assert.equal('object', typeof data.response.hotspotServiceAdvertisement);
                assert.equal('string', data.response.interfaceName);
                assert.equal(9, data.response.interfaceType);
                assert.equal(true, data.response.isWiredLan);
                assert.equal('string', data.response.lanType);
                assert.equal(1, data.response.lastModified);
                assert.equal(false, data.response.layer2FastTransitionEnabled);
                assert.equal(false, data.response.layer2FastTransitionOverDsEnabled);
                assert.equal(2, data.response.layer2FastTransitionReassociationTimeout);
                assert.equal(true, data.response.layer2MacFilteringEnabled);
                assert.equal(true, data.response.layer3GlobalWebAuthEnabled);
                assert.equal('string', data.response.layer3PreauthenticationAcl);
                assert.equal('string', data.response.layer3PreauthenticationIpv6Acl);
                assert.equal(false, data.response.layer3VpnPassthroughEnabled);
                assert.equal('string', data.response.layer3WebAuthFlexAcl);
                assert.equal('string', data.response.layer3WebAuthType);
                assert.equal(true, data.response.layer3WebPolicyAuthenticationEnabled);
                assert.equal(true, data.response.layer3WebPolicyConditionalRedirectEnabled);
                assert.equal(true, data.response.layer3WebPolicyOnMacFailureEnabled);
                assert.equal(false, data.response.layer3WebPolicyPassthroughEnabled);
                assert.equal(false, data.response.multicastVlanEnabled);
                assert.equal('string', data.response.multicastVlanInterface);
                assert.equal('string', data.response.profileName);
                assert.equal('string', data.response.qos7920Cac);
                assert.equal('string', data.response.qosAvcProfileName);
                assert.equal(false, data.response.qosNbarVisibilityEnabled);
                assert.equal('string', data.response.qosNetflowMonitor);
                assert.equal(4, data.response.qosPerSSidBurstRealTimeUpstreamRate);
                assert.equal(2, data.response.qosPerSsidAverageDownstreamRate);
                assert.equal(6, data.response.qosPerSsidAverageRealTimeDownstreamRate);
                assert.equal(2, data.response.qosPerSsidAverageRealTimeUpstreamRate);
                assert.equal(9, data.response.qosPerSsidAverageUpstreamRate);
                assert.equal(8, data.response.qosPerSsidBurstDownstreamRate);
                assert.equal(9, data.response.qosPerSsidBurstRealTimeDownstreamRate);
                assert.equal(1, data.response.qosPerSsidBurstUpstreamRate);
                assert.equal(9, data.response.qosPerUserAverageDownstreamRate);
                assert.equal(8, data.response.qosPerUserAverageRealTimeDownstreamRate);
                assert.equal(8, data.response.qosPerUserAverageRealTimeUpstreamRate);
                assert.equal(9, data.response.qosPerUserAverageUpstreamRate);
                assert.equal(8, data.response.qosPerUserBurstDownstreamRate);
                assert.equal(2, data.response.qosPerUserBurstRealTimeDownstreamRate);
                assert.equal(1, data.response.qosPerUserBurstRealTimeUpstreamRate);
                assert.equal(10, data.response.qosPerUserBurstUpstreamRate);
                assert.equal('string', data.response.qosProfile);
                assert.equal('string', data.response.qosWmmPolicy);
                assert.equal('string', data.response.radioPolicy);
                assert.equal('string', data.response.ssid);
                assert.equal('string', data.response.templateName);
                assert.equal('object', typeof data.response.vpnPassThroughGatewayAddress);
                assert.equal('string', data.response.webAuthExternalUrl);
                assert.equal('string', data.response.webAuthLoginFailurePage);
                assert.equal('string', data.response.webAuthLoginPage);
                assert.equal('string', data.response.webAuthLogoutPage);
                assert.equal(false, data.response.webPassthruEmailInputEnabled);
                assert.equal(false, data.response.wepAllowSharedKeyAuthentication);
                assert.equal(7, data.response.wepKeyIndex);
                assert.equal('string', data.response.wepKeySize);
                assert.equal(true, data.response.wepSecurityEnabled);
                assert.equal(true, data.response.wpa2Enabled);
                assert.equal(false, data.response.wpa2EncryptionProtocolAes);
                assert.equal(true, data.response.wpa2EncryptionProtocolTkip);
                assert.equal(true, data.response.wpaAuthenticationKeyManagement8021x);
                assert.equal(false, data.response.wpaAuthenticationKeyManagementCckm);
                assert.equal(true, data.response.wpaAuthenticationKeyManagementFt8021x);
                assert.equal(true, data.response.wpaAuthenticationKeyManagementFtPsk);
                assert.equal(false, data.response.wpaAuthenticationKeyManagementPmf8021x);
                assert.equal(true, data.response.wpaAuthenticationKeyManagementPmfPsk);
                assert.equal(false, data.response.wpaAuthenticationKeyManagementPsk);
                assert.equal(false, data.response.wpaControllerSpecificPresharedKeyEnabled);
                assert.equal(false, data.response.wpaEnabled);
                assert.equal(true, data.response.wpaEncryptionProtocolAes);
                assert.equal(false, data.response.wpaEncryptionProtocolTkip);
                assert.equal('string', data.response.wpaPresharedKey);
                assert.equal('string', data.response.wpaPresharedKeyFormat);
                assert.equal(true, data.response.wpaSecurityEnabled);
                assert.equal('string', data.response.x8021KeySize);
                assert.equal(true, data.response.x8021SecurityEnabled);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataWlanTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpAaaTacacsPlusServerBodyParam = {
      authenticationType: {},
      localInterfaceIp: {},
      numberOfTries: {},
      port: {},
      retransmitTimeout: {},
      secretKey: 'string',
      secretKeyType: {},
      serverHostName: 'string',
      serverIp: {}
    };
    describe('#putWebacsApiV4OpAaaTacacsPlusServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpAaaTacacsPlusServer(webacsPutWebacsApiV4OpAaaTacacsPlusServerBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpAaaTacacsPlusServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpAaaTacacsPlusServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpAaaTacacsPlusServer((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.operationSucceeded);
                assert.equal('string', data.response.resultMessage);
                assert.equal(true, Array.isArray(data.response.servers));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpAaaTacacsPlusServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpAlarmsAcknowledge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpAlarmsAcknowledge((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpAlarmsAcknowledge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpAlarmsAnnotate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpAlarmsAnnotate(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpAlarmsAnnotate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpAlarmsClear - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpAlarmsClear((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpAlarmsClear', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpAlarmsClearByEventType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpAlarmsClearByEventType(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpAlarmsClearByEventType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpAlarmsUnacknowledge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpAlarmsUnacknowledge((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpAlarmsUnacknowledge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpApOnboardingProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpApOnboardingProfile((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpApOnboardingProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpApServiceAccessPointBodyParam = {
      accessPointId: {},
      adminStatus: false,
      location: 'string',
      name: 'string',
      primaryMwar: {},
      secondaryMwar: {},
      tertiaryMwar: {}
    };
    describe('#putWebacsApiV4OpApServiceAccessPoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpApServiceAccessPoint(webacsPutWebacsApiV4OpApServiceAccessPointBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpApServiceAccessPoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpApServiceDeleteApBodyParam = {
      apName: [
        'string'
      ]
    };
    describe('#putWebacsApiV4OpApServiceDeleteAp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpApServiceDeleteAp(webacsPutWebacsApiV4OpApServiceDeleteApBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpApServiceDeleteAp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpApServiceDeleteByIdBodyParam = {
      apID: [
        {}
      ]
    };
    describe('#putWebacsApiV4OpApServiceDeleteById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpApServiceDeleteById(webacsPutWebacsApiV4OpApServiceDeleteByIdBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpApServiceDeleteById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsApId = 555;
    describe('#getWebacsApiV4OpApServiceRxNeighbors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpApServiceRxNeighbors(webacsApId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.macAddress);
                assert.equal(2, data.response.neighborApId);
                assert.equal('string', data.response.neighborApName);
                assert.equal(7, data.response.neighborChannel);
                assert.equal('string', data.response.neighborChannelBandwidth);
                assert.equal('object', typeof data.response.neighborIpAddress);
                assert.equal('object', typeof data.response.neighborMacAddress);
                assert.equal('string', data.response.neighborMapLocation);
                assert.equal(2, data.response.neighborRSSI);
                assert.equal(1, data.response.neighborSlotId);
                assert.equal('string', data.response.radioBand);
                assert.equal(3, data.response.slotId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpApServiceRxNeighbors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpApiHealthCallsPerClient - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpApiHealthCallsPerClient(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.clientIp);
                assert.equal(4, data.response.requestCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpApiHealthCallsPerClient', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpApiHealthRequestCountTrend - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpApiHealthRequestCountTrend(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(3, data.response.responseCount);
                assert.equal(3, data.response.responseStatus);
                assert.equal(1, data.response.startOfDay);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpApiHealthRequestCountTrend', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpApiHealthResponseTimeTrend - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpApiHealthResponseTimeTrend(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.requestReceivedTime);
                assert.equal(8, data.response.responseTime);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpApiHealthResponseTimeTrend', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpApiHealthServiceNameList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpApiHealthServiceNameList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.path);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpApiHealthServiceNameList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpCliTemplateConfigurationDeployTemplateThroughJob - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpCliTemplateConfigurationDeployTemplateThroughJob(webacsStartTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpCliTemplateConfigurationDeployTemplateThroughJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpCliTemplateConfigurationDeviceTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpCliTemplateConfigurationDeviceTypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.deviceTypes));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpCliTemplateConfigurationDeviceTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webacsGroupId = 'fakedata';
    describe('#getWebacsApiV4OpCliTemplateConfigurationFolder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpCliTemplateConfigurationFolder((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.folderFQN);
                assert.equal(8, data.response.groupId);
              } else {
                runCommonAsserts(data, error);
              }
              webacsGroupId = data.response.groupId;
              saveMockData('Webacs', 'getWebacsApiV4OpCliTemplateConfigurationFolder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpCliTemplateConfigurationTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpCliTemplateConfigurationTemplate((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpCliTemplateConfigurationTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpComplianceCheck - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpComplianceCheck(webacsJobName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.devices));
                assert.equal(true, data.response.isSuccess);
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpComplianceCheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsDevice = 'fakedata';
    let webacsDeviceIp = 'fakedata';
    describe('#getWebacsApiV4OpConfigArchiveServiceConfigDiffDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpConfigArchiveServiceConfigDiffDevice(webacsDevice, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.configChanged);
                assert.equal(true, Array.isArray(data.response.configDiffs));
                assert.equal('string', data.response.deviceIp);
                assert.equal('string', data.response.deviceName);
              } else {
                runCommonAsserts(data, error);
              }
              webacsDeviceIp = data.response.deviceIp;
              saveMockData('Webacs', 'getWebacsApiV4OpConfigArchiveServiceConfigDiffDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsFileId = 555;
    describe('#getWebacsApiV4OpConfigArchiveServiceExtractSanitizedFile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpConfigArchiveServiceExtractSanitizedFile(webacsFileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.fileData);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpConfigArchiveServiceExtractSanitizedFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpConfigArchiveServiceExtractUnsanitizedFile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpConfigArchiveServiceExtractUnsanitizedFile(webacsFileId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.fileData);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpConfigArchiveServiceExtractUnsanitizedFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpConfigureApMaintenanceModeBodyParam = {
      apIds: [
        {}
      ],
      maintenanceMode: true
    };
    describe('#putWebacsApiV4OpConfigureApMaintenanceMode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpConfigureApMaintenanceMode(webacsPutWebacsApiV4OpConfigureApMaintenanceModeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpConfigureApMaintenanceMode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpConfigureApScheduleMaintenanceMode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpConfigureApScheduleMaintenanceMode(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpConfigureApScheduleMaintenanceMode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpConfigureDeviceMaintenanceModeBodyParam = {
      deviceIds: [
        {}
      ],
      maintenanceMode: true,
      reason: 'string'
    };
    describe('#putWebacsApiV4OpConfigureDeviceMaintenanceMode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpConfigureDeviceMaintenanceMode(webacsPutWebacsApiV4OpConfigureDeviceMaintenanceModeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpConfigureDeviceMaintenanceMode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpConfigureDeviceScheduleMaintenanceMode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpConfigureDeviceScheduleMaintenanceMode(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpConfigureDeviceScheduleMaintenanceMode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpCredentialProfilesManagementCredentialProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpCredentialProfilesManagementCredentialProfiles((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.cliParameters);
                assert.equal('string', data.response.description);
                assert.equal('object', typeof data.response.httpParameters);
                assert.equal('string', data.response.profileName);
                assert.equal('object', typeof data.response.snmpParameters);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpCredentialProfilesManagementCredentialProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpCredentialProfilesManagementDeviceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpCredentialProfilesManagementDeviceList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpCredentialProfilesManagementDeviceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpCredentialProfilesManagementDeviceList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpCredentialProfilesManagementDeviceList(webacsProfileName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.deviceIp);
                assert.equal('string', data.response.deviceName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpCredentialProfilesManagementDeviceList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpDeviceRestDeviceLispMapSummaryDeviceIpStartTimeEndTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestDeviceLispMapSummaryDeviceIpStartTimeEndTime(webacsDeviceIp, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpDeviceRestDeviceLispMapSummaryDeviceIpStartTimeEndTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsTopn = 555;
    const webacsSource = 'fakedata';
    const webacsDashlet = 555;
    describe('#getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn(webacsGroupId, webacsStartTime, webacsEndTime, webacsTopn, webacsSource, webacsDashlet, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpDeviceRestTopNDeviceCacheALLGroupIdStartTimeEndTimeTopn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsMaxrecords = 555;
    describe('#getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords(webacsStartTime, webacsEndTime, webacsGroupId, webacsMaxrecords, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpDeviceRestUciDeviceReachabilityGroupIdStartTimeEndTimeMaxrecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords(webacsStartTime, webacsEndTime, webacsMaxrecords, webacsGroupId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpDeviceRestUnReachableUciDeviceGroupIdStartTimeEndTimeMaxrecords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpDevicesBulkImportBodyParam = {
      devices: [
        {}
      ]
    };
    describe('#putWebacsApiV4OpDevicesBulkImport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpDevicesBulkImport(webacsPutWebacsApiV4OpDevicesBulkImportBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpDevicesBulkImport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpDevicesExportDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpDevicesExportDevices(webacsIpAddress, webacsGroupId, webacsGroupPath, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.devices));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpDevicesExportDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGroupsAlarmSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpGroupsAlarmSummary((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.nbrAlarmedAPs);
                assert.equal(5, data.response.nbrAlarmedClients);
                assert.equal(2, data.response.nbrAlarmedDevices);
                assert.equal(9, data.response.nbrAlarmedSites);
                assert.equal(1, data.response.totalAlarms);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpGroupsAlarmSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGroupsDeviceGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpGroupsDeviceGroups(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.clearedAlarms);
                assert.equal(4, data.response.criticalAlarms);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.groupId);
                assert.equal('string', data.response.groupName);
                assert.equal('string', data.response.groupType);
                assert.equal(9, data.response.informationAlarms);
                assert.equal(true, data.response.isExplicit);
                assert.equal(1, data.response.majorAlarms);
                assert.equal(1, data.response.membersCount);
                assert.equal(10, data.response.minorAlarms);
                assert.equal('string', data.response.name);
                assert.equal(6, data.response.unacknowledgedClearedAlarms);
                assert.equal(10, data.response.unacknowledgedCriticalAlarms);
                assert.equal(2, data.response.unacknowledgedInformationAlarms);
                assert.equal(9, data.response.unacknowledgedMajorAlarms);
                assert.equal(8, data.response.unacknowledgedMinorAlarms);
                assert.equal(10, data.response.unacknowledgedWarningAlarms);
                assert.equal(7, data.response.warningAlarms);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpGroupsDeviceGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpGroupsGroupDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpGroupsGroupDevices(webacsGroupId, webacsGroupPath, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpGroupsGroupDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpGroupsGroupInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpGroupsGroupInterfaces(webacsGroupId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpGroupsGroupInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGroupsGroupRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpGroupsGroupRules(webacsGroupId, webacsGroupPath, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.rules));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpGroupsGroupRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpGroupsRemoveDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpGroupsRemoveDevices(webacsGroupId, webacsGroupPath, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpGroupsRemoveDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpGroupsRemoveInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpGroupsRemoveInterfaces(webacsGroupId, webacsGroupPath, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpGroupsRemoveInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGroupsSites - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpGroupsSites(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.apCount);
                assert.equal(8, data.response.clearedAlarms);
                assert.equal(9, data.response.clientCount);
                assert.equal(4, data.response.criticalAlarms);
                assert.equal('string', data.response.description);
                assert.equal(5, data.response.deviceCount);
                assert.equal(2, data.response.groupId);
                assert.equal('string', data.response.groupName);
                assert.equal(4, data.response.informationAlarms);
                assert.equal(true, data.response.isExplicit);
                assert.equal(4, data.response.latitude);
                assert.equal('string', data.response.locationAddress);
                assert.equal('string', data.response.locationGroupType);
                assert.equal(10, data.response.longitude);
                assert.equal(4, data.response.majorAlarms);
                assert.equal(3, data.response.membersCount);
                assert.equal(9, data.response.minorAlarms);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.siteType);
                assert.equal(8, data.response.unacknowledgedClearedAlarms);
                assert.equal(2, data.response.unacknowledgedCriticalAlarms);
                assert.equal(2, data.response.unacknowledgedInformationAlarms);
                assert.equal(5, data.response.unacknowledgedMajorAlarms);
                assert.equal(4, data.response.unacknowledgedMinorAlarms);
                assert.equal(5, data.response.unacknowledgedWarningAlarms);
                assert.equal(1, data.response.warningAlarms);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpGroupsSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGroupsUserDefinedGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpGroupsUserDefinedGroups(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.clearedAlarms);
                assert.equal(8, data.response.criticalAlarms);
                assert.equal('string', data.response.description);
                assert.equal(6, data.response.groupId);
                assert.equal('string', data.response.groupName);
                assert.equal('string', data.response.groupType);
                assert.equal(3, data.response.informationAlarms);
                assert.equal(true, data.response.isExplicit);
                assert.equal(2, data.response.majorAlarms);
                assert.equal(10, data.response.membersCount);
                assert.equal(6, data.response.minorAlarms);
                assert.equal('string', data.response.name);
                assert.equal(2, data.response.unacknowledgedClearedAlarms);
                assert.equal(8, data.response.unacknowledgedCriticalAlarms);
                assert.equal(4, data.response.unacknowledgedInformationAlarms);
                assert.equal(6, data.response.unacknowledgedMajorAlarms);
                assert.equal(5, data.response.unacknowledgedMinorAlarms);
                assert.equal(5, data.response.unacknowledgedWarningAlarms);
                assert.equal(2, data.response.warningAlarms);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpGroupsUserDefinedGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpGuestUserDeleteBodyParam = {
      applyGuestUserTo: {},
      configGroup: 'string',
      controllerId: [
        {}
      ],
      description: 'string',
      disclaimer: 'string',
      endTime: {},
      locationGroupId: {},
      password: 'string',
      profile: 'string',
      rebootController: false,
      saveConfigToFlash: true,
      userRole: 'string',
      username: 'string'
    };
    describe('#putWebacsApiV4OpGuestUserDelete - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpGuestUserDelete(webacsPutWebacsApiV4OpGuestUserDeleteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpGuestUserDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGuestUserGuestUsersOnController - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpGuestUserGuestUsersOnController(webacsControllerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpGuestUserGuestUsersOnController', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpGuestUserLoggedInGuestUsersOnController - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpGuestUserLoggedInGuestUsersOnController(webacsControllerId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpGuestUserLoggedInGuestUsersOnController', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpImageList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpImageList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpImageList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpImageListFamily - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpImageListFamily(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpImageListFamily', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpImageListId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpImageListId(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpImageListId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpImageListPhysical - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpImageListPhysical((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpImageListPhysical', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpImageListVirtual - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpImageListVirtual((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.items));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpImageListVirtual', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpInfoCoredumps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpInfoCoredumps((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.coreDumpCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpInfoCoredumps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpInfoDisk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpInfoDisk((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.availableCapacity);
                assert.equal('string', data.response.name);
                assert.equal(8, data.response.usedCapacity);
                assert.equal(9, data.response.usedPercentage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpInfoDisk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpInfoLicense - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpInfoLicense((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.daysToExpire);
                assert.equal(3, data.response.deviceCountRemaining);
                assert.equal(3, data.response.deviceCountUsed);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpInfoLicense', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpInfoUptime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpInfoUptime((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.applicationUptime);
                assert.equal('string', data.response.defaultGateway);
                assert.equal('string', data.response.domainName);
                assert.equal(true, data.response.highAvailabilityEnabled);
                assert.equal('string', data.response.highAvailabilityPrimaryServerHostname);
                assert.equal('string', data.response.highAvailabilityPrimaryServerIp);
                assert.equal('object', typeof data.response.highAvailabilityRole);
                assert.equal('string', data.response.highAvailabilitySecondaryServerHostname);
                assert.equal('string', data.response.highAvailabilitySecondaryServerIp);
                assert.equal('string', data.response.highAvailabilityVirtualHostname);
                assert.equal('string', data.response.highAvailabilityVirtualIp);
                assert.equal('string', data.response.hostName);
                assert.equal(7, data.response.serverTime);
                assert.equal('string', data.response.serverUptime);
                assert.equal(4, data.response.totalMemory);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpInfoUptime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpInfoVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpInfoVersion((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.result);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpInfoVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpJobServiceCancelBodyParam = {
      jobId: [
        {}
      ]
    };
    describe('#putWebacsApiV4OpJobServiceCancel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpJobServiceCancel(webacsPutWebacsApiV4OpJobServiceCancelBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpJobServiceCancel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpJobServiceCancelrunningBodyParam = {
      runId: [
        {}
      ]
    };
    describe('#putWebacsApiV4OpJobServiceCancelrunning - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpJobServiceCancelrunning(webacsPutWebacsApiV4OpJobServiceCancelrunningBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpJobServiceCancelrunning', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpJobServiceResumeBodyParam = {
      jobId: [
        {}
      ]
    };
    describe('#putWebacsApiV4OpJobServiceResume - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpJobServiceResume(webacsPutWebacsApiV4OpJobServiceResumeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpJobServiceResume', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpJobServiceRunhistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpJobServiceRunhistory(null, null, webacsJobName, webacsStartTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal(10, data.response.jobId);
                assert.equal('string', data.response.jobName);
                assert.equal('object', typeof data.response.jobStatus);
                assert.equal('string', data.response.jobType);
                assert.equal(10, data.response.nextRunTime);
                assert.equal(true, Array.isArray(data.response.runInstances));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpJobServiceRunhistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpJobServiceSuspendBodyParam = {
      jobId: [
        {}
      ]
    };
    describe('#putWebacsApiV4OpJobServiceSuspend - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpJobServiceSuspend(webacsPutWebacsApiV4OpJobServiceSuspendBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpJobServiceSuspend', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpMacFilterDeploy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpMacFilterDeploy((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpMacFilterDeploy', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpNfvDeviceDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpNfvDeviceDetails(webacsDeviceIp, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.cimcIp);
                assert.equal('string', data.response.compileTime);
                assert.equal('string', data.response.cpu);
                assert.equal('string', data.response.diskSize);
                assert.equal('string', data.response.kernelVersion);
                assert.equal('string', data.response.libVirVersion);
                assert.equal('string', data.response.manufacturer);
                assert.equal('string', data.response.memory);
                assert.equal('string', data.response.ovsVersion);
                assert.equal('string', data.response.pid);
                assert.equal(true, Array.isArray(data.response.portDetails));
                assert.equal('string', data.response.qemuVersion);
                assert.equal('string', data.response.sn);
                assert.equal(4, data.response.switchPorts);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpNfvDeviceDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpNfvGetBridges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpNfvGetBridges(webacsDeviceIp, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.port));
                assert.equal(false, data.response.stp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpNfvGetBridges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpNfvGetDeployments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpNfvGetDeployments(webacsDeviceIp, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.networks));
                assert.equal('string', data.response.version);
                assert.equal(true, Array.isArray(data.response.vmgroup));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpNfvGetDeployments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpNfvGetOvsBridges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpNfvGetOvsBridges(webacsDeviceIp, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.adminstate);
                assert.equal('string', data.response.akeyname);
                assert.equal('string', data.response.bridge);
                assert.equal(true, data.response.mirror);
                assert.equal('string', data.response.mirrorsrc);
                assert.equal('string', data.response.providernetworktype);
                assert.equal('string', data.response.providerphysicalnetwork);
                assert.equal(3, data.response.providersegmentationid);
                assert.equal(true, data.response.routerexternal);
                assert.equal(false, data.response.shared);
                assert.equal('string', data.response.sriov);
                assert.equal(true, Array.isArray(data.response.subnet));
                assert.equal(true, data.response.trunk);
                assert.equal(true, Array.isArray(data.response.vlan));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpNfvGetOvsBridges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpNfvGetServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpNfvGetServices(webacsDeviceIp, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.images));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpNfvGetServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpNfvUpdateBridgeBodyParam = {
      bridges: [
        {}
      ],
      server: {},
      transactionId: 'string'
    };
    describe('#putWebacsApiV4OpNfvUpdateBridge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpNfvUpdateBridge(webacsPutWebacsApiV4OpNfvUpdateBridgeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpNfvUpdateBridge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpNfvUpdateOvsBridgeBodyParam = {
      networks: [
        {}
      ],
      server: {},
      transactionId: 'string'
    };
    describe('#putWebacsApiV4OpNfvUpdateOvsBridge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpNfvUpdateOvsBridge(webacsPutWebacsApiV4OpNfvUpdateOvsBridgeBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpNfvUpdateOvsBridge', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsSiteName = 'fakedata';
    describe('#getWebacsApiV4OpPfrMonitoringExtGetAllBorderRoutersStartTimeEndTimeSiteName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllBorderRoutersStartTimeEndTimeSiteName(webacsStartTime, webacsEndTime, webacsSiteName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetAllBorderRoutersStartTimeEndTimeSiteName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsSrcSite = 'fakedata';
    const webacsDstSite = 'fakedata';
    describe('#getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime(webacsStartTime, webacsEndTime, webacsSrcSite, webacsDstSite, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetAllIMEsSrcSiteDstSiteStartTimeEndTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime(webacsStartTime, webacsEndTime, webacsSrcSite, webacsDstSite, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetAllRCsSrcSiteDstSiteStartTimeEndTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime(webacsStartTime, webacsEndTime, webacsSrcSite, webacsDstSite, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetAllTCAsSrcSiteDstSiteStartTimeEndTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsBrIp = 'fakedata';
    describe('#getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp(webacsStartTime, webacsEndTime, webacsSiteName, webacsBrIp, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetAllWANInterfacesStartTimeEndTimeSiteNameBrIp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsDscp = 555;
    const webacsNoOfPoints = 555;
    describe('#getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints(webacsStartTime, webacsEndTime, webacsSiteName, webacsBrIp, webacsDscp, webacsNoOfPoints, webacsInterfaceName, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetDSCPTimeVsUsageChartStartTimeEndTimeSiteNameBrIpDscpNoOfPoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageStartTimeEndTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageStartTimeEndTime(webacsStartTime, webacsEndTime, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageStartTimeEndTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsSrcSiteId = 555;
    const webacsDstSiteId = 555;
    describe('#getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId(webacsStartTime, webacsEndTime, webacsSrcSiteId, webacsDstSiteId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetDscpAppUsageForLiveTopoStartTimeEndTimeSrcSiteIdDstSiteId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetOutStandingIMEsForSiteStartTimeEndTimeSrcSite - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetOutStandingIMEsForSiteStartTimeEndTimeSrcSite(webacsStartTime, webacsEndTime, webacsSrcSite, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetOutStandingIMEsForSiteStartTimeEndTimeSrcSite', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetSiteAppHealthStartTimeEndTime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetSiteAppHealthStartTimeEndTime(webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetSiteAppHealthStartTimeEndTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsRownum = 555;
    describe('#getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum(webacsStartTime, webacsEndTime, webacsDeviceIp, webacsRownum, webacsInterfaceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeStartTimeEndTimeDeviceIpRownum', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum(webacsStartTime, webacsEndTime, webacsDeviceIp, webacsRownum, webacsInterfaceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetTopNApplicationUsageOverTimeOldStartTimeEndTimeDeviceIpRownum', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsBrIpAddr = 'fakedata';
    const webacsDirection = 'fakedata';
    describe('#getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection(webacsStartTime, webacsEndTime, webacsBrIpAddr, webacsDirection, webacsInterfaceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeForLinkStartTimeEndTimeBrIpAddrDirection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection(webacsStartTime, webacsEndTime, webacsBrIpAddr, webacsDirection, webacsInterfaceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPfrMonitoringExtGetWanUtilizationOverTimeRateForLinkStartTimeEndTimeBrIpAddrDirection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpPnpProfilePnpProfileFolderProfileName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPnpProfilePnpProfileFolderProfileName(webacsPnpProfileFolder, webacsProfileName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.apicEmProfileOptions);
                assert.equal('string', data.response.author);
                assert.equal('string', data.response.bootStrapTemplateName);
                assert.equal('string', data.response.configurationTemplateName);
                assert.equal('string', data.response.credentialProfileName);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.deviceType);
                assert.equal('string', data.response.imageLocation);
                assert.equal('string', data.response.imageName);
                assert.equal('object', typeof data.response.nexusProfileDetails);
                assert.equal('string', data.response.postPnPTemplateName);
                assert.equal('string', data.response.profileName);
                assert.equal('string', data.response.profileType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPnpProfilePnpProfileFolderProfileName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsInstanceName = 'fakedata';
    describe('#getWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName(webacsProfileName, webacsInstanceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.profileInstances));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpRateServiceRateLimits - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpRateServiceRateLimits((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpRateServiceRateLimits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpRateServiceRateLimits - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpRateServiceRateLimits((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(8, data.response.limitPageSize);
                assert.equal(8, data.response.limitUnpagedQuery);
                assert.equal(1, data.response.maxAllUserConcurrentQueries);
                assert.equal(1, data.response.maxConcurrentQueries);
                assert.equal(9, data.response.perUserThreshold);
                assert.equal(3, data.response.totalRequestThreshold);
                assert.equal(3, data.response.windowSegments);
                assert.equal(4, data.response.windowSize);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpRateServiceRateLimits', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpReportServiceGetReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpReportServiceGetReport('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childReports));
                assert.equal(true, Array.isArray(data.response.dataRows));
                assert.equal('string', data.response.descriptorName);
                assert.equal(7, data.response.pageCount);
                assert.equal(9, data.response.pageIndex);
                assert.equal('string', data.response.reportDate);
                assert.equal('string', data.response.reportName);
                assert.equal('string', data.response.reportTitle);
                assert.equal('string', data.response.reportUrl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpReportServiceGetReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let webacsReportTitle = 'fakedata';
    describe('#getWebacsApiV4OpReportServiceTemplates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpReportServiceTemplates(null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.reportTitle);
                assert.equal('string', data.response.reportType);
                assert.equal('string', data.response.scheduled);
                assert.equal('string', data.response.virtualDomain);
              } else {
                runCommonAsserts(data, error);
              }
              webacsReportTitle = data.response.reportTitle;
              saveMockData('Webacs', 'getWebacsApiV4OpReportServiceTemplates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpReportServiceZipReport - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpReportServiceZipReport((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.reportTitle);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpReportServiceZipReport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpRogueApClassification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpRogueApClassification((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpRogueApClassification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsAlarmsAlarmDeletionPeriods - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpSettingsAlarmsAlarmDeletionPeriods((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.activeAndCleared);
                assert.equal(10, data.response.allEvents);
                assert.equal(5, data.response.clearedNonSecurityAlarms);
                assert.equal(3, data.response.clearedSecurityAlarms);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpSettingsAlarmsAlarmDeletionPeriods', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsAlarmsSeverityAutoClearConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpSettingsAlarmsSeverityAutoClearConfig((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.groups));
                assert.equal(true, Array.isArray(data.response.items));
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpSettingsAlarmsSeverityAutoClearConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsDataDataRetentionPeriods - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpSettingsDataDataRetentionPeriods((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(10, data.response.auditRetainDaily);
                assert.equal(6, data.response.deviceHealthRetainDaily);
                assert.equal(4, data.response.deviceHealthRetainHourly);
                assert.equal(7, data.response.deviceHealthRetainWeekly);
                assert.equal(5, data.response.performanceRetainLong);
                assert.equal(4, data.response.performanceRetainMedium);
                assert.equal(7, data.response.performanceRetainShort);
                assert.equal(3, data.response.systemHealthRetainDaily);
                assert.equal(2, data.response.systemHealthRetainHourly);
                assert.equal(2, data.response.systemHealthRetainWeekly);
                assert.equal(10, data.response.trendRetainDaily);
                assert.equal(9, data.response.trendRetainHourly);
                assert.equal(7, data.response.trendRetainWeekly);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpSettingsDataDataRetentionPeriods', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsDataPruneDataConfigTableName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpSettingsDataPruneDataConfigTableName('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(6, data.response.age);
                assert.equal('string', data.response.ageAttribute);
                assert.equal(10, data.response.maxRecords);
                assert.equal('string', data.response.tableName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpSettingsDataPruneDataConfigTableName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsDataPruneDataConfigsByGroupCategory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpSettingsDataPruneDataConfigsByGroupCategory(webacsCategory, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.pruneDataConfigs));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpSettingsDataPruneDataConfigsByGroupCategory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsInventoryConfigArchiveBasic - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpSettingsInventoryConfigArchiveBasic((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.inventoryIntegration);
                assert.equal(false, data.response.inventoryTriggered);
                assert.equal(false, data.response.maskAuthInfo);
                assert.equal(5, data.response.noOfParallelThreads);
                assert.equal(2, data.response.numberOfDays);
                assert.equal(4, data.response.numberOfVersions);
                assert.equal(true, data.response.outofboxArchiveIntegration);
                assert.equal(4, data.response.syslogHoldOffTimer);
                assert.equal(1, data.response.timeout);
                assert.equal(8, data.response.viewUpdateTimeout);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpSettingsInventoryConfigArchiveBasic', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsLoggingGeneralLoggingOptions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpSettingsLoggingGeneralLoggingOptions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.logFileSettings);
                assert.equal('string', data.response.logMessageLevel);
                assert.equal(true, Array.isArray(data.response.logModules));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpSettingsLoggingGeneralLoggingOptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsLoggingLogFileNames - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpSettingsLoggingLogFileNames((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.category);
                assert.equal('string', data.response.fileNames);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpSettingsLoggingLogFileNames', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsLoggingSnmpSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpSettingsLoggingSnmpSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.enableTrace);
                assert.equal('object', typeof data.response.logFileSettings);
                assert.equal(true, data.response.traceAllIps);
                assert.equal(true, data.response.traceDisplayValues);
                assert.equal('string', data.response.tracedIPs);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpSettingsLoggingSnmpSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpSettingsLoggingSysLogSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpSettingsLoggingSysLogSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpSettingsLoggingSysLogSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsLoggingSysLogSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpSettingsLoggingSysLogSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.enabled);
                assert.equal('string', data.response.facility);
                assert.equal('string', data.response.hostName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpSettingsLoggingSysLogSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsNotificationAlarmCategories - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpSettingsNotificationAlarmCategories((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.alarmCategoriesSettings));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpSettingsNotificationAlarmCategories', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsNotificationMailServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpSettingsNotificationMailServer((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.applyToAlarms);
                assert.equal('string', data.response.fromAddress);
                assert.equal('string', data.response.globalSubject);
                assert.equal('string', data.response.primaryMailServerAddress);
                assert.equal('string', data.response.primaryMailServerPassword);
                assert.equal(3, data.response.primaryMailServerPort);
                assert.equal('string', data.response.primaryMailServerUserName);
                assert.equal('string', data.response.secondaryMailServerAddress);
                assert.equal('string', data.response.secondaryMailServerPassword);
                assert.equal(6, data.response.secondaryMailServerPort);
                assert.equal('string', data.response.secondaryMailServerUserName);
                assert.equal('string', data.response.toAddress);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpSettingsNotificationMailServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpSettingsServersFtpServers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpSettingsServersFtpServers((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.serverType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpSettingsServersFtpServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsRange = '555';
    const webacsSiteId = 555;
    describe('#getWebacsApiV4OpStatisticsServiceApplicationAppVolume - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationAppVolume(webacsRange, webacsStartTime, webacsEndTime, null, webacsSiteId, null, webacsDeviceIp, null, webacsTopN, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationAppVolume', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationApplicationArtAnalysis - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationApplicationArtAnalysis(webacsRange, webacsStartTime, webacsEndTime, null, null, webacsSiteId, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationApplicationArtAnalysis', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationNumberOfUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationNumberOfUsers(webacsStartTime, webacsEndTime, webacsRange, null, webacsSiteId, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationNumberOfUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationPerformance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationPerformance(webacsStartTime, webacsEndTime, webacsRange, null, webacsSiteId, null, null, webacsTopN, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationPerformance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites(webacsStartTime, webacsEndTime, webacsRange, null, null, webacsSiteId, webacsTopN, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationTopNApplicationsWithSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationTopNHosts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTopNHosts(webacsStartTime, webacsEndTime, webacsRange, null, webacsSiteId, null, null, webacsTopN, null, webacsIpAddress, webacsInterfaceName, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationTopNHosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationTopNTraffics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTopNTraffics(webacsRange, webacsStartTime, webacsEndTime, null, webacsSiteId, null, webacsDeviceIp, null, null, null, null, webacsTopN, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationTopNTraffics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis(webacsStartTime, webacsEndTime, webacsRange, webacsType, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationTrafficAnalysis', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts(webacsStartTime, webacsEndTime, webacsRange, null, webacsSiteId, null, null, null, null, webacsTopN, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationWorstNHosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationWorstNSites - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationWorstNSites(webacsStartTime, webacsEndTime, webacsRange, null, webacsSiteId, null, null, null, webacsTopN, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationWorstNSites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationsAppCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationsAppCount(webacsStartTime, webacsEndTime, webacsRange, webacsSiteId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationsAppCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationsAppHealthCrossing - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationsAppHealthCrossing(webacsStartTime, webacsEndTime, webacsRange, webacsSiteId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationsAppHealthCrossing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsIsFabric = true;
    describe('#getWebacsApiV4OpStatisticsServiceApplicationsOvertime - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationsOvertime(webacsStartTime, webacsEndTime, webacsRange, null, null, webacsInterfaceName, webacsDeviceIp, null, null, webacsRownum, null, webacsSiteId, null, webacsType, null, null, null, null, webacsIsFabric, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationsOvertime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceApplicationsPerformance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceApplicationsPerformance(null, webacsStartTime, webacsEndTime, webacsRange, null, webacsSiteId, null, webacsDscp, null, null, webacsDeviceIp, null, null, null, null, null, webacsTopN, null, webacsType, null, null, null, null, webacsControllerId, webacsApId, null, null, webacsIsFabric, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceApplicationsPerformance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceClientsDistributions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceClientsDistributions((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.info);
                assert.equal(true, Array.isArray(data.response.metrics));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceClientsDistributions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsDistribution = 'fakedata';
    const webacsTimeInterval = 555;
    describe('#getWebacsApiV4OpStatisticsServiceClientsDistributionsDistribution - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceClientsDistributionsDistribution(webacsDistribution, webacsDevice, null, null, null, null, null, null, webacsDirection, webacsTimeInterval, webacsStartTime, webacsEndTime, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.currentDateTime);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.metricName);
                assert.equal(true, Array.isArray(data.response.metricRows));
                assert.equal('string', data.response.resourceName);
                assert.equal('object', typeof data.response.xValueProperty);
                assert.equal(true, Array.isArray(data.response.yValueProperty));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceClientsDistributionsDistribution', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceClusters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceClusters(null, null, null, webacsRange, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.resources));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceClusters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceClustersMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceClustersMetrics(null, null, webacsTimeInterval, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.info);
                assert.equal(true, Array.isArray(data.response.metrics));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceClustersMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsMetric = 'fakedata';
    describe('#getWebacsApiV4OpStatisticsServiceClustersMetricsMetric - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceClustersMetricsMetric(webacsMetric, null, null, webacsTimeInterval, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.currentDateTime);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.metricName);
                assert.equal(true, Array.isArray(data.response.metricRows));
                assert.equal('string', data.response.resourceName);
                assert.equal('object', typeof data.response.xValueProperty);
                assert.equal(true, Array.isArray(data.response.yValueProperty));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceClustersMetricsMetric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDatacenters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDatacenters(null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.resources));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDatacenters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDatacentersMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDatacentersMetrics(null, webacsTimeInterval, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.info);
                assert.equal(true, Array.isArray(data.response.metrics));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDatacentersMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric(webacsMetric, null, webacsTimeInterval, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.currentDateTime);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.metricName);
                assert.equal(true, Array.isArray(data.response.metricRows));
                assert.equal('string', data.response.resourceName);
                assert.equal('object', typeof data.response.xValueProperty);
                assert.equal(true, Array.isArray(data.response.yValueProperty));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDatacentersMetricsMetric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceAvailability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceAvailability(webacsIpAddress, webacsRange, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDeviceAvailability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceAvailabilityMessage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceAvailabilityMessage(webacsIpAddress, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDeviceAvailabilityMessage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceAvailabilitySummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceAvailabilitySummary(webacsRange, webacsStartTime, webacsEndTime, webacsSource, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDeviceAvailabilitySummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceCpuUtilSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceCpuUtilSummary(webacsRange, webacsStartTime, webacsEndTime, webacsType, webacsSource, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDeviceCpuUtilSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend(webacsIpAddress, webacsRange, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDeviceCpuUtilTrend', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceDownMessage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceDownMessage(webacsIpAddress, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDeviceDownMessage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceHealthInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceHealthInfo(webacsRange, webacsStartTime, webacsEndTime, webacsSource, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDeviceHealthInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend(webacsIpAddress, webacsRange, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDeviceMemoryUtilTrend', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDevicePortSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDevicePortSummary(webacsIpAddress, webacsType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDevicePortSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceReachabilityStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceReachabilityStatus(webacsTopN, webacsSource, webacsType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDeviceReachabilityStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceTopNCPU - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNCPU(webacsRange, webacsStartTime, webacsEndTime, webacsTopN, webacsSource, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDeviceTopNCPU', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceTopNMemory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNMemory(webacsRange, webacsStartTime, webacsEndTime, webacsTopN, webacsSource, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDeviceTopNMemory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDeviceTopNTemp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDeviceTopNTemp(webacsRange, webacsStartTime, webacsEndTime, webacsTopN, webacsSource, webacsType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDeviceTopNTemp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDevices(webacsDevice, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.resources));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceDevicesMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDevicesMetrics(webacsDevice, webacsTimeInterval, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.info);
                assert.equal(true, Array.isArray(data.response.metrics));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDevicesMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsLineCard = 'fakedata';
    describe('#getWebacsApiV4OpStatisticsServiceDevicesMetricsMetric - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceDevicesMetricsMetric(webacsMetric, webacsDevice, webacsLineCard, webacsTimeInterval, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.currentDateTime);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.metricName);
                assert.equal(true, Array.isArray(data.response.metricRows));
                assert.equal('string', data.response.resourceName);
                assert.equal('object', typeof data.response.xValueProperty);
                assert.equal(true, Array.isArray(data.response.yValueProperty));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceDevicesMetricsMetric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceHosts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceHosts(null, null, null, null, webacsRange, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.resources));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceHosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceHostsMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceHostsMetrics(null, null, null, webacsTimeInterval, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.info);
                assert.equal(true, Array.isArray(data.response.metrics));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceHostsMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceHostsMetricsMetric - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceHostsMetricsMetric(webacsMetric, null, null, null, webacsTimeInterval, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.currentDateTime);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.metricName);
                assert.equal(true, Array.isArray(data.response.metricRows));
                assert.equal('string', data.response.resourceName);
                assert.equal('object', typeof data.response.xValueProperty);
                assert.equal(true, Array.isArray(data.response.yValueProperty));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceHostsMetricsMetric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsIfName = 'fakedata';
    describe('#getWebacsApiV4OpStatisticsServiceInterfaceAvailMessage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceAvailMessage(webacsIpAddress, webacsIfName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceAvailMessage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceAvailability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceAvailability(webacsStartTime, webacsEndTime, webacsRange, webacsIpAddress, webacsIfName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceAvailability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceAvailabilitySummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceAvailabilitySummary(webacsStartTime, webacsEndTime, webacsRange, null, webacsSource, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceAvailabilitySummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceDetails(webacsIpAddress, webacsIfName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceDiscards - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceDiscards(webacsStartTime, webacsEndTime, webacsRange, webacsIpAddress, webacsIfName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceDiscards', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceDownMessage - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceDownMessage(webacsIpAddress, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceDownMessage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceErrors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceErrors(webacsStartTime, webacsEndTime, webacsRange, webacsIpAddress, webacsIfName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceErrors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceStatusSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceStatusSummary(webacsStartTime, webacsEndTime, webacsRange, null, webacsSource, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceStatusSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsIfIndex = 'fakedata';
    describe('#getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS(webacsStartTime, webacsEndTime, webacsRange, webacsTopN, webacsSource, webacsIfIndex, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceTopNClassMapQOS', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors(webacsStartTime, webacsEndTime, webacsRange, webacsTopN, webacsSource, webacsType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceTopNErrors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil(webacsStartTime, webacsEndTime, webacsRange, webacsTopN, webacsSource, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceTopNUtil', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues(webacsStartTime, webacsEndTime, webacsRange, webacsTopN, webacsSource, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceTopNWanIssues', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil(webacsStartTime, webacsEndTime, webacsRange, webacsTopN, webacsSource, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceTopNWanUtil', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceUtil - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceUtil(webacsStartTime, webacsEndTime, webacsRange, webacsIpAddress, webacsIfName, webacsType, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceUtil', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaceUtilSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaceUtilSummary(webacsStartTime, webacsEndTime, webacsRange, null, webacsSource, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaceUtilSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfaces(webacsDevice, webacsIfName, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.resources));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfacesMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfacesMetrics(webacsDevice, webacsIfName, webacsTimeInterval, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.info);
                assert.equal(true, Array.isArray(data.response.metrics));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfacesMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceInterfacesMetricsMetric - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceInterfacesMetricsMetric(webacsMetric, webacsDevice, webacsIfName, webacsTimeInterval, webacsStartTime, webacsEndTime, null, webacsDirection, null, null, null, null, null, webacsSiteId, null, webacsDscp, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.currentDateTime);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.metricName);
                assert.equal(true, Array.isArray(data.response.metricRows));
                assert.equal('string', data.response.resourceName);
                assert.equal('object', typeof data.response.xValueProperty);
                assert.equal(true, Array.isArray(data.response.yValueProperty));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceInterfacesMetricsMetric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceResourceTypes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceResourceTypes((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.supportedResources));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceResourceTypes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceSystemHealth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceSystemHealth(webacsStartTime, webacsEndTime, webacsRange, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceSystemHealth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceSystemInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceSystemInfo((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.childStatistics));
                assert.equal(true, Array.isArray(data.response.statisticEntry));
                assert.equal('string', data.response.statisticsName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceSystemInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceVms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceVms(null, null, null, null, null, webacsRange, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.resources));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceVms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceVmsMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceVmsMetrics(null, null, null, null, webacsTimeInterval, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.info);
                assert.equal(true, Array.isArray(data.response.metrics));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceVmsMetrics', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpStatisticsServiceVmsMetricsMetric - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpStatisticsServiceVmsMetricsMetric(webacsMetric, null, null, null, null, webacsTimeInterval, webacsStartTime, webacsEndTime, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.currentDateTime);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.metricName);
                assert.equal(true, Array.isArray(data.response.metricRows));
                assert.equal('string', data.response.resourceName);
                assert.equal('object', typeof data.response.xValueProperty);
                assert.equal(true, Array.isArray(data.response.yValueProperty));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpStatisticsServiceVmsMetricsMetric', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpUdfServiceDeleteUserTagDefinitionBodyParam = {
      names: [
        'string'
      ]
    };
    describe('#putWebacsApiV4OpUdfServiceDeleteUserTagDefinition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpUdfServiceDeleteUserTagDefinition(webacsPutWebacsApiV4OpUdfServiceDeleteUserTagDefinitionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpUdfServiceDeleteUserTagDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPutWebacsApiV4OpUdfServiceImportUserTagDefinitionBodyParam = {
      definitions: [
        {}
      ]
    };
    describe('#putWebacsApiV4OpUdfServiceImportUserTagDefinition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpUdfServiceImportUserTagDefinition(webacsPutWebacsApiV4OpUdfServiceImportUserTagDefinitionBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpUdfServiceImportUserTagDefinition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpUpdateRetrieve - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpUpdateRetrieve((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.ubfname);
                assert.equal('string', data.response.state);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpUpdateRetrieve', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpUserManagementUserGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpUserManagementUserGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpUserManagementUserGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpUserManagementUserGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpUserManagementUserGroups((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.active);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.userGroups);
                assert.equal('string', data.response.virtualDomains);
                assert.equal(true, data.response.wirelessUser);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpUserManagementUserGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsDomainFQN = 'fakedata';
    describe('#putWebacsApiV4OpVd - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpVd(webacsDomainFQN, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpVd', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWebacsApiV4OpVd - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getWebacsApiV4OpVd(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.domainFQN);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.timezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4OpVd', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putWebacsApiV4OpWlanProvisioningDeployTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.putWebacsApiV4OpWlanProvisioningDeployTemplate((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'putWebacsApiV4OpWlanProvisioningDeployTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpAaaTacacsPlusServer - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpAaaTacacsPlusServer(null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpAaaTacacsPlusServer', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsDeleteWebacsApiV4OpApOnboardingProfileBodyParam = {
      profileIds: [
        {}
      ]
    };
    describe('#deleteWebacsApiV4OpApOnboardingProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpApOnboardingProfile(webacsDeleteWebacsApiV4OpApOnboardingProfileBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpApOnboardingProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpCliTemplateConfigurationDeleteTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpCliTemplateConfigurationDeleteTemplate((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpCliTemplateConfigurationDeleteTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpCliTemplateConfigurationFolder - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpCliTemplateConfigurationFolder((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpCliTemplateConfigurationFolder', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpCredentialProfilesManagementCredentialProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpCredentialProfilesManagementCredentialProfiles((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpCredentialProfilesManagementCredentialProfiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpGroupsGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpGroupsGroup((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpGroupsGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpImageId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpImageId((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpImageId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpMacFilterMacFilterTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpMacFilterMacFilterTemplate((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpMacFilterMacFilterTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpPnpProfilePnpProfileFolderProfileName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpPnpProfilePnpProfileFolderProfileName(webacsPnpProfileFolder, webacsProfileName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpPnpProfilePnpProfileFolderProfileName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpPnpProfileProfileNameInstance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpPnpProfileProfileNameInstance(webacsProfileName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpPnpProfileProfileNameInstance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName(webacsProfileName, webacsInstanceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpPnpProfileProfileNameInstanceInstanceName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpSettingsServersFtpServersServerName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpSettingsServersFtpServersServerName('fakedata', (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpSettingsServersFtpServersServerName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpUserManagementUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpUserManagementUsers((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpUserManagementUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpVd - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpVd(webacsDomainFQN, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpVd', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsApGroupName = 'fakedata';
    describe('#deleteWebacsApiV4OpWlanProvisioningApGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningApGroup(webacsControllerId, webacsControllerName, webacsApGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpWlanProvisioningApGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpWlanProvisioningInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningInterface(webacsControllerId, webacsControllerName, webacsInterfaceName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpWlanProvisioningInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsInterfaceGroupName = 'fakedata';
    describe('#deleteWebacsApiV4OpWlanProvisioningInterfaceGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningInterfaceGroup(webacsControllerId, webacsControllerName, webacsInterfaceGroupName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpWlanProvisioningInterfaceGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsWlanProfileName = 'fakedata';
    describe('#deleteWebacsApiV4OpWlanProvisioningWlanProfile - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningWlanProfile(webacsControllerId, webacsControllerName, webacsWlanProfileName, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpWlanProvisioningWlanProfile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteWebacsApiV4OpWlanProvisioningWlanTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteWebacsApiV4OpWlanProvisioningWlanTemplate((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'deleteWebacsApiV4OpWlanProvisioningWlanTemplate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const webacsPostWebacsApiV4OpDevicesSyncDevicesBodyParam = {
      devices: [
        {}
      ]
    };
    describe('#getWebacsApiV4DataDevices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getWebacsApiV4DataDevices((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-cisco_prime-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Webacs', 'getWebacsApiV4DataDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
