
## 0.7.4 [10-15-2024]

* Changes made at 2024.10.14_20:16PM

See merge request itentialopensource/adapters/adapter-cisco_prime!17

---

## 0.7.3 [08-24-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-cisco_prime!15

---

## 0.7.2 [08-14-2024]

* Changes made at 2024.08.14_18:26PM

See merge request itentialopensource/adapters/adapter-cisco_prime!14

---

## 0.7.1 [08-07-2024]

* Changes made at 2024.08.06_19:39PM

See merge request itentialopensource/adapters/adapter-cisco_prime!13

---

## 0.7.0 [05-23-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!12

---

## 0.6.6 [03-27-2024]

* Changes made at 2024.03.27_13:13PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!11

---

## 0.6.5 [03-21-2024]

* Changes made at 2024.03.21_14:56PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!10

---

## 0.6.4 [03-13-2024]

* Changes made at 2024.03.13_11:12AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!9

---

## 0.6.3 [03-12-2024]

* Changes made at 2024.03.12_10:58AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!8

---

## 0.6.2 [02-27-2024]

* Changes made at 2024.02.27_11:32AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!7

---

## 0.6.1 [12-24-2023]

* update axios and metadata

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!6

---

## 0.6.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!5

---

## 0.5.0 [11-07-2023]

* More migration changes

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!5

---

## 0.4.0 [09-26-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!4

---

## 0.3.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!3

---

## 0.2.1 [12-28-2021]

- Fix healthcheck

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!2

---

## 0.2.0 [12-28-2021]

- Adding a call for POC

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!1

---

## 0.1.1 [10-27-2021]

- Initial Commit

See commit 6fddc73

---
