
## 0.3.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!3

---

## 0.2.1 [12-28-2021]

- Fix healthcheck

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!2

---

## 0.2.0 [12-28-2021]

- Adding a call for POC

See merge request itentialopensource/adapters/telemetry-analytics/adapter-cisco_prime!1

---

## 0.1.1 [10-27-2021]

- Initial Commit

See commit 6fddc73

---
